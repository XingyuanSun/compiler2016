all:
	$(MAKE) -C src all
	if [ ! -d bin ]; then mkdir bin; fi
	if [ ! -d bin/compiler ]; then mkdir bin/compiler; fi
	if [ ! -d bin/compiler/ast ]; then mkdir bin/compiler/ast; fi
	if [ ! -d bin/compiler/parser ]; then mkdir bin/compiler/parser; fi
	if [ ! -d bin/compiler/ir ]; then mkdir bin/compiler/ir; fi
	if [ ! -d bin/compiler/cfg ]; then mkdir bin/compiler/cfg; fi
	cp src/*.class bin/
	cp src/compiler/ast/*.class bin/compiler/ast/
	cp src/compiler/parser/*.class bin/compiler/parser/
	cp src/compiler/ir/*.class bin/compiler/ir/
	cp src/compiler/cfg/*.class bin/compiler/cfg/
	$(MAKE) -C src clean
clean:
	$(MAKE) -C src clean
	-rm -rf bin
