import compiler.cfg.BasicBlock;
import compiler.cfg.CFG;
import compiler.ir.Function;
import compiler.ir.IR;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.*;
import compiler.parser.*;
import compiler.ast.*;

import javax.annotation.processing.SupportedSourceVersion;

public class Main {
	public static void main(String[] args) throws IOException {
		InputStream is = System.in;
		ANTLRInputStream input = new ANTLRInputStream(is);
		MoLexer lexer = new MoLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MoParser parser = new MoParser(tokens);
		ParseTree tree = parser.program();
		ParseTreeWalker walker = new ParseTreeWalker();
		MoListenerAST extractor = new MoListenerAST(parser);
		walker.walk(extractor, tree);
		if (!extractor.successful)
			System.exit(1);
		Prog root = (Prog)extractor.stack.peek();

		SymbolTable table = new SymbolTable();
		table.put(Symbol.get("print"), new FunctionDecl(new VoidType(), Symbol.get("print"), new VarDeclList(new VarDecl(new StringType(), Symbol.get("str"))), new CompoundStmt(new StmtList(new EmptyExpr()))));
		table.put(Symbol.get("println"), new FunctionDecl(new VoidType(), Symbol.get("println"), new VarDeclList(new VarDecl(new StringType(), Symbol.get("str"))), new CompoundStmt(new StmtList(new EmptyExpr()))));
		table.put(Symbol.get("getString"), new FunctionDecl(new StringType(), Symbol.get("getString"), null, new CompoundStmt(new StmtList(new EmptyExpr()))));
		table.put(Symbol.get("getInt"), new FunctionDecl(new IntType(), Symbol.get("getInt"), null, new CompoundStmt(new StmtList(new EmptyExpr()))));
		table.put(Symbol.get("toString"), new FunctionDecl(new StringType(), Symbol.get("toString"), new VarDeclList(new VarDecl(new IntType(), Symbol.get("i"))), new CompoundStmt(new StmtList(new EmptyExpr()))));

		table.put(Symbol.get("length"), new FunctionDecl(new IntType(), Symbol.get("length"), new VarDeclList(new VarDecl(new StringType(), Symbol.get("_"))), new CompoundStmt(new StmtList(new EmptyExpr()))));
		table.put(Symbol.get("substring"), new FunctionDecl(new StringType(), Symbol.get("substring"), new VarDeclList(new VarDecl(new StringType(), Symbol.get("_")), new VarDeclList(new VarDecl(new IntType(), Symbol.get("left")), new VarDeclList(new VarDecl(new IntType(), Symbol.get("right"))))), new CompoundStmt(new StmtList(new EmptyExpr()))));
		table.put(Symbol.get("parseInt"), new FunctionDecl(new IntType(), Symbol.get("parseInt"), new VarDeclList(new VarDecl(new StringType(), Symbol.get("_"))), new CompoundStmt(new StmtList(new EmptyExpr()))));
		table.put(Symbol.get("ord"), new FunctionDecl(new IntType(), Symbol.get("ord"), new VarDeclList(new VarDecl(new StringType(), Symbol.get("_")), new VarDeclList(new VarDecl(new IntType(), Symbol.get("pos")))), new CompoundStmt(new StmtList(new EmptyExpr()))));

		table.put(Symbol.get("size"), new FunctionDecl(new IntType(), Symbol.get("size"), new VarDeclList(new VarDecl(new ArrayType(new IntType()), Symbol.get("_"))), new CompoundStmt(new StmtList(new EmptyExpr()))));
		if (!root.round_1(table))
			System.exit(1);
		if (!root.round_2(table))
			System.exit(1);
		if (root.round_3(table, null, null) == null)
			System.exit(1);
		FileWriter pretty = new FileWriter("PrettyPrinter.mx");
		pretty.write(root.prettyPrinter(0));
		pretty.flush();
		pretty.close();

		IR ir = root.translate(table);
		ir.adjust();

		FileWriter SSA = new FileWriter("SSA.mx");

		for (Function function : ir.fragments) {
			CFG cfg = new CFG(function);
			cfg.calcDominator();
			cfg.insertPhi();
			cfg.variableRenaming();
			cfg.uselessElimination();
			//cfg.redundancyElimination();
			for (BasicBlock basicBlock : cfg.extra)
				cfg.basicBlocks.add(basicBlock);
			SSA.write(function.printSSA(cfg));
			SSA.flush();
			function.set(cfg);
		}
		SSA.close();
		ir.isLeaf();
		if (ir.fragments.size() == 1 && ir.staticAllocation()) {
			System.out.print(ir.staticToString());
		}
		else {
			ir.dynamicAllocation();
			System.out.print(ir.dynamicToString());
		}
	}
}
