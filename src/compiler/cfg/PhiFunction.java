package compiler.cfg;

import compiler.ir.Register;
import compiler.ir.Temp;

import java.util.HashSet;

/**
 * Created by 孙星远 on 2016/5/6.
 */
public class PhiFunction {
    public Temp temp;
    public RenamedTemp def;
    public HashSet<RenamedTemp> used;
    public boolean useless;
    public PhiFunction(Temp temp) {
        this.temp = temp;
        used = new HashSet<RenamedTemp>();
        this.useless = false;
    }
}
