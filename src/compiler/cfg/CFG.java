package compiler.cfg;

import compiler.ir.*;

import java.awt.image.renderable.RenderableImage;
import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Created by 孙星远 on 2016/5/6.
 */
public class CFG {
    public List<BasicBlock> basicBlocks;
    public BasicBlock start;
    public HashMap<Temp, HashSet<BasicBlock>> definition;
    public List<BasicBlock> extra = new ArrayList<>();

    public CFG(compiler.ir.Function function) {
        int count = 0;
        List<BasicBlock> basicBlocks = new ArrayList<BasicBlock>();
        for (int i = 0; i < function.body.size(); ++i) {
            if (function.body.get(i) instanceof Label) {
                basicBlocks.add(new BasicBlock((Label) function.body.get(i), count++));
                //System.out.println(function.body.get(i).print());
            }
            basicBlocks.get(basicBlocks.size() - 1).data.add(function.body.get(i));
        }
        start = basicBlocks.get(0);
        for (int i = 0; i < basicBlocks.size(); ++i)
            if (basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1) instanceof Branch) {
                Branch branch = (Branch) basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1);
                for (int j = 0; j < basicBlocks.size(); ++j) {
                    if (branch.label1.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ1 = basicBlocks.get(j);
                    if (branch.label2.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ2 = basicBlocks.get(j);
                }
            }
            else if (basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1) instanceof Goto) {
                Goto got = (Goto) basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1);
                for (int j = 0; j < basicBlocks.size(); ++j)
                    if (got.label.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ1 = basicBlocks.get(j);
            }
            else if (basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1) instanceof RelationalBranch) {
                RelationalBranch relationalBranch = (RelationalBranch) basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1);
                for (int j = 0; j < basicBlocks.size(); ++j)
                    if (relationalBranch.label.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ1 = basicBlocks.get(j);
                basicBlocks.get(i).succ2 = basicBlocks.get(i + 1);
            }
        this.basicBlocks = new ArrayList<BasicBlock>();

        //System.out.println("size = " + basicBlocks.size());
        int size = 0;
        for (BasicBlock basicBlock : basicBlocks)
            if (basicBlock.label.num > size)
                size = basicBlock.label.num;
        boolean[] visited = new boolean[size + 1];
        dfs(basicBlocks.get(0), visited);
        for (BasicBlock basicBlock : basicBlocks)
            if (basicBlock.number == -1)
                extra.add(basicBlock);
        //System.out.println("this.size = " + this.basicBlocks.size());

        for (BasicBlock basicBlock : this.basicBlocks) {
            if (basicBlock.succ1 != null)
                basicBlock.succ1.predecessor.add(basicBlock);
            if (basicBlock.succ2 != null)
                basicBlock.succ2.predecessor.add(basicBlock);
        }

        definition = new HashMap<Temp, HashSet<BasicBlock>>();
        for (BasicBlock basicBlock : this.basicBlocks) {
            for (Quadruple quadruple : basicBlock.data) {
                ArrayList<Temp> list = new ArrayList<>();
                quadruple.write(list);
                for (int i = 0; i < list.size(); ++i) {
                    if (!definition.containsKey(list.get(i)))
                        definition.put(list.get(i), new HashSet<BasicBlock>());
                    definition.get(list.get(i)).add(basicBlock);
                    //System.out.println("insert " + basicBlock.label.num + " " + list.get(i).print());
                    LinkedList<RenamedTemp> templist = new LinkedList<>();
                    templist.add(new RenamedTemp(list.get(i), -1));
                    stack.put(list.get(i), templist);
                    //list.get(i).reachingDef = new RenamedTemp(list.get(i), -1, start, -1);
                }

                list = new ArrayList<>();
                quadruple.read(list);
                for (int i = 0; i < list.size(); ++i) {
                    LinkedList<RenamedTemp> templist = new LinkedList<>();
                    templist.add(new RenamedTemp(list.get(i), -1));
                    stack.put(list.get(i), templist);
                    //list.get(i).reachingDef = new RenamedTemp(list.get(i), -1, start, -1);
                }
            }
        }
    }

    private void dfs(BasicBlock now, boolean[] visited) {
        visited[now.label.num] = true;
        if (now.succ1 != null && !visited[now.succ1.label.num]) {
            //System.out.println(now.succ1.label.num);
            dfs(now.succ1, visited);
        }
        if (now.succ2 != null && !visited[now.succ2.label.num])
            dfs(now.succ2, visited);
        now.number = basicBlocks.size();
        basicBlocks.add(now);
    }

    public void calcDominator() {
        /*for (int i = 0; i < basicBlocks.size(); ++i) {
            System.out.print(i + " preds:");
            for (Iterator<BasicBlock> iterator = basicBlocks.get(i).predecessor.iterator(); iterator.hasNext(); ) {
                System.out.print(iterator.next().number + " ");
            }
            System.out.println("");
        }*/

        BasicBlock[] doms = new BasicBlock[this.basicBlocks.size()];
        doms[start.number] = start;
        boolean changed = true;
        //System.out.println("CFG");
        while (changed) {
            changed = false;
            for (int i = basicBlocks.size() - 1; i >= 0; --i) {
                BasicBlock b = basicBlocks.get(i);
                if (b.equals(start))
                    continue;
                BasicBlock new_idom = null;
                for (Iterator<BasicBlock> iterator = b.predecessor.iterator(); iterator.hasNext(); ) {
                    BasicBlock next = iterator.next();
                    if (new_idom == null)
                        new_idom = next;
                    else if (next.number > new_idom.number)
                        new_idom = next;
                }
                if (new_idom != null) {
                    for (Iterator<BasicBlock> iterator = b.predecessor.iterator(); iterator.hasNext(); ) {
                        BasicBlock p = iterator.next();
                        if (p.equals(new_idom))
                            continue;
                        if (doms[p.number] != null)
                            new_idom = intersect(p, new_idom, doms);
                    }
                    if (!new_idom.equals(doms[b.number])) {
                        doms[b.number] = new_idom;
                        changed = true;
                    }
                }
            }
        }
        //System.out.println("CFG");

        /*for (int i = 0; i < basicBlocks.size(); ++i) {
            System.out.print(i + ":" + doms[i].number + "_");
            System.out.println("");
        }*/

        for (BasicBlock b : basicBlocks)
            if (b.predecessor.size() >= 2) {
                //System.out.println("b = " + b.number);
                for (Iterator<BasicBlock> iterator = b.predecessor.iterator(); iterator.hasNext(); ) {
                    BasicBlock runner = iterator.next();
                    while (!runner.equals(doms[b.number])) {
                        //System.out.println("runner = " + runner.number);
                        runner.dominanceFrontier.add(b);
                        runner = doms[runner.number];
                    }
                }
            }

        /*for (int i = 0; i < basicBlocks.size(); ++i) {
            System.out.print(i + ":" + doms[i].number + "_");
            for (Iterator<BasicBlock> iterator = basicBlocks.get(i).dominanceFrontier.iterator(); iterator.hasNext(); ) {
                System.out.print(iterator.next().number + " ");
            }
            System.out.println("");
        }*/

        for (BasicBlock b: basicBlocks) {
            /*for (BasicBlock basicBlock = b; basicBlock != start; basicBlock = doms[basicBlock.number])
                b.dominator.add(basicBlock);
            b.dominator.add(start);*/
            if (doms[b.number].number == b.number)
                continue;
            doms[b.number].son.add(b);
        }

        /*for (BasicBlock b: basicBlocks) {
            System.out.print(b.number + ":");
            for (BasicBlock basicBlock : b.dominator)
                System.out.print(basicBlock.number + " " );
            System.out.println("");
        }
        for (BasicBlock b: basicBlocks) {
            System.out.print(b.number + ":");
            for (BasicBlock basicBlock : b.son)
                System.out.print(basicBlock.number + " " );
            System.out.println("");
        }*/
    }

    private BasicBlock intersect(BasicBlock b1, BasicBlock b2, BasicBlock[] doms) {
        int finger1 = b1.number;
        int finger2 = b2.number;
        while (finger1 != finger2) {
            while (finger1 < finger2)
                finger1 = doms[finger1].number;
            while (finger2 < finger1)
                finger2 = doms[finger2].number;
        }
        return basicBlocks.get(finger1);
    }

    public void insertPhi() {
        /*for (Iterator iterator = definition.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry entry = (Map.Entry) iterator.next();
            Temp v = (Temp) entry.getKey();
            v.reachingDef = new RenamedTemp(v, -1, start);
            System.out.println("set " + v.print() + " as " + v.reachingDef.print());
        }*/
        for (Iterator iterator = definition.entrySet().iterator(); iterator.hasNext(); ) {
            HashSet<BasicBlock> F = new HashSet<>();
            Map.Entry entry = (Map.Entry) iterator.next();
            Temp v = (Temp) entry.getKey();
            HashSet<BasicBlock> defs = (HashSet<BasicBlock>) entry.getValue();
            HashSet<BasicBlock> W = new HashSet<>();
            for (Iterator<BasicBlock> iter = defs.iterator(); iter.hasNext(); ) {
                BasicBlock B = iter.next();
                W.add(B);
            }
            while (W.size() > 0) {
                Iterator<BasicBlock> it = W.iterator();
                BasicBlock X = it.next();
                it.remove();
                for (Iterator<BasicBlock> y = X.dominanceFrontier.iterator(); y.hasNext(); ) {
                    BasicBlock Y = y.next();
                    if (!F.contains(Y)) {
                        Y.phiFunctions.add(new PhiFunction(v));
                        F.add(Y);
                        if (!defs.contains(Y))
                            W.add(Y);
                    }
                }
            }
        }
        /*for (int i = 0; i < basicBlocks.size(); ++i) {
            for (PhiFunction phiFunction : basicBlocks.get(i).phiFunctions) {
                System.out.print("Phi" + phiFunction.temp.print() + " ");
            }
            System.out.println("");
            for (Quadruple quadruple : basicBlocks.get(i).data) {
                System.out.println(quadruple.print() + " ");
            }
            System.out.println("");
        }*/
    }

    public void variableRenaming() {
        Temp.last = new HashMap<Temp, RenamedTemp>();
        dfsOnDomTree(start);

        /*System.out.println("start here!");

        for (int i = 0; i < basicBlocks.size(); ++i) {
            System.out.println("block" + basicBlocks.get(i).label.num);
            for (PhiFunction phiFunction : basicBlocks.get(i).phiFunctions) {
                System.out.println("Phi" + phiFunction.temp.print() + " ");
                System.out.println("def" + phiFunction.def.print());
                for (RenamedTemp renamedTemp : phiFunction.used)
                    System.out.println("use" + renamedTemp.print());
            }
            System.out.println("");
            for (Quadruple quadruple : basicBlocks.get(i).data) {
                System.out.println(quadruple.print() + " ");
            }
            System.out.println("");
        }*/
    }

    private HashMap<Temp, LinkedList<RenamedTemp>> stack = new HashMap<>();

    private void dfsOnDomTree(BasicBlock now) {
        List<Temp> pushed = new LinkedList<>();
        for (int i = 0; i < now.phiFunctions.size(); ++i) {
            PhiFunction v = now.phiFunctions.get(i);
            RenamedTemp vp;
            if (!Temp.last.containsKey(v.temp))
                vp = new RenamedTemp(v.temp, 0);
            else
                vp = new RenamedTemp(v.temp, Temp.last.get(v.temp).subscript + 1);
            Temp.last.put(v.temp, vp);
            stack.get(v.temp).push(vp);
            pushed.add(v.temp);
            v.def = vp;
        }
        for (int j = 0; j < now.data.size(); ++j){
            Quadruple i = now.data.get(j);
            Quadruple newi = i;
            ArrayList<Temp> used = new ArrayList<>();
            i.read(used);
            for (int k = 0; k < used.size(); ++k) {
                Temp v = used.get(k);
                newi = newi.replaceUse(v, stack.get(v).peek());
            }

            ArrayList<Temp> defined = new ArrayList<>();
            i.write(defined);
            for (int k = 0; k < defined.size(); ++k) {
                Temp v = defined.get(k);
                RenamedTemp vp;
                if (!Temp.last.containsKey(v))
                    vp = new RenamedTemp(v, 0);
                else
                    vp = new RenamedTemp(v, Temp.last.get(v).subscript + 1);
                Temp.last.put(v, vp);
                newi = newi.replaceDefine(v, vp);
                stack.get(v).push(vp);
                pushed.add(v);
            }
            now.data.set(j, newi);
        }
        if (now.succ1 != null)
            for (int k = 0; k < now.succ1.phiFunctions.size(); ++k) {
                PhiFunction v = now.succ1.phiFunctions.get(k);
                v.used.add(stack.get(v.temp).peek());
            }
        if (now.succ2 != null)
            for (int k = 0; k < now.succ2.phiFunctions.size(); ++k) {
                PhiFunction v = now.succ2.phiFunctions.get(k);
                v.used.add(stack.get(v.temp).peek());
            }
        for (Iterator<BasicBlock> iterator = now.son.iterator(); iterator.hasNext(); )
            dfsOnDomTree(iterator.next());
        for (Temp temp : pushed)
            stack.get(temp).pop();
    }

    public void uselessElimination() {
        boolean changed = true;
        while (changed) {
            changed = false;
            for (BasicBlock basicBlock : basicBlocks) {
                for (PhiFunction phiFunction : basicBlock.phiFunctions)
                    for (RenamedTemp renamedTemp : phiFunction.used)
                        renamedTemp.used = false;
                for (Quadruple quadruple : basicBlock.data) {
                    List<RenamedTemp> used = new ArrayList<>();
                    quadruple.used(used);
                    for (RenamedTemp renamedTemp : used)
                        renamedTemp.used = false;
                }
            }
            for (BasicBlock basicBlock : basicBlocks) {
                for (PhiFunction phiFunction : basicBlock.phiFunctions) {
                    if (phiFunction.useless)
                        continue;
                    for (RenamedTemp renamedTemp : phiFunction.used)
                        renamedTemp.used = true;
                }
                for (Quadruple quadruple : basicBlock.data) {
                    if (quadruple.useless)
                        continue;
                    List<RenamedTemp> used = new ArrayList<>();
                    quadruple.used(used);
                    for (RenamedTemp renamedTemp : used)
                        renamedTemp.used = true;
                }
            }
            for (BasicBlock basicBlock : basicBlocks) {
                for (PhiFunction phiFunction : basicBlock.phiFunctions) {
                    if (phiFunction.useless)
                        continue;
                    RenamedTemp renamedTemp = phiFunction.def;
                    if (!renamedTemp.used && renamedTemp.temp.name == null) {
                        phiFunction.useless = true;
                        changed = true;
                    }
                }
                for (Quadruple quadruple : basicBlock.data) {
                    if (quadruple.useless)
                        continue;
                    List<RenamedTemp> defined = new ArrayList<>();
                    quadruple.defined(defined);
                    for (RenamedTemp renamedTemp : defined)
                        if (!renamedTemp.used && renamedTemp.temp.name == null && !(quadruple instanceof Call)) {
                            quadruple.useless = true;
                            changed = true;
                        }
                }
            }
        }
    }

    public void redundancyElimination() {
        HashMap<RenamedTemp, RenamedTemp> mapping = new HashMap<>();
        for (BasicBlock basicBlock : this.basicBlocks) {
            for (Quadruple quadruple : basicBlock.data) {
                List<RenamedTemp> list = new ArrayList<>();
                quadruple.used(list);
                for (int i = 0; i < list.size(); ++i)
                    mapping.put(list.get(i), list.get(i));
                list = new ArrayList<>();
                quadruple.defined(list);
                for (int i = 0; i < list.size(); ++i)
                    mapping.put(list.get(i), list.get(i));
            }
        }
        for (BasicBlock basicBlock : basicBlocks)
            for (Quadruple quadruple : basicBlock.data)
                if (quadruple instanceof Assign)
                    if (((Assign) quadruple).src instanceof RenamedTemp && ((Assign) quadruple).dest instanceof RenamedTemp) {
                        if (((RenamedTemp) ((Assign) quadruple).dest).temp.name != null)
                            continue;
                        mapping.put((RenamedTemp) ((Assign) quadruple).dest, (RenamedTemp) ((Assign) quadruple).src);
                        quadruple.useless = true;
                    }
        boolean changed = true;
        while (changed) {
            changed = false;
            for (Iterator iterator = mapping.entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry<RenamedTemp, RenamedTemp> entry = (Map.Entry<RenamedTemp, RenamedTemp>)iterator.next();
                RenamedTemp src1 = entry.getKey();
                RenamedTemp src2 = entry.getValue();
                if (!mapping.get(src2).equals(src2)) {
                    changed = true;
                    mapping.put(src1, mapping.get(src2));
                }
            }
        }
        for (Iterator iterator = mapping.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<RenamedTemp, RenamedTemp> entry = (Map.Entry<RenamedTemp, RenamedTemp>)iterator.next();
            RenamedTemp src1 = entry.getKey();
            RenamedTemp src2 = entry.getValue();
            //System.out.println(src1.print() + " " + src2.print());
        }
        for (BasicBlock basicBlock : basicBlocks)
            for (Quadruple quadruple : basicBlock.data) {
                quadruple.replaceUse(mapping);
            }
    }
}
