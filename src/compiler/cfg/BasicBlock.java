package compiler.cfg;

import compiler.ir.Label;
import compiler.ir.Quadruple;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by 孙星远 on 2016/5/6.
 */
public class BasicBlock {
    public List<Quadruple> data;
    public List<PhiFunction> phiFunctions;
    public Label label;
    public int number;
    public int originalRank;
    public BasicBlock succ1, succ2;
    public HashSet<BasicBlock> predecessor;
    public HashSet<BasicBlock> dominanceFrontier;
    public HashSet<BasicBlock> son;

    public BasicBlock(Label label, int originalRank) {
        this.label = label;
        this.originalRank = originalRank;
        data = new ArrayList<>();
        phiFunctions = new ArrayList<>();
        predecessor = new HashSet<>();
        dominanceFrontier = new HashSet<>();
        son = new HashSet<>();
        this.number = -1;
    }

    @Override
    public int hashCode() {
        return number;
    }

    public boolean equals(BasicBlock rhs) {
        if (rhs == null)
            return false;
        return number == rhs.number;
    }
}
