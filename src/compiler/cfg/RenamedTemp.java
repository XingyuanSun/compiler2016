package compiler.cfg;

import compiler.ir.Address;
import compiler.ir.Temp;

/**
 * Created by 孙星远 on 2016/5/7.
 */
public class RenamedTemp extends Address {
    public Temp temp;
    public int subscript;
    public boolean used;

    public RenamedTemp(Temp temp, int subscript) {
        this.temp = temp;
        this.subscript = subscript;
        this.used = false;
    }

    public String print() {
        return temp.print() + "_" + subscript;
    }

    public String generate() {
        return "";
    }

    public int hashCode() {
        return temp.hashCode() * 1000 + subscript;
    }

    public boolean equals(RenamedTemp rhs) {
        if (rhs == null)
            return false;
        if (!rhs.temp.equals(temp))
            return false;
        if (subscript != rhs.subscript)
            return false;
        return true;
    }
}
