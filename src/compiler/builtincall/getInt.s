getInt:
sw	$ra, 124($sp)
li	$a1, 0
getInt_label1:
li	$v0, 12
syscall
beq	$v0, 45, getInt_label2
bge	$v0, 48, getInt_label3
j	getInt_label1
getInt_label2:
li	$v0, 12
syscall
li	$a1, 1
getInt_label3:
sub	$a0, $v0, 48
getInt_label6:
li	$v0, 12
syscall
blt	$v0, 48, getInt_label4
sub	$v0, $v0, 48
mul	$a0, $a0, 10
add	$a0, $a0, $v0
j getInt_label6
getInt_label4:
move	$v0, $a0
beq	$a1, 0, getInt_label5
neg	$v0, $v0
getInt_label5:
lw	$ra, 124($sp)
jr	$ra
