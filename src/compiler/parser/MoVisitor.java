// Generated from Mo.g4 by ANTLR 4.5.2

package compiler.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MoParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MoVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MoParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(MoParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link MoParser#class_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_decl(MoParser.Class_declContext ctx);
	/**
	 * Visit a parse tree produced by the {@code member_decl_list_}
	 * labeled alternative in {@link MoParser#member_decl_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember_decl_list_(MoParser.Member_decl_list_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code member_decl_list_c}
	 * labeled alternative in {@link MoParser#member_decl_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember_decl_list_c(MoParser.Member_decl_list_cContext ctx);
	/**
	 * Visit a parse tree produced by {@link MoParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(MoParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code type_arr_c}
	 * labeled alternative in {@link MoParser#type_arr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_arr_c(MoParser.Type_arr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code type_arr_}
	 * labeled alternative in {@link MoParser#type_arr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_arr_(MoParser.Type_arr_Context ctx);
	/**
	 * Visit a parse tree produced by {@link MoParser#var_decl_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_decl_stmt(MoParser.Var_decl_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code var_decl_}
	 * labeled alternative in {@link MoParser#var_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_decl_(MoParser.Var_decl_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code var_decl_expr}
	 * labeled alternative in {@link MoParser#var_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_decl_expr(MoParser.Var_decl_exprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code func_decl_t}
	 * labeled alternative in {@link MoParser#func_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_decl_t(MoParser.Func_decl_tContext ctx);
	/**
	 * Visit a parse tree produced by the {@code func_decl_v}
	 * labeled alternative in {@link MoParser#func_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_decl_v(MoParser.Func_decl_vContext ctx);
	/**
	 * Visit a parse tree produced by {@link MoParser#block_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_stmt(MoParser.Block_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_list_}
	 * labeled alternative in {@link MoParser#stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_list_(MoParser.Stmt_list_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_list_c}
	 * labeled alternative in {@link MoParser#stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_list_c(MoParser.Stmt_list_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parameter_list_}
	 * labeled alternative in {@link MoParser#parameter_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter_list_(MoParser.Parameter_list_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code parameter_list_c}
	 * labeled alternative in {@link MoParser#parameter_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter_list_c(MoParser.Parameter_list_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code argument_list_}
	 * labeled alternative in {@link MoParser#argument_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument_list_(MoParser.Argument_list_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code argument_list_c}
	 * labeled alternative in {@link MoParser#argument_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument_list_c(MoParser.Argument_list_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_block_stmt}
	 * labeled alternative in {@link MoParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_block_stmt(MoParser.Stmt_block_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_expr_stmt}
	 * labeled alternative in {@link MoParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_expr_stmt(MoParser.Stmt_expr_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_selection_stmt}
	 * labeled alternative in {@link MoParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_selection_stmt(MoParser.Stmt_selection_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_iteration_stmt}
	 * labeled alternative in {@link MoParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_iteration_stmt(MoParser.Stmt_iteration_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_jump_stmt}
	 * labeled alternative in {@link MoParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_jump_stmt(MoParser.Stmt_jump_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt_var_decl_stmt}
	 * labeled alternative in {@link MoParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_var_decl_stmt(MoParser.Stmt_var_decl_stmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constant_null}
	 * labeled alternative in {@link MoParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_null(MoParser.Constant_nullContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constant_int}
	 * labeled alternative in {@link MoParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_int(MoParser.Constant_intContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constant_string}
	 * labeled alternative in {@link MoParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_string(MoParser.Constant_stringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constant_true}
	 * labeled alternative in {@link MoParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_true(MoParser.Constant_trueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constant_false}
	 * labeled alternative in {@link MoParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_false(MoParser.Constant_falseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code jump_stmt_return}
	 * labeled alternative in {@link MoParser#jump_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJump_stmt_return(MoParser.Jump_stmt_returnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code jump_stmt_break}
	 * labeled alternative in {@link MoParser#jump_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJump_stmt_break(MoParser.Jump_stmt_breakContext ctx);
	/**
	 * Visit a parse tree produced by the {@code jump_stmt_continue}
	 * labeled alternative in {@link MoParser#jump_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJump_stmt_continue(MoParser.Jump_stmt_continueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code selection_stmt_if}
	 * labeled alternative in {@link MoParser#selection_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelection_stmt_if(MoParser.Selection_stmt_ifContext ctx);
	/**
	 * Visit a parse tree produced by the {@code selection_stmt_if_else}
	 * labeled alternative in {@link MoParser#selection_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelection_stmt_if_else(MoParser.Selection_stmt_if_elseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code iteration_stmt_while}
	 * labeled alternative in {@link MoParser#iteration_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIteration_stmt_while(MoParser.Iteration_stmt_whileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code iteration_stmt_for}
	 * labeled alternative in {@link MoParser#iteration_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIteration_stmt_for(MoParser.Iteration_stmt_forContext ctx);
	/**
	 * Visit a parse tree produced by {@link MoParser#expr_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_stmt(MoParser.Expr_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MoParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(MoParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignment_expr_}
	 * labeled alternative in {@link MoParser#assignment_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_expr_(MoParser.Assignment_expr_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code assignment_expr_c}
	 * labeled alternative in {@link MoParser#assignment_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_expr_c(MoParser.Assignment_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code condition_expr_}
	 * labeled alternative in {@link MoParser#condition_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_expr_(MoParser.Condition_expr_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code condition_expr_c}
	 * labeled alternative in {@link MoParser#condition_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_expr_c(MoParser.Condition_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logical_or_expr_c}
	 * labeled alternative in {@link MoParser#logical_or_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_or_expr_c(MoParser.Logical_or_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logical_or_expr_}
	 * labeled alternative in {@link MoParser#logical_or_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_or_expr_(MoParser.Logical_or_expr_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code logical_and_expr_c}
	 * labeled alternative in {@link MoParser#logical_and_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_and_expr_c(MoParser.Logical_and_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logical_and_expr_}
	 * labeled alternative in {@link MoParser#logical_and_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_and_expr_(MoParser.Logical_and_expr_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_ior_expr_c}
	 * labeled alternative in {@link MoParser#bitwise_ior_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_ior_expr_c(MoParser.Bitwise_ior_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_ior_expr_}
	 * labeled alternative in {@link MoParser#bitwise_ior_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_ior_expr_(MoParser.Bitwise_ior_expr_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_eor_expr_c}
	 * labeled alternative in {@link MoParser#bitwise_eor_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_eor_expr_c(MoParser.Bitwise_eor_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_eor_expr_}
	 * labeled alternative in {@link MoParser#bitwise_eor_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_eor_expr_(MoParser.Bitwise_eor_expr_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_and_expr_c}
	 * labeled alternative in {@link MoParser#bitwise_and_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_and_expr_c(MoParser.Bitwise_and_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_and_expr_}
	 * labeled alternative in {@link MoParser#bitwise_and_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_and_expr_(MoParser.Bitwise_and_expr_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_equal_expr_eq}
	 * labeled alternative in {@link MoParser#relation_equal_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_equal_expr_eq(MoParser.Relation_equal_expr_eqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_equal_expr_c}
	 * labeled alternative in {@link MoParser#relation_equal_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_equal_expr_c(MoParser.Relation_equal_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_equal_expr_neq}
	 * labeled alternative in {@link MoParser#relation_equal_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_equal_expr_neq(MoParser.Relation_equal_expr_neqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_gl_expr_c}
	 * labeled alternative in {@link MoParser#relation_gl_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_gl_expr_c(MoParser.Relation_gl_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_gl_expr_le}
	 * labeled alternative in {@link MoParser#relation_gl_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_gl_expr_le(MoParser.Relation_gl_expr_leContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_gl_expr_g}
	 * labeled alternative in {@link MoParser#relation_gl_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_gl_expr_g(MoParser.Relation_gl_expr_gContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_gl_expr_ge}
	 * labeled alternative in {@link MoParser#relation_gl_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_gl_expr_ge(MoParser.Relation_gl_expr_geContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relation_gl_expr_l}
	 * labeled alternative in {@link MoParser#relation_gl_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_gl_expr_l(MoParser.Relation_gl_expr_lContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_shift_expr_c}
	 * labeled alternative in {@link MoParser#bitwise_shift_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_shift_expr_c(MoParser.Bitwise_shift_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_shift_expr_r}
	 * labeled alternative in {@link MoParser#bitwise_shift_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_shift_expr_r(MoParser.Bitwise_shift_expr_rContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitwise_shift_expr_l}
	 * labeled alternative in {@link MoParser#bitwise_shift_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_shift_expr_l(MoParser.Bitwise_shift_expr_lContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additive_subtractive_expr_c}
	 * labeled alternative in {@link MoParser#additive_subtractive_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditive_subtractive_expr_c(MoParser.Additive_subtractive_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additive_subtractive_expr_s}
	 * labeled alternative in {@link MoParser#additive_subtractive_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditive_subtractive_expr_s(MoParser.Additive_subtractive_expr_sContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additive_subtractive_expr_a}
	 * labeled alternative in {@link MoParser#additive_subtractive_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditive_subtractive_expr_a(MoParser.Additive_subtractive_expr_aContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicative_divisive_expr_c}
	 * labeled alternative in {@link MoParser#multiplicative_divisive_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicative_divisive_expr_c(MoParser.Multiplicative_divisive_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicative_divisive_expr_mul}
	 * labeled alternative in {@link MoParser#multiplicative_divisive_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicative_divisive_expr_mul(MoParser.Multiplicative_divisive_expr_mulContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicative_divisive_expr_mod}
	 * labeled alternative in {@link MoParser#multiplicative_divisive_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicative_divisive_expr_mod(MoParser.Multiplicative_divisive_expr_modContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicative_divisive_expr_div}
	 * labeled alternative in {@link MoParser#multiplicative_divisive_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicative_divisive_expr_div(MoParser.Multiplicative_divisive_expr_divContext ctx);
	/**
	 * Visit a parse tree produced by the {@code creation_expr_d}
	 * labeled alternative in {@link MoParser#creation_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreation_expr_d(MoParser.Creation_expr_dContext ctx);
	/**
	 * Visit a parse tree produced by the {@code creation_expr_e}
	 * labeled alternative in {@link MoParser#creation_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreation_expr_e(MoParser.Creation_expr_eContext ctx);
	/**
	 * Visit a parse tree produced by the {@code creation_expr_u}
	 * labeled alternative in {@link MoParser#creation_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreation_expr_u(MoParser.Creation_expr_uContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dim_expr_c}
	 * labeled alternative in {@link MoParser#dim_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_expr_c(MoParser.Dim_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dim_expr_d}
	 * labeled alternative in {@link MoParser#dim_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_expr_d(MoParser.Dim_expr_dContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dim_expr_noe_}
	 * labeled alternative in {@link MoParser#dim_expr_noe}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_expr_noe_(MoParser.Dim_expr_noe_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code dim_expr_noe_c}
	 * labeled alternative in {@link MoParser#dim_expr_noe}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_expr_noe_c(MoParser.Dim_expr_noe_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expr_c}
	 * labeled alternative in {@link MoParser#unary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expr_c(MoParser.Unary_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expr_in}
	 * labeled alternative in {@link MoParser#unary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expr_in(MoParser.Unary_expr_inContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expr_de}
	 * labeled alternative in {@link MoParser#unary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expr_de(MoParser.Unary_expr_deContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expr_n}
	 * labeled alternative in {@link MoParser#unary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expr_n(MoParser.Unary_expr_nContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expr_d}
	 * labeled alternative in {@link MoParser#unary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expr_d(MoParser.Unary_expr_dContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expr_add}
	 * labeled alternative in {@link MoParser#unary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expr_add(MoParser.Unary_expr_addContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expr_sub}
	 * labeled alternative in {@link MoParser#unary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expr_sub(MoParser.Unary_expr_subContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfix_expr_a}
	 * labeled alternative in {@link MoParser#postfix_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expr_a(MoParser.Postfix_expr_aContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfix_expr_de}
	 * labeled alternative in {@link MoParser#postfix_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expr_de(MoParser.Postfix_expr_deContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfix_expr_c}
	 * labeled alternative in {@link MoParser#postfix_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expr_c(MoParser.Postfix_expr_cContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfix_expr_e}
	 * labeled alternative in {@link MoParser#postfix_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expr_e(MoParser.Postfix_expr_eContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfix_expr_in}
	 * labeled alternative in {@link MoParser#postfix_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expr_in(MoParser.Postfix_expr_inContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfix_expr_id}
	 * labeled alternative in {@link MoParser#postfix_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expr_id(MoParser.Postfix_expr_idContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primary_expr_id}
	 * labeled alternative in {@link MoParser#primary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_expr_id(MoParser.Primary_expr_idContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primary_expr_const}
	 * labeled alternative in {@link MoParser#primary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_expr_const(MoParser.Primary_expr_constContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primary_expr_paren}
	 * labeled alternative in {@link MoParser#primary_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_expr_paren(MoParser.Primary_expr_parenContext ctx);
}