grammar Mo;

@header {
package compiler.parser;
}

program
	: (class_decl|func_decl|var_decl ';')+
	;

class_decl
	: 'class' ID ('extends' ID)? '{' member_decl_list? func_decl* '}'
	;

member_decl_list
	: type_arr ID ';' 					# member_decl_list_
	| type_arr ID ';' member_decl_list	# member_decl_list_c
	;

type
	: 'int'
	| 'string'
	| 'bool'
	| ID
	;

type_arr
	: type				# type_arr_
	| type_arr '['']'	# type_arr_c
	;

var_decl_stmt
	: var_decl ';'
	;

var_decl
	: type_arr ID			# var_decl_
	| type_arr ID '=' expr	# var_decl_expr
	;

func_decl
	: type_arr ID '(' parameter_list? ')' block_stmt	# func_decl_t
	| 'void' ID '(' parameter_list? ')' block_stmt		# func_decl_v
	;

block_stmt
	: '{' stmt_list? '}'
	;

stmt_list
	: stmt				# stmt_list_
	| stmt stmt_list	# stmt_list_c
	;

parameter_list
	: type_arr ID						# parameter_list_
	| type_arr ID ',' parameter_list	# parameter_list_c
	;

argument_list
	: expr						# argument_list_
	| expr ',' argument_list	# argument_list_c
	;

stmt
	: block_stmt		# stmt_block_stmt
	| expr_stmt			# stmt_expr_stmt
	| selection_stmt	# stmt_selection_stmt
	| iteration_stmt	# stmt_iteration_stmt
	| jump_stmt			# stmt_jump_stmt
	| var_decl_stmt		# stmt_var_decl_stmt
	;

constant
	: 'null'			# constant_null
	| Int_Literal		# constant_int
	| String_Literal	# constant_string
	| 'true'			# constant_true
	| 'false'			# constant_false
	;

jump_stmt
	: 'return' expr? ';'	# jump_stmt_return
	| 'break' ';'			# jump_stmt_break
	| 'continue' ';'		# jump_stmt_continue
	;

selection_stmt
	: 'if' '(' expr ')' stmt 				# selection_stmt_if
	| 'if' '(' expr ')' stmt 'else' stmt	# selection_stmt_if_else
	;

iteration_stmt
	: 'while' '(' expr ')' stmt 					# iteration_stmt_while
	| 'for' '(' expr? ';' expr? ';' expr? ')' stmt	# iteration_stmt_for
	;

expr_stmt
	: expr? ';'
	;

expr
	: assignment_expr
	;

assignment_expr
	: unary_expr '=' assignment_expr	# assignment_expr_
	| condition_expr					# assignment_expr_c
	;

condition_expr
	: logical_or_expr '?' expr ':' condition_expr	# condition_expr_
	| logical_or_expr								# condition_expr_c
	;
	
logical_or_expr
	: logical_or_expr '||' logical_and_expr	# logical_or_expr_
	| logical_and_expr						# logical_or_expr_c
	;

logical_and_expr
	: logical_and_expr '&&' bitwise_ior_expr	# logical_and_expr_
	| bitwise_ior_expr							# logical_and_expr_c
	;

bitwise_ior_expr
	: bitwise_ior_expr '|' bitwise_eor_expr	# bitwise_ior_expr_
	| bitwise_eor_expr						# bitwise_ior_expr_c
	;
	
bitwise_eor_expr
	: bitwise_eor_expr '^' bitwise_and_expr	# bitwise_eor_expr_
	| bitwise_and_expr						# bitwise_eor_expr_c
	;

bitwise_and_expr
	: bitwise_and_expr '&' relation_equal_expr	# bitwise_and_expr_
	| relation_equal_expr						# bitwise_and_expr_c
	;
	
relation_equal_expr
	: relation_equal_expr '==' relation_gl_expr	# relation_equal_expr_eq
	| relation_equal_expr '!=' relation_gl_expr # relation_equal_expr_neq
	| relation_gl_expr							# relation_equal_expr_c
	;

relation_gl_expr
	: relation_gl_expr '<' bitwise_shift_expr	# relation_gl_expr_l
	| relation_gl_expr '>' bitwise_shift_expr	# relation_gl_expr_g
	| relation_gl_expr '>=' bitwise_shift_expr	# relation_gl_expr_ge
	| relation_gl_expr '<=' bitwise_shift_expr	# relation_gl_expr_le
	| bitwise_shift_expr						# relation_gl_expr_c
	;

bitwise_shift_expr
	: bitwise_shift_expr '<<' additive_subtractive_expr	# bitwise_shift_expr_l
	| bitwise_shift_expr '>>' additive_subtractive_expr	# bitwise_shift_expr_r
	| additive_subtractive_expr							# bitwise_shift_expr_c
	;

additive_subtractive_expr
	: additive_subtractive_expr '+' multiplicative_divisive_expr	# additive_subtractive_expr_a
	| additive_subtractive_expr '-' multiplicative_divisive_expr	# additive_subtractive_expr_s
	| multiplicative_divisive_expr									# additive_subtractive_expr_c
	;

multiplicative_divisive_expr
	: multiplicative_divisive_expr '*' creation_expr	# multiplicative_divisive_expr_mul
	| multiplicative_divisive_expr '/' creation_expr	# multiplicative_divisive_expr_div
	| multiplicative_divisive_expr '%' creation_expr	# multiplicative_divisive_expr_mod
	| creation_expr										# multiplicative_divisive_expr_c
	;

creation_expr
	: 'new' type dim_expr			# creation_expr_d
	| 'new' type_arr '(' expr ')'	# creation_expr_e
	| unary_expr					# creation_expr_u
	;
	
dim_expr
	: dim_expr_noe				# dim_expr_c
	| '[' expr ']' dim_expr		# dim_expr_d
	;

dim_expr_noe
	: 						# dim_expr_noe_
	| '[' ']' dim_expr_noe	# dim_expr_noe_c
	;

unary_expr
	: postfix_expr		# unary_expr_c
	| '++' unary_expr	# unary_expr_in
	| '--' unary_expr	# unary_expr_de
	| '!' unary_expr	# unary_expr_n
	| '~' unary_expr	# unary_expr_d
	| '+' unary_expr	# unary_expr_add
	| '-' unary_expr	# unary_expr_sub
	;

postfix_expr
	: primary_expr									# postfix_expr_c
	| postfix_expr '[' expr ']'						# postfix_expr_e
	| postfix_expr '.' ID '(' argument_list? ')'	# postfix_expr_f
	| postfix_expr '(' argument_list? ')'			# postfix_expr_a
	| postfix_expr '.' ID							# postfix_expr_id
	| postfix_expr '++'								# postfix_expr_in
	| postfix_expr '--'								# postfix_expr_de
	;

primary_expr
	: ID			# primary_expr_id
	| constant		# primary_expr_const
	| '(' expr ')'	# primary_expr_paren
	;

Break : 'break' ;
Continue : 'continue' ;
Else : 'else' ;
For : 'for' ;
If : 'if' ;
Int : 'int' ;
Void : 'void' ;
While : 'while' ;
Bool : 'bool' ;
String : 'string' ;
Null : 'null' ;
True : 'true' ;
False : 'false' ;
Return : 'return' ;
New : 'new' ;
Class : 'class';

LeftParen : '(';
RightParen : ')';
LeftBracket : '[';
RightBracket : ']';
LeftBrace : '{';
RightBrace : '}';

Less : '<';
LessEqual : '<=';
Greater : '>';
GreaterEqual : '>=';
LeftShift : '<<';
RightShift : '>>';

Plus : '+';
PlusPlus : '++';
Minus : '-';
MinusMinus : '--';
Star : '*';
Div : '/';
Mod : '%';

And : '&';
Or : '|';
AndAnd : '&&';
OrOr : '||';
Caret : '^';
Not : '!';
Tilde : '~';

Question : '?';
Colon : ':';
Semi : ';';
Comma : ',';

Assign : '=';
Equal : '==';
NotEqual : '!=';

Dot : '.';

ID : [a-zA-Z] ('a'..'z'|'A'..'Z'|'_'|'0'..'9')* ;
Int_Literal : [0-9]+ ;

fragment
EscapeSequence
    :   '\\' ["n\\]
    ;

String_Literal
	: '"' SCharSequence? '"'
	;

fragment
SCharSequence
	: SChar+
	;

fragment
SChar
	: ~["\r\n]
    | EscapeSequence
	;

Whitespace
	: [ \t]+ -> skip
	;

Newline
	: ('\r' '\n'? | '\n' ) -> skip
	;

BlockComment
    :   '/*' .*? '*/' -> skip
    ;
	
LineComment
	: '//' ~[\r\n]* -> skip
	;