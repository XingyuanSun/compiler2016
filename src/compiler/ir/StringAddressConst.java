package compiler.ir;

import java.util.ArrayList;
import java.util.List;

public class StringAddressConst extends Const {
    public String value;
    public String original;
    public int num;
    public static int count = 0;
    public static StringAddressConst newLine;

    public static List<StringAddressConst> list = new ArrayList<StringAddressConst>();

    public static void initialize() {
        count = 0;
        list = new ArrayList<StringAddressConst>();
        newLine = new StringAddressConst("\n", "\\n");
    }
    public StringAddressConst(String value, String original) {
        this.value = value;
        this.original = original;
        this.num = count++;
        list.add(this);
    }

    public String print() {
        return value;
    }

    public String generate() {
        return value;
    }
}
