package compiler.ir;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 孙星远 on 2016/4/27.
 */
public class Register extends Address {
    public int num;
    public static final int numOfReg = 18;
    public static final int numOfParam = 4;
    public static Temp[] reference;
    public static int[] time;
    public static boolean[] modified;

    public Register(int num) {
        this.num = num;
    }

    public static void initialize() {
        reference = new Temp[numOfReg + numOfParam];
        time = new int[numOfReg + numOfParam];
        modified = new boolean[numOfReg + numOfParam];
    }
    public static void releaseGlobal(List<Quadruple> list, int num) {
        if (reference[num] != null && reference[num].name != null) {
            if (list != null && modified[num]) //optimize
                list.add(new MemoryWrite(reference[num], new IntegerConst(0), new Register(num)));
            reference[num] = null;
            time[num] = 0;
            modified[num] = false;
        }
    }

    public static void modify(Register register) {
        modified[register.num] = true;
    }

    public static void release(List<Quadruple> list, int num) {
        if (reference[num] != null) {
            if (list != null && modified[num]) //optimize
                list.add(new MemoryWrite(reference[num], new IntegerConst(0), new Register(num)));
            reference[num] = null;
            time[num] = 0;
            modified[num] = false;
        }
    }

    public static void dispose(Temp temp) {
        for (int i = 0; i < numOfReg; ++i) {
            if (reference[i] != null && reference[i].equal(temp)) {
                reference[i] = null;
                time[i] = 0;
                modified[i] = false;
            }
        }
    }

    public static void release(List<Quadruple> list, Temp temp) {
        for (int i = 0; i < numOfReg; ++i) {
            if (reference[i] != null && reference[i].equal(temp))
                release(list, i);
        }
    }

    public static void store(List<Quadruple> list, int num) {
        if (reference[num] != null) {
            if (list != null)
                list.add(new MemoryWrite(reference[num], new IntegerConst(0), new Register(num)));
        }
    }

    public static void load(List<Quadruple> list, int num) {
        if (reference[num] != null) {
            if (list != null)
                list.add(new MemoryRead(new Register(num), reference[num], new IntegerConst(0)));
        }
    }

    public static Register Allocate(List<Quadruple> list, Temp temp, boolean read) {
        return Allocate(list, temp, read, null, null);
    }

    public static Register Allocate(List<Quadruple> list, Temp temp, boolean read, Address forbidden1) {
        return Allocate(list, temp, read, forbidden1, null);
    }

    public static Register Allocate(List<Quadruple> list, Temp temp, boolean read, Address forbidden1, Address forbidden2) {
        if (!(forbidden1 instanceof Register))
            forbidden1 = null;
        if (!(forbidden2 instanceof Register))
            forbidden2 = null;
        Register register = get(temp);
        if (register != null)
            return register;
        for (int i = 0; i < numOfReg; ++i)
            if (reference[i] == null) {
                reference[i] = temp;
                time[i] = list.size();
                modified[i] = false;
                if (read)
                    list.add(new MemoryRead(new Register(i), temp, new IntegerConst(0)));
                return new Register(i);
            }
        int minimum = 2000000000; //Infinity
        int optimal = -1;
        for (int i = 0; i < numOfReg; ++i)
            if ((forbidden1 == null || i != ((Register) forbidden1).num) && (forbidden2 == null || i != ((Register) forbidden2).num) && time[i] < minimum) {
                minimum = time[i];
                optimal = i;
            }
        release(list, optimal);
        //list.add(new MemoryWrite(reference[optimal], new IntegerConst(0), new Register(optimal)));
        reference[optimal] = temp;
        time[optimal] = list.size();
        modified[optimal] = false;
        if (read)
            list.add(new MemoryRead(new Register(optimal), temp, new IntegerConst(0)));
        return new Register(optimal);
    }

    public static Register get(Temp temp) {
        for (int i = 0; i < numOfReg + numOfParam; ++i)
            if (reference[i] != null && temp.equal(reference[i]))
                return new Register(i);
        return null;
    }

    public String print() {
        if (num < 10)
            return "$t" + num;
        else if (num < 18)
            return "$s" + (num - 10);
        else
            return "$a" + (num - 18);
    }

    public String generate() {
        return print();// + modified[this.num];
    }

    public static String getName(int num) {
        String s = "";
        switch (num) {
            case 0:
                s = "$zero";
                break;
            case 1:
                s = "$at";
                break;
            case 2:
                s = "$v0";
                break;
            case 3:
                s = "$v1";
                break;
            case 4:
                s = "$a0";
                break;
            case 5:
                s = "$a1";
                break;
            case 6:
                s = "$a2";
                break;
            case 7:
                s = "$a3";
                break;
            case 8:
                s = "$t0";
                break;
            case 9:
                s = "$t1";
                break;
            case 10:
                s = "$t2";
                break;
            case 11:
                s = "$t3";
                break;
            case 12:
                s = "$t4";
                break;
            case 13:
                s = "$t5";
                break;
            case 14:
                s = "$t6";
                break;
            case 15:
                s = "$t7";
                break;
            case 16:
                s = "$s0";
                break;
            case 17:
                s = "$s1";
                break;
            case 18:
                s = "$s2";
                break;
            case 19:
                s = "$s3";
                break;
            case 20:
                s = "$s4";
                break;
            case 21:
                s = "$s5";
                break;
            case 22:
                s = "$s6";
                break;
            case 23:
                s = "$s7";
                break;
            case 24:
                s = "$t8";
                break;
            case 25:
                s = "$t9";
                break;
            case 26:
                s = "$k0";
                break;
            case 27:
                s = "$k1";
                break;
            case 28:
                s = "$gp";
                break;
            case 29:
                s = "$sp";
                break;
            case 30:
                s = "$fp";
                break;
            case 31:
                s = "$ra";
                break;
        }
        return s;
    }

    public static int getNum(String s) {
        for (int i = 0; i < 32; ++i)
            if (getName(i).equals(s))
                return i;
        System.out.print("Register ERROR!\n");
        return -1;
    }
}