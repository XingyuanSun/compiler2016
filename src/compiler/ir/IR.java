package compiler.ir;

import java.util.*;

public class IR {
    public List<Function> fragments;
    public List<Temp> variables;
    public ArrayList<Quadruple> initializer;
    public ArrayList<Quadruple> dynamic;
    public int size;
    public Function mainFunction;

    public IR(List<Function> fragments, List<Temp> variables, ArrayList<Quadruple> initializer, Function mainFunction, int size) {
        this.fragments = fragments;
        this.variables = variables;
        this.initializer = initializer;
        this.mainFunction = mainFunction;
        this.size = size;
    }

    public void isLeaf() {
        for (Function function : fragments)
            function.isLeaf();
    }

    public String print() {
        String s = "";
        for (int i = 0; i < fragments.size(); ++i)
            s = s + fragments.get(i).print() + "\n";
        return s;
    }

    public void adjust() {
        for (int i = 0; i < fragments.size(); ++i)
            fragments.get(i).adjust();
    }

    public void dynamicAllocation() {
        Register.initialize();
        dynamic = new ArrayList<Quadruple>();
        for (int i = 0; i < initializer.size(); ++i)
            initializer.get(i).dynamicTranslate(dynamic);
        for (int i = 0; i < Register.numOfReg; ++i)
            Register.releaseGlobal(dynamic, i);

        Register.initialize();
        for (int i = 0; i < fragments.size(); ++i)
            fragments.get(i).dynamicAllocation();
    }

    public boolean staticAllocation() {
        Register.initialize();
        dynamic = new ArrayList<Quadruple>();
        for (int i = 0; i < initializer.size(); ++i)
            initializer.get(i).dynamicTranslate(dynamic);
        for (int i = 0; i < Register.numOfReg; ++i)
            Register.releaseGlobal(dynamic, i);

        for (int i = 0; i < fragments.size(); ++i) {
            if (!fragments.get(i).coloring())
                return false;
            /*for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
                HashMap.Entry entry = (HashMap.Entry) iter.next();
                Temp key = (Temp) entry.getKey();
                Register register = (Register) entry.getValue();
                System.out.println(key.print() + " " + register.print());
            }*/
        }
        for (int i = 0; i < fragments.size(); ++i) {
            fragments.get(i).staticAllocation();
        }
        return true;
    }

    public String dynamicToString() {
        String s = "\t.data\n";
        for (int i = 0; i < variables.size(); ++i)
            s = s + "var" + variables.get(i).name + ":\n\t.word 0\n";
        for (int i = 0; i < StringAddressConst.list.size(); ++i)
            s = s + "str" + StringAddressConst.list.get(i).num + ":\n\t.word " + StringAddressConst.list.get(i).value.getBytes().length + "\n\t.align 2\n\t.asciiz \"" + StringAddressConst.list.get(i).original + "\"\n";
        s = s +"\t.text\n\t.globl main\n";

        s = s + "main:\n";
        int theSize = size + 32;
        s = s + "subu $sp, $sp, " + theSize * 4 + "\n";
        s = s + "sw\t" + "$ra, " + (size + Register.getNum("$ra")) * 4 + "($sp)" + "\n";

        for (int i = 0; i < dynamic.size(); ++i) {
            s = s + dynamic.get(i).generate();
            if (dynamic.get(i) instanceof Label)
                s = s + ":\n";
        }

        s = s + "subu $sp, $sp, " + (32 + mainFunction.size) * 4 + "\n";
        s = s + "jal " + "main_enter\n";

        s = s + "addu $sp, $sp, " + (32 + mainFunction.size) * 4 + "\n";
        s = s + "lw\t" + "$ra, " + (size + Register.getNum("$ra")) * 4 + "($sp)" + "\n";
        s = s + "addu $sp, $sp, " + theSize * 4 + "\n";

        //print the returned value
        /*s = s + "move\t$a0, $v0\n";
        s = s + "li\t$v0, 1\n";
        s = s + "syscall\n";*/
        //ended here.

        s = s + "jr\t" + "$ra" + "\n";
        //main ended here.

        s = s + IR.getServices();

        for (int i = 0; i < fragments.size(); ++i)
            s = s + fragments.get(i).dynamicToString();
        return s;
    }

    public String staticToString() {
        String s = "\t.data\n";
        for (int i = 0; i < variables.size(); ++i)
            s = s + "var" + variables.get(i).name + ":\n\t.word 0\n";
        for (int i = 0; i < StringAddressConst.list.size(); ++i)
            s = s + "str" + StringAddressConst.list.get(i).num + ":\n\t.word " + StringAddressConst.list.get(i).value.getBytes().length + "\n\t.align 2\n\t.asciiz \"" + StringAddressConst.list.get(i).original + "\"\n";
        s = s +"\t.text\n\t.globl main\n";

        s = s + "main:\n";
        int theSize = 32;
        s = s + "subu $sp, $sp, " + theSize * 4 + "\n";
        s = s + "sw\t" + "$ra, " + (Register.getNum("$ra")) * 4 + "($sp)" + "\n";

        for (int i = 0; i < dynamic.size(); ++i) {
            s = s + dynamic.get(i).generate();
            if (dynamic.get(i) instanceof Label)
                s = s + ":\n";
        }

        s = s + "subu $sp, $sp, " + 32 * 4 + "\n";
        s = s + "jal " + "main_enter\n";

        s = s + "addu $sp, $sp, " + 32 * 4 + "\n";
        s = s + "lw\t" + "$ra, " + (Register.getNum("$ra")) * 4 + "($sp)" + "\n";
        s = s + "addu $sp, $sp, " + theSize * 4 + "\n";

        //print the returned value
        /*s = s + "move\t$a0, $v0\n";
        s = s + "li\t$v0, 1\n";
        s = s + "syscall\n";*/
        //ended here.

        s = s + "jr\t" + "$ra" + "\n";
        //main ended here.

        s = s + IR.getServices();

        for (int i = 0; i < fragments.size(); ++i)
            s = s + fragments.get(i).staticToString();
        return s;
    }

    public static String getServices() {
        String s = "";
        //service 0(new)
        s = s + "new:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 1(print)
        s = s + "print:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 4\n" +
                "addu\t$a0, $a0, 4\n" +
                "syscall\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 2(getString)
        s = s + "getString:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 9\n" +
                "li\t$a0, 8192\n" +
                "syscall\n" +
                "addu\t$a0, $v0, 4\n" +
                "li\t$a1, 8188\n" +
                "li\t$v0, 8\n" +
                "syscall\n" +
                "subu\t$v0, $a0, 4\n" +
                "getString_label:\n" +
                "lb\t$a1, 0($a0)\n" +
                "addu\t$a0, $a0, 1\n" +
                "bnez\t$a1, getString_label\n" +
                "subu\t$a0, $a0, $v0\n" +
                "subu\t$a0, $a0, 5\n" +
                "sw\t$a0, 0($v0)\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 3
        s = s + "getInt:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$a1, 0\n" +
                "getInt_label1:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "beq\t$v0, 45, getInt_label2\n" +
                "bge\t$v0, 48, getInt_label3\n" +
                "j\tgetInt_label1\n" +
                "getInt_label2:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "li\t$a1, 1\n" +
                "getInt_label3:\n" +
                "sub\t$a0, $v0, 48\n" +
                "getInt_label6:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "blt\t$v0, 48, getInt_label4\n" +
                "sub\t$v0, $v0, 48\n" +
                "mul\t$a0, $a0, 10\n" +
                "add\t$a0, $a0, $v0\n" +
                "j getInt_label6\n" +
                "getInt_label4:\n" +
                "move\t$v0, $a0\n" +
                "beq\t$a1, 0, getInt_label5\n" +
                "neg\t$v0, $v0\n" +
                "getInt_label5:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 4
        s = s + "toString:\n" +
                "sw\t$ra, 124($sp)\n" +
                "move\t$a1, $a0\n" +
                "li\t$a2, 0\n" +
                "toString_label1:\n" +
                "div\t$a1, $a1, 10\n" +
                "addu\t$a2, $a2, 1\n" +
                "bnez\t$a1, toString_label1\n" +
                "move\t$a1, $a0\n" +
                "move\t$a0, $a2\n" +
                "addu\t$a0, $a0, 9\n" +
                "divu\t$a0, $a0, 4\n" +
                "mulou\t$a0, $a0, 4\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "bltz\t$a1, toString_label2\n" +
                "sw\t$a2, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "addu\t$a0, $a0, $a2\n" +
                "j\ttoString_label3\n" +
                "toString_label2:\n" +
                "abs\t$a1, $a1\n" +
                "addu\t$a2, $a2, 1\n" +
                "sw\t$a2, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "li\t$a3, 45\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a0, $a0, $a2\n" +
                "toString_label3:\n" +
                "li\t$a2, 0\n" +
                "sb\t$a2, 0($a0)\n" +
                "toString_label4:\n" +
                "subu\t$a0, $a0, 1\n" +
                "rem\t$a3, $a1, 10\n" +
                "addu $a3, $a3, 48\n" +
                "sb\t$a3, 0($a0)\n" +
                "div\t$a1, $a1, 10\n" +
                "bnez\t$a1, toString_label4\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 5
        s = s + "substring:\n" +
                "sw\t$ra, 124($sp)\n" +
                "addu\t$a1, $a1, 4\n" +
                "addu\t$a1, $a1, $a0\n" +
                "addu\t$a2, $a2, 4\n" +
                "addu\t$a2, $a2, $a0\n" +
                "li\t$v0, 9\n" +
                "subu\t$a0, $a2, $a1\n" +
                "addu\t$a0, $a0, 9\n" +
                "divu\t$a0, $a0, 4\n" +
                "mulou\t$a0, $a0, 4\n" +
                "syscall\n" +
                "subu\t$a3, $a2, $a1\n" +
                "addu\t$a3, $a3, 1\n" +
                "sw\t$a3, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "substring_label1:\n" +
                "bgt\t$a1, $a2, substring_label2\n" +
                "lb\t$a3, 0($a1)\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a1, $a1, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j substring_label1\n" +
                "substring_label2:\n" +
                "li\t$a3, 0\n" +
                "sb\t$a3, 0($a0)\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 6
        s = s + "parseInt:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$a1, 0\n" +
                "addu\t$a2, $a0, 4\n" +
                "lb\t$a3, 0($a2)\n" +
                "bge\t$a3, 48, parseInt_label1\n" +
                "addu\t$a2, $a2, 1\n" +
                "lb\t$a3, 0($a2)\n" +
                "li\t$a1, 1\n" +
                "parseInt_label1:\n" +
                "sub\t$a0, $a3, 48\n" +
                "parseInt_label2:\n" +
                "addu\t$a2, $a2, 1\n" +
                "lb\t$a3, 0($a2)\n" +
                "blt\t$a3, 48, parseInt_label3\n" +
                "bgt\t$a3, 57, parseInt_label3\n" +
                "sub\t$a3, $a3, 48\n" +
                "mul\t$a0, $a0, 10\n" +
                "add\t$a0, $a0, $a3\n" +
                "j parseInt_label2\n" +
                "parseInt_label3:\n" +
                "move\t$v0, $a0\n" +
                "beq\t$a1, 0, parseInt_label4\n" +
                "neg\t$v0, $v0\n" +
                "parseInt_label4:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 7
        s = s + "stringAdd:\n" +
                "sw\t$ra, 124($sp)\n" +
                "lw\t$a2, 0($a0)\n" +
                "lw\t$a3, 0($a1)\n" +
                "add\t$a3, $a2, $a3\n" +
                "move\t$a2, $a0\n" +
                "move\t$a0, $a3\n" +
                "add\t$a0, $a0, 8\n" +
                "div\t$a0, $a0, 4\n" +
                "mul\t$a0, $a0, 4\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "sw\t$a3, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "addu\t$a2, $a2, 4\n" +
                "stringAdd_label1:\n" +
                "lb\t$a3, 0($a2)\n" +
                "beqz\t$a3, stringAdd_label2\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a2, $a2, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j\tstringAdd_label1\n" +
                "stringAdd_label2:\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringAdd_label3:\n" +
                "lb\t$a3, 0($a1)\n" +
                "beqz\t$a3, stringAdd_label4\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a1, $a1, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j\tstringAdd_label3\n" +
                "stringAdd_label4:\n" +
                "li\t$a3, 0\n" +
                "sb\t$a3, 0($a0)\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 8
        s = s + "stringEquals:\n" +
                "sw\t$ra, 124($sp)\n" +
                "lw\t$a2, 0($a0)\n" +
                "lw\t$a3, 0($a1)\n" +
                "bne\t$a2, $a3, stringEquals_neq\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringEquals_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "bne\t$a2, $a3, stringEquals_neq\n" +
                "beq\t$a2, 0, stringEquals_eq\n" +
                "j\tstringEquals_start\n" +
                "stringEquals_eq:\n" +
                "li\t$v0, 1\n" +
                "j stringEquals_end\n" +
                "stringEquals_neq:\n" +
                "li\t$v0, 0\n" +
                "stringEquals_end:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 9
        s = s + "stringLessThan:\n" +
                "sw\t$ra, 124($sp)\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringLessThan_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "add\t$v0, $a2, $a3\n" +
                "beq\t$v0, 0, stringLessThan_no\n" +
                "beq\t$a2, 0, stringLessThan_yes\n" +
                "beq\t$a3, 0, stringLessThan_no\n" +
                "blt\t$a2, $a3, stringLessThan_yes\n" +
                "bgt\t$a2, $a3, stringLessThan_no\n" +
                "j\tstringLessThan_start\n" +
                "stringLessThan_yes:\n" +
                "li\t$v0, 1\n" +
                "j stringLessThan_end\n" +
                "stringLessThan_no:\n" +
                "li\t$v0, 0\n" +
                "stringLessThan_end:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 10
        s = s + "stringLessThanOrEquals:\n" +
                "sw\t$ra, 124($sp)\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringLessThanOrEquals_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "add\t$v0, $a2, $a3\n" +
                "beq\t$v0, 0, stringLessThanOrEquals_yes\n" +
                "beq\t$a2, 0, stringLessThanOrEquals_yes\n" +
                "beq\t$a3, 0, stringLessThanOrEquals_no\n" +
                "blt\t$a2, $a3, stringLessThanOrEquals_yes\n" +
                "bgt\t$a2, $a3, stringLessThanOrEquals_no\n" +
                "j\tstringLessThanOrEquals_start\n" +
                "stringLessThanOrEquals_yes:\n" +
                "li\t$v0, 1\n" +
                "j stringLessThanOrEquals_end\n" +
                "stringLessThanOrEquals_no:\n" +
                "li\t$v0, 0\n" +
                "stringLessThanOrEquals_end:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";
        return s;
    }
}
