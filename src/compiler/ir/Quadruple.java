package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Quadruple {
    abstract public String print();

    abstract public void dynamicTranslate(List<Quadruple> list);

    abstract public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color);

    abstract public String generate();

    abstract public void read(ArrayList<Temp> list);

    abstract public void write(ArrayList<Temp> list);

    abstract public Quadruple replaceUse(Temp from, Address to);

    abstract public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping);

    abstract public Quadruple replaceDefine(Temp from, Address to);

    abstract public void used(List<RenamedTemp> list);

    abstract public void defined(List<RenamedTemp> list);

    abstract public Quadruple SSADestruction();

    public boolean useless = false;
}
