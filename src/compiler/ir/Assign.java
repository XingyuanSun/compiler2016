package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Assign extends Quadruple {
    public Address dest;//Register
    public Address src;//IntegerConst or Register

    public Assign(Address dest, Address src) {
        this.dest = dest;
        this.src = src;
    }

    public String print() {
        if (src instanceof StringAddressConst)
            return dest.print() + " = " + "move" + " " + "\"" + ((StringAddressConst) src).original + "\"";
        else
            return dest.print() + " = " + "move" + " " + src.print();
    }

    public void read(ArrayList<Temp> list) {
        if (src instanceof Temp)
            list.add((Temp) src);
    }

    public void used(List<RenamedTemp> list) {
        if (src instanceof RenamedTemp)
            list.add((RenamedTemp) src);
    }

    public void write(ArrayList<Temp> list) {
        if (dest instanceof Temp)
            list.add((Temp) dest);
    }

    public void defined(List<RenamedTemp> list) {
        if (dest instanceof RenamedTemp)
            list.add((RenamedTemp) dest);
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Dest, Src;
        if (src instanceof Temp)
            Src = Register.Allocate(list, (Temp) src, true);
        else
            Src = src;

        if (dest instanceof Temp)
            Dest = Register.Allocate(list, (Temp) dest, false, Src);
        else
            Dest = dest;

        list.add(new Assign(Dest, Src));
        if (Dest instanceof Register)
            Register.modify((Register) Dest);
        //System.out.println(dest.print() + " " + Dest.generate() + " " + src.print());
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Dest, Src;
        if (src instanceof Temp)
            Src = Temp.get((Temp) src, color);
        else
            Src = src;

        if (dest instanceof Temp)
            Dest = Temp.get((Temp) dest, color);
        else
            Dest = dest;

        list.add(new Assign(Dest, Src));
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Dest, Src;
        if (src instanceof Temp && from.equal((Temp) src))
            Src = to;
        else
            Src = src;
        Dest = dest;
        return new Assign(Dest, Src);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (src instanceof RenamedTemp)
            src = mapping.get(src);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Dest, Src;
        Src = src;
        if (dest instanceof Temp && from.equal((Temp) dest))
            Dest = to;
        else
            Dest = dest;
        return new Assign(Dest, Src);
    }

    public Quadruple SSADestruction() {
        Address Dest, Src;
        if (src instanceof RenamedTemp)
            Src = ((RenamedTemp) src).temp;
        else
            Src = src;

        if (dest instanceof RenamedTemp)
            Dest = ((RenamedTemp) dest).temp;
        else
            Dest = dest;

        return new Assign(Dest, Src);
    }

    public String generate() {
        if (src instanceof StringAddressConst)
            return "la\t" + dest.generate() + ", " + "str" + ((StringAddressConst) src).num + "\n";
        else if (src instanceof IntegerConst)
            return "li\t" + dest.generate() + ", " + src.generate() + "\n";
        else
            return "move\t" + dest.generate() + ", " + src.generate() + "\n";
    }
}
