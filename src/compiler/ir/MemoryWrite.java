package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MemoryWrite extends Quadruple {
    public Address dest;
    public Address src;
    public IntegerConst offset;
    public int size = 4;

    public MemoryWrite(Address dest, IntegerConst offset, Address src) {
        this.dest = dest;
        this.offset = offset;
        this.src = src;
    }

    public String print() {
        return "store " + size + " " + dest.print() + " " + src.print() + " " + offset.print();
    }

    public void used(List<RenamedTemp> list) {
        if (src instanceof RenamedTemp)
            list.add((RenamedTemp) src);
        if (dest instanceof RenamedTemp)
            list.add((RenamedTemp) dest);
    }

    public void read(ArrayList<Temp> list) {
        if (src instanceof Temp)
            list.add((Temp) src);
        if (dest instanceof Temp)
            list.add((Temp) dest);
    }

    public void write(ArrayList<Temp> list) {
    }

    public void defined(List<RenamedTemp> list) {
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Dest, Src;
        if (src instanceof Temp)
            Src = Register.Allocate(list, (Temp) src, true);
        else
            Src = src;

        if (dest instanceof Temp)
            Dest = Register.Allocate(list, (Temp) dest, true, Src);
        else
            Dest = dest;

        list.add(new MemoryWrite(Dest, offset, Src));
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Dest, Src;
        if (src instanceof Temp)
            Src = Temp.get((Temp) src, color);
        else
            Src = src;

        if (dest instanceof Temp)
            Dest = Temp.get((Temp) dest, color);
        else
            Dest = dest;

        list.add(new MemoryWrite(Dest, offset, Src));
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Dest, Src;
        if (src instanceof Temp && from.equal((Temp) src))
            Src = to;
        else
            Src = src;

        if (dest instanceof Temp && from.equal((Temp) dest))
            Dest = to;
        else
            Dest = dest;
        return new MemoryWrite(Dest, offset, Src);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (src instanceof RenamedTemp)
            src = mapping.get(src);
        if (dest instanceof RenamedTemp)
            dest = mapping.get(dest);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Dest, Src;
        Src = src;
        Dest = dest;
        return new MemoryWrite(Dest, offset, Src);
    }

    public Quadruple SSADestruction() {
        Address Dest, Src;
        if (src instanceof RenamedTemp)
            Src = ((RenamedTemp) src).temp;
        else
            Src = src;
        if (dest instanceof RenamedTemp)
            Dest = ((RenamedTemp) dest).temp;
        else
            Dest = dest;
        return new MemoryWrite(Dest, offset, Src);
    }


    public String generate() {
        String s =  "sw\t" + src.generate() + ", ";
        if (dest instanceof Temp) {
            if (offset.value != 0)
                System.out.print("MemoryWrite ERROR!\n");
            if (((Temp) dest).name != null)
                s = s + "var" + ((Temp) dest).name + "\n";
                //s = s + ((Temp) dest).name + "($gp)" + "\n";
            else
                s = s + ((Temp) dest).num * 4 + "($sp)" + "\n";
        }
        else {
            s = s + offset.value + "(" + dest.generate() + ")" + "\n";
        }
        return s;
    }
}
