package compiler.ir;
import compiler.cfg.RenamedTemp;

import java.util.*;

public class Call extends Quadruple {
    public Address returnValue;
    public Function callee;
    public List<Address> params;
    public boolean[] a;
    public boolean isVoid;
    public boolean[] local;
    public HashSet<Temp>[] global;
    public boolean isStatic;

    public Call(Address returnValue, Function callee, List<Address> params, boolean isVoid) {
        this.returnValue = returnValue;
        this.callee = callee;
        this.params = params;
        this.isVoid = isVoid;
        this.a = new boolean[4];
    }

    public String print() {
        String s = "";
        for (int i = 0; i < params.size(); ++i)
            s = s + " " + params.get(i).print();
        if (isVoid)
            return "call" + " "  + callee.name + s;
        else
            return returnValue.print() + " = call" + " "  + callee.name + s;
    }

    public void used(List<RenamedTemp> list) {
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof RenamedTemp)
                list.add((RenamedTemp) params.get(i));
    }

    public void read(ArrayList<Temp> list) {
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof Temp)
                list.add((Temp) params.get(i));
    }

    public void write(ArrayList<Temp> list) {
        if (returnValue instanceof Temp)
            list.add((Temp) returnValue);
    }

    public void defined(List<RenamedTemp> list) {
        if (returnValue instanceof RenamedTemp)
            list.add((RenamedTemp) returnValue);
    }

    public void dynamicTranslate(List<Quadruple> list) {
        for (int i = 0; i < Register.numOfReg; ++i)
            Register.release(list, i);
        Address ReturnValue;

        if (returnValue instanceof Temp)
            ReturnValue = Register.Allocate(list, (Temp) returnValue, false); //We only need to store ReturnValue in it.
        else
            ReturnValue = returnValue;

        Call n = new Call(ReturnValue, callee, params, isVoid);
        for (int i = 0; i < 4; ++i) {
            if (Register.reference[Register.numOfReg + i] == null)
                n.a[i] = false;
            else
                n.a[i] = true;
        }
        list.add(n);
        if (ReturnValue instanceof Register)
            Register.modify((Register) ReturnValue);
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        /*for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            Temp key = (Temp) entry.getKey();
            Register register = (Register) entry.getValue();
            if (key.name != null)
                list.add(new MemoryWrite(key, new IntegerConst(0), register));
        }*/
        Address ReturnValue;
        List<Address> Params = new ArrayList<Address>();
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof Temp && ((Temp)(params.get(i))).name == null)
                Params.add(Temp.get((Temp) params.get(i), color));
            else
                Params.add(params.get(i));
        if (returnValue instanceof Temp)
            ReturnValue = Temp.get((Temp) returnValue, color);
        else
            ReturnValue = returnValue;
        Call call = new Call(ReturnValue, callee, Params, isVoid);
        call.isStatic = true;
        call.local = new boolean[32];
        call.global = new HashSet[32];
        for (int i = 0; i < 32; ++i)
            call.global[i] = new HashSet<Temp>();
        for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            if (((Temp) entry.getKey()).name == null)
                call.local[Register.getNum(((Register) entry.getValue()).print())] = true;
            else
                call.global[Register.getNum(((Register) entry.getValue()).print())].add((Temp) entry.getKey());
        }
        list.add(call);
        /*for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            Temp key = (Temp) entry.getKey();
            Register register = (Register) entry.getValue();
            if (key.name != null)
                list.add(new MemoryRead(register, key, new IntegerConst(0)));
        }*/
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address ReturnValue;
        List<Address> Params = new ArrayList<>();
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof Temp && from.equal((Temp) params.get(i)))
                Params.add(to);
            else
                Params.add(params.get(i));
        ReturnValue = returnValue;
        return new Call(ReturnValue, callee, Params, isVoid);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof RenamedTemp)
                params.set(i, mapping.get(params.get(i)));
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address ReturnValue;
        if (returnValue instanceof Temp && from.equal((Temp) returnValue))
            ReturnValue = to;
        else
            ReturnValue = returnValue;
        return new Call(ReturnValue, callee, params, isVoid);
    }

    public Quadruple SSADestruction() {
        Address ReturnValue;
        List<Address> Params = new ArrayList<>();
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof RenamedTemp)
                Params.add(((RenamedTemp) params.get(i)).temp);
            else
                Params.add(params.get(i));
        if (returnValue instanceof RenamedTemp)
            ReturnValue = ((RenamedTemp) returnValue).temp;
        else
            ReturnValue = returnValue;
        return new Call(ReturnValue, callee, Params, isVoid);
    }

    public String generate() {
        String s = "";
        if (!isStatic) {
            int theSize = callee.size + 32;

            for (int i = 0; i < 4; ++i)
                if (a[i])
                    s = s + "sw\t" + "$a" + i + ", " + i * 4 + "($sp)" + "\n";
            s = s + "subu\t$sp, $sp, " + theSize * 4 + "\n";

            for (int i = 0; i < params.size() && i < 4; ++i) {
                if (params.get(i) instanceof IntegerConst) {
                    s = s + "li\t" + "$a" + i + ", " + params.get(i).generate() + "\n";
                } else {
                    if (((Temp) params.get(i)).name != null)
                        s = s + "lw\t" + "$a" + i + ", " + "var" + ((Temp) params.get(i)).name + "\n";
                    else
                        s = s + "lw\t" + "$a" + i + ", " + (((Temp) params.get(i)).num + theSize) * 4 + "($sp)" + "\n";
                }
            }

            for (int i = 4; i < params.size(); ++i) {
                if (params.get(i) instanceof IntegerConst) {
                    s = s + "li\t$t0, " + params.get(i).generate() + "\n";
                } else {
                    if (((Temp) params.get(i)).name != null)
                        s = s + "lw\t" + "$t0, " + "var" + ((Temp) params.get(i)).name + "\n";
                    else
                        s = s + "lw\t" + "$t0, " + (((Temp) params.get(i)).num + theSize) * 4 + "($sp)" + "\n";
                }
                s = s + "sw\t$t0, " + i * 4 + "($sp)" + "\n";
            }

            s = s + "jal\t" + callee.name + "_enter\n";

            s = s + "addu\t$sp, $sp, " + theSize * 4 + "\n";
            for (int i = 0; i < 4; ++i)
                if (a[i])
                    s = s + "lw\t" + "$a" + i + ", " + i * 4 + "($sp)" + "\n";
        }
        else {
            int theSize = 32;

            for (int i = 4; i < 26; ++i) {
                if (local[i])
                    s = s + "sw\t" + Register.getName(i) + ", " + i * 4 + "($sp)" + "\n";
                for (Iterator<Temp> iter = global[i].iterator(); iter.hasNext(); ){
                    s = s + "sw\t" + Register.getName(i) + ", var" + iter.next().name + "\n";
                }
            }

            s = s + "subu\t$sp, $sp, " + theSize * 4 + "\n";

            for (int i = 0; i < params.size(); ++i)
                if (params.get(i) instanceof IntegerConst)
                    s = s + "li\t" + callee.static_args.get(i).generate() + ", " + params.get(i).generate() + "\n";
                else if (params.get(i) instanceof Register)
                    s = s + "lw\t" + callee.static_args.get(i).generate() + ", " + (Register.getNum(((Register) params.get(i)).print()) + theSize) * 4 + "($sp)" + "\n";
                else
                    s = s + "lw\t" + callee.static_args.get(i).generate() + ", var" + ((Temp) params.get(i)).name + "\n";

            s = s + "jal\t" + callee.name + "_enter\n";

            s = s + "addu\t$sp, $sp, " + theSize * 4 + "\n";

            for (int i = 4; i < 26; ++i) {
                if (local[i])
                    s = s + "lw\t" + Register.getName(i) + ", " + i * 4 + "($sp)" + "\n";
                for (Iterator<Temp> iter = global[i].iterator(); iter.hasNext(); ){
                    s = s + "lw\t" + Register.getName(i) + ", var" + iter.next().name + "\n";
                }
            }
        }
        if (!isVoid) {
            s = s + "move\t" + returnValue.generate() + ", $v0\n";
        }
        return s;
    }
}
