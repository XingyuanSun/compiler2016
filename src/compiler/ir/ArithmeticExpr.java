package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ArithmeticExpr extends Quadruple {
    public ArithmeticOp op;
    public Address dest;
    public Address src1;//must be a Register
    public Address src2;

    public ArithmeticExpr(Address dest, Address src1, ArithmeticOp op, Address src2) {
        if (src1 instanceof IntegerConst)
            System.out.print("ArithmeticExpr ERROR\n");
        this.dest = dest;
        this.src1 = src1;
        this.op = op;
        this.src2 = src2;
    }

    public ArithmeticExpr(Address dest, ArithmeticOp op, Address src1) {
        if (src1 instanceof IntegerConst)
            System.out.print("ArithmeticExpr ERROR\n");
        this.dest = dest;
        this.op = op;
        this.src1 = src1;
        this.src2 = null;
    }

    public String print() {
        String opr = "";
        switch (op) {
            case ADD: opr = "add"; break;
            case SUB: opr = "sub"; break;
            case MUL: opr = "mul"; break;
            case DIV: opr = "div"; break;
            case MOD: opr = "rem"; break;
            case SHL: opr = "shl"; break;
            case SHR: opr = "shr"; break;
            case AND: opr = "and"; break;
            case OR: opr = "or"; break;
            case XOR: opr = "xor"; break;
            case MINUS: opr = "neg"; break;
            case TILDE: opr = "not"; break;
            case BOOLNOT: return dest.print() + " = " + "xor" + " " + src1.print() + " " + (new IntegerConst(1)).print();
        }
        if (src2 == null)
            return dest.print() + " = " + opr + " " + src1.print();
        else
            return dest.print() + " = " + opr + " " + src1.print() + " " + src2.print();
    }

    public void read(ArrayList<Temp> list) {
        if (src1 instanceof Temp)
            list.add((Temp) src1);
        if (src2 instanceof Temp)
            list.add((Temp) src2);
    }

    public void used(List<RenamedTemp> list) {
        if (src1 instanceof RenamedTemp)
            list.add((RenamedTemp) src1);
        if (src2 instanceof RenamedTemp)
            list.add((RenamedTemp) src2);
    }

    public void write(ArrayList<Temp> list) {
        if (dest instanceof Temp)
            list.add((Temp) dest);
    }

    public void defined(List<RenamedTemp> list) {
        if (dest instanceof RenamedTemp)
            list.add((RenamedTemp) dest);
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Dest, Src1, Src2;
        if (src1 instanceof Temp)
            Src1 = Register.Allocate(list, (Temp) src1, true);
        else
            Src1 = src1;
        if (src2 instanceof Temp)
            Src2 = Register.Allocate(list, (Temp) src2, true, Src1);
        else
            Src2 = src2;

        if (dest instanceof Temp)
            Dest = Register.Allocate(list, (Temp) dest, false, Src1, Src2);
        else
            Dest = dest;

        if (src2 != null) {
            list.add(new ArithmeticExpr(Dest, Src1, op, Src2));
        }
        else {
            list.add(new ArithmeticExpr(Dest, op, Src1));
        }
        if (Dest instanceof Register)
            Register.modify((Register) Dest);
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Dest, Src1, Src2;
        if (src1 instanceof Temp) {
            Src1 = Temp.get((Temp) src1, color);
            /*if (((Temp) src1).name != null)
                list.add(new MemoryRead(Src1, src1, new IntegerConst(0)));*/
        }
        else
            Src1 = src1;

        if (src2 instanceof Temp) {
            Src2 = Temp.get((Temp) src2, color);
            /*if (((Temp) src2).name != null)
                list.add(new MemoryRead(Src2, src2, new IntegerConst(0)));*/
        }
        else
            Src2 = src2;

        if (dest instanceof Temp)
            Dest = Temp.get((Temp) dest, color);
        else
            Dest = dest;

        if (src2 != null) {
            list.add(new ArithmeticExpr(Dest, Src1, op, Src2));
        }
        else {
            list.add(new ArithmeticExpr(Dest, op, Src1));
        }
        /*if (dest instanceof Temp)
            if (((Temp) dest).name != null)
                list.add(new MemoryWrite(dest, new IntegerConst(0), Dest));*/
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Dest, Src1, Src2;
        if (src1 instanceof Temp && from.equal((Temp) src1))
            Src1 = to;
        else
            Src1 = src1;
        if (src2 instanceof Temp && from.equal((Temp) src2))
            Src2 = to;
        else
            Src2 = src2;
        Dest = dest;

        if (src2 != null)
            return new ArithmeticExpr(Dest, Src1, op, Src2);
        else
            return new ArithmeticExpr(Dest, op, Src1);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (src1 instanceof RenamedTemp)
            src1 = mapping.get(src1);
        if (src2 instanceof RenamedTemp)
            src2 = mapping.get(src2);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Dest, Src1, Src2;
        Src1 = src1;
        Src2 = src2;
        if (dest instanceof Temp && from.equal((Temp) dest))
            Dest = to;
        else
            Dest = dest;

        if (src2 != null)
            return new ArithmeticExpr(Dest, Src1, op, Src2);
        else
            return new ArithmeticExpr(Dest, op, Src1);
    }

    public Quadruple SSADestruction() {
        Address Dest, Src1, Src2;
        if (src1 instanceof RenamedTemp)
            Src1 = ((RenamedTemp) src1).temp;
        else
            Src1 = src1;
        if (src2 instanceof RenamedTemp)
            Src2 = ((RenamedTemp) src2).temp;
        else
            Src2 = src2;

        if (dest instanceof RenamedTemp)
            Dest = ((RenamedTemp) dest).temp;
        else
            Dest = dest;

        if (src2 != null)
            return new ArithmeticExpr(Dest, Src1, op, Src2);
        else
            return new ArithmeticExpr(Dest, op, Src1);
    }

    public String generate() {
        String opr = "";
        switch (op) {
            case ADD: opr = "add"; break;
            case SUB: opr = "sub"; break;
            case MUL: opr = "mul"; break;
            case DIV: opr = "div"; break;
            case MOD: opr = "rem"; break;
            case SHL: opr = "sll"; break;
            case SHR: opr = "sra"; break;
            case AND: opr = "and"; break;
            case OR: opr = "or"; break;
            case XOR: opr = "xor"; break;
            case MINUS: opr = "neg"; break;
            case TILDE: opr = "not"; break;
            case BOOLNOT: return "xor\t" + dest.generate() + ", " + src1.generate() +  ", 1\n";
        }
        if (src2 == null)
            return opr + " " + dest.generate() + ", " + src1.generate() + "\n";
        else
            return opr + " " + dest.generate() + ", " + src1.generate() + ", " + src2.generate() + "\n";
    }
}
