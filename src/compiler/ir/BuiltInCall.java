package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.*;

/**
 * Created by 孙星远 on 2016/4/30.
 */
public class BuiltInCall extends Quadruple {
    public Address returnValue;
    public int service;
    public List<Address> params;
    public boolean[] a;
    public boolean[] local;
    public HashSet<Temp>[] global;
    public boolean isStatic;

    public BuiltInCall(Address returnValue, int service) {
        this.returnValue = returnValue;
        this.service = service;
        this.params = new ArrayList<Address>();
        this.a = new boolean[4];
    }

    public BuiltInCall(Address returnValue, int service, Address param) {
        this.returnValue = returnValue;
        this.service = service;
        this.params = new ArrayList<Address>();
        this.params.add(param);
        this.a = new boolean[4];
    }

    public BuiltInCall(Address returnValue, int service, List<Address> params) {
        this.returnValue = returnValue;
        this.service = service;
        this.params = params;
        this.a = new boolean[4];
    }

    public String print() {
        String name = "";
        switch (service) {
            case 0:
                name = "new";
                break;
            case 1:
                name = "print";
                break;
            case 2:
                name = "getString";
                break;
            case 3:
                name = "getInt";
                break;
            case 4:
                name = "toString";
                break;
            case 5:
                name = "substring";
                break;
            case 6:
                name = "parseInt";
                break;
            case 7:
                name = "stringAdd";
                break;
            case 8:
                name = "stringEquals";
                break;
            case 9:
                name = "stringLessThan";
                break;
            case 10:
                name = "stringLessThanOrEquals";
                break;
        }
        String s = "";
        for (int i = 0; i < params.size(); ++i)
            s = s + " " + params.get(i).print();
        if (returnValue == null)
            return "call" + " "  + name + s;
        else
            return returnValue.print() + " = call" + " "  + name + s;
    }

    public void used(List<RenamedTemp> list) {
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof RenamedTemp)
                list.add((RenamedTemp) params.get(i));
    }

    public void read(ArrayList<Temp> list) {
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof Temp)
                list.add((Temp) params.get(i));
    }

    public void write(ArrayList<Temp> list) {
        if (returnValue instanceof Temp)
            list.add((Temp) returnValue);
    }

    public void defined(List<RenamedTemp> list) {
        if (returnValue instanceof RenamedTemp)
            list.add((RenamedTemp) returnValue);
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address ReturnValue;//Only use $a0, $a1, $a2, $a3, $v0

        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof Temp)
                Register.release(list, (Temp) params.get(i));
        if (returnValue instanceof Temp)
            ReturnValue = Register.Allocate(list, (Temp) returnValue, false); //We only need to store ReturnValue in it.
        else
            ReturnValue = returnValue;

        BuiltInCall n = new BuiltInCall(ReturnValue, service, params);
        for (int i = 0; i < 4; ++i) {
            if (Register.reference[Register.numOfReg + i] == null)
                n.a[i] = false;
            else
                n.a[i] = true;
        }
        list.add(n);
        if (ReturnValue instanceof Register)
            Register.modify((Register) ReturnValue);
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        /*for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            Temp key = (Temp) entry.getKey();
            Register register = (Register) entry.getValue();
            if (key.name != null)
                list.add(new MemoryWrite(key, new IntegerConst(0), register));
        }*/
        Address ReturnValue;
        List<Address> Params = new ArrayList<Address>();
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof Temp && ((Temp)(params.get(i))).name == null)
                Params.add(Temp.get((Temp) params.get(i), color));
            else
                Params.add(params.get(i));

        if (returnValue instanceof Temp)
            ReturnValue = Temp.get((Temp) returnValue, color); //We only need to store ReturnValue in it.
        else
            ReturnValue = returnValue;
        BuiltInCall builtInCall = new BuiltInCall(ReturnValue, service, Params);
        builtInCall.isStatic = true;
        builtInCall.local = new boolean[32];
        builtInCall.global = new HashSet[32];
        for (int i = 0; i < 32; ++i)
            builtInCall.global[i] = new HashSet<Temp>();
        for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            if (((Temp) entry.getKey()).name == null)
                builtInCall.local[Register.getNum(((Register) entry.getValue()).print())] = true;
            else
                builtInCall.global[Register.getNum(((Register) entry.getValue()).print())].add((Temp) entry.getKey());
            //System.out.println(Register.getNum(((Register) entry.getValue()).print()));
        }
        list.add(builtInCall);
        /*for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            Temp key = (Temp) entry.getKey();
            Register register = (Register) entry.getValue();
            if (key.name != null)
                list.add(new MemoryRead(register, key, new IntegerConst(0)));
        }*/
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address ReturnValue;
        List<Address> Params = new ArrayList<>();
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof Temp && from.equal((Temp) params.get(i)))
                Params.add(to);
            else
                Params.add(params.get(i));
        ReturnValue = returnValue;
        return new BuiltInCall(ReturnValue, service, Params);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof RenamedTemp)
                params.set(i, mapping.get(params.get(i)));
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address ReturnValue;
        if (returnValue instanceof Temp && from.equal((Temp) returnValue))
            ReturnValue = to;
        else
            ReturnValue = returnValue;
        return new BuiltInCall(ReturnValue, service, params);
    }

    public Quadruple SSADestruction() {
        Address ReturnValue;
        List<Address> Params = new ArrayList<>();
        for (int i = 0; i < params.size(); ++i)
            if (params.get(i) instanceof RenamedTemp)
                Params.add(((RenamedTemp) params.get(i)).temp);
            else
                Params.add(params.get(i));
        if (returnValue instanceof RenamedTemp)
            ReturnValue = ((RenamedTemp) returnValue).temp;
        else
            ReturnValue = returnValue;
        return new BuiltInCall(ReturnValue, service, Params);
    }

    public String generate() {
        String s = "";
        int theSize = 32;
        if (!isStatic) {

            for (int i = 0; i < 4; ++i)
                if (a[i])
                    s = s + "sw\t" + "$a" + i + ", " + i * 4 + "($sp)" + "\n";

            s = s + "subu\t$sp, $sp, " + theSize * 4 + "\n";
            for (int i = 0; i < params.size(); ++i) {
                if (params.get(i) instanceof IntegerConst) {
                    s = s + "li\t" + "$a" + i + ", " + params.get(i).generate() + "\n";
                } else {
                    if (((Temp) params.get(i)).name != null)
                        s = s + "lw\t" + "$a" + i + ", " + "var" + ((Temp) params.get(i)).name + "\n";
                    else
                        s = s + "lw\t" + "$a" + i + ", " + (((Temp) params.get(i)).num + theSize) * 4 + "($sp)" + "\n";
                }
            }

            String name = "";
            switch (service) {
                case 0:
                    name = "new";
                    break;
                case 1:
                    name = "print";
                    break;
                case 2:
                    name = "getString";
                    break;
                case 3:
                    name = "getInt";
                    break;
                case 4:
                    name = "toString";
                    break;
                case 5:
                    name = "substring";
                    break;
                case 6:
                    name = "parseInt";
                    break;
                case 7:
                    name = "stringAdd";
                    break;
                case 8:
                    name = "stringEquals";
                    break;
                case 9:
                    name = "stringLessThan";
                    break;
                case 10:
                    name = "stringLessThanOrEquals";
                    break;
            }
            s = s + "jal\t" + name + "\n";

            s = s + "addu\t$sp, $sp, " + theSize * 4 + "\n";
            for (int i = 0; i < 4; ++i)
                if (a[i])
                    s = s + "lw\t" + "$a" + i + ", " + i * 4 + "($sp)" + "\n";
        }
        else {
            for (int i = 4; i < 26; ++i) {
                if (local[i])
                    s = s + "sw\t" + Register.getName(i) + ", " + i * 4 + "($sp)" + "\n";
                for (Iterator<Temp> iter = global[i].iterator(); iter.hasNext(); ){
                    s = s + "sw\t" + Register.getName(i) + ", var" + iter.next().name + "\n";
                }
            }

            s = s + "subu\t$sp, $sp, " + theSize * 4 + "\n";
            for (int i = 0; i < params.size(); ++i) {
                if (params.get(i) instanceof IntegerConst)
                    s = s + "li\t" + "$a" + i + ", " + params.get(i).generate() + "\n";
                else if (params.get(i) instanceof Register)
                    s = s + "lw\t" + "$a" + i + ", " + (Register.getNum(((Register) params.get(i)).print()) + theSize) * 4 + "($sp)" + "\n";
                else
                    s = s + "lw\t" + "$a" + i + ", var" + ((Temp) params.get(i)).name + "\n";
            }

            String name = "";
            switch (service) {
                case 0:
                    name = "new";
                    break;
                case 1:
                    name = "print";
                    break;
                case 2:
                    name = "getString";
                    break;
                case 3:
                    name = "getInt";
                    break;
                case 4:
                    name = "toString";
                    break;
                case 5:
                    name = "substring";
                    break;
                case 6:
                    name = "parseInt";
                    break;
                case 7:
                    name = "stringAdd";
                    break;
                case 8:
                    name = "stringEquals";
                    break;
                case 9:
                    name = "stringLessThan";
                    break;
                case 10:
                    name = "stringLessThanOrEquals";
                    break;
            }
            s = s + "jal\t" + name + "\n";

            s = s + "addu\t$sp, $sp, " + theSize * 4 + "\n";
            for (int i = 4; i < 26; ++i) {
                if (local[i])
                    s = s + "lw\t" + Register.getName(i) + ", " + i * 4 + "($sp)" + "\n";
                for (Iterator<Temp> iter = global[i].iterator(); iter.hasNext(); ){
                    s = s + "lw\t" + Register.getName(i) + ", var" + iter.next().name + "\n";
                }
            }
        }

        if (returnValue != null) {
            s = s + "move\t" + returnValue.generate() + ", $v0\n";
        }
        return s;
    }
}
