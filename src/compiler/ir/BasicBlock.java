package compiler.ir;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Created by 孙星远 on 2016/5/3.
 */
public class BasicBlock {
    public List<Quadruple> data;
    public List<HashSet<Temp>> datain, dataout;
    public List<HashSet<Temp>> datadef, datause;
    public Label label;
    public BasicBlock succ1, succ2;
    public HashSet<Temp> in, out;
    public HashSet<Temp> def, use;

    public BasicBlock(Label label) {
        this.label = label;
        data = new ArrayList<Quadruple>();
        def = new HashSet<Temp>();
        use = new HashSet<Temp>();
        in = new HashSet<Temp>();
        out = new HashSet<Temp>();
        datain = new ArrayList<HashSet<Temp>>();
        dataout = new ArrayList<HashSet<Temp>>();
        datadef = new ArrayList<HashSet<Temp>>();
        datause = new ArrayList<HashSet<Temp>>();
    }

    public void initialize() {
        for (int i = 0; i < data.size(); ++i) {
            ArrayList<Temp> list = new ArrayList<Temp>();
            data.get(i).read(list);
            addUse(list);
            list = new ArrayList<Temp>();
            data.get(i).write(list);
            addDef(list);
        }
    }

    public void addDef(ArrayList<Temp> list) {
        for (int i = 0; i < list.size(); ++i)
            def.add(list.get(i));
    }

    public void addUse(ArrayList<Temp> list) {
        for (int i = 0; i < list.size(); ++i) {
            if (def.contains(list.get(i)))
                continue;
            use.add(list.get(i));
        }
    }

    public static void addDef(HashSet<Temp> def, ArrayList<Temp> list) {
        for (int i = 0; i < list.size(); ++i)
            def.add(list.get(i));
    }

    public static void addUse(HashSet<Temp> use, ArrayList<Temp> list, HashSet<Temp> def) {
        for (int i = 0; i < list.size(); ++i) {
            if (def.contains(list.get(i)))
                continue;
            use.add(list.get(i));
        }
    }

    public boolean addIn(HashSet<Temp> set, HashSet<Temp> except) {
        boolean modified = false;
        for (Iterator<Temp> iter = set.iterator(); iter.hasNext(); ) {
            Temp temp = iter.next();
            if (except != null && except.contains(temp))
                continue;
            if (!in.contains(temp))
                modified = true;
            in.add(temp);
        }
        return modified;
    }

    public static boolean addIn(HashSet<Temp> in, HashSet<Temp> set, HashSet<Temp> except) {
        boolean modified = false;
        for (Iterator<Temp> iter = set.iterator(); iter.hasNext(); ) {
            Temp temp = iter.next();
            if (except != null && except.contains(temp))
                continue;
            if (!in.contains(temp))
                modified = true;
            in.add(temp);
        }
        return modified;
    }

    public boolean addOut(HashSet<Temp> set) {
        boolean modified = false;
        for (Iterator<Temp> iter = set.iterator(); iter.hasNext(); ) {
            Temp temp = iter.next();
            if (!out.contains(temp))
                modified = true;
            out.add(temp);
        }
        return modified;
    }

    public static boolean addOut(HashSet<Temp> out, HashSet<Temp> set) {
        boolean modified = false;
        for (Iterator<Temp> iter = set.iterator(); iter.hasNext(); ) {
            Temp temp = iter.next();
            if (!out.contains(temp))
                modified = true;
            out.add(temp);
        }
        return modified;
    }

    public static String print(HashSet<Temp> set) {
        String s = "";
        for (Iterator<Temp> iter = set.iterator(); iter.hasNext(); ) {
            s = s + iter.next().print() + " ";
        }
        return s;
    }

    public static String print(ArrayList<Temp> list) {
        String s = "";
        for (Iterator<Temp> iter = list.iterator(); iter.hasNext(); ) {
            s = s + iter.next().print() + " ";
        }
        return s;
    }

    public void fixPoint() {
        for (int i = 0; i < data.size(); ++i) {
            datadef.add(new HashSet<Temp>());
            datause.add(new HashSet<Temp>());
            datain.add(new HashSet<Temp>());
            dataout.add(new HashSet<Temp>());
            ArrayList<Temp> list = new ArrayList<Temp>();
            data.get(i).read(list);
            BasicBlock.addUse(datause.get(i), list, datadef.get(i));
            list = new ArrayList<Temp>();
            data.get(i).write(list);
            BasicBlock.addDef(datadef.get(i), list);
            BasicBlock.addIn(datain.get(i), datause.get(i), null);
            if (i == data.size() - 1) {
                BasicBlock.addOut(dataout.get(i), out);
            }
        }
        for (; ; ) {
            boolean modified = false;
            for (int i = data.size() - 1; i >= 0; --i) {
                if (i < data.size() - 1)
                    modified = modified || BasicBlock.addOut(dataout.get(i), datain.get(i + 1));
                modified = modified || BasicBlock.addIn(datain.get(i), dataout.get(i), datadef.get(i));
            }
            if (!modified)
                break;
        }
        /*for (int i = 0; i < data.size(); ++i) {
            System.out.println(data.get(i).print());
            System.out.println(BasicBlock.print(datause.get(i)) + "use");
            System.out.println(BasicBlock.print(datadef.get(i)) + "def");
            System.out.println(BasicBlock.print(datain.get(i)) + "in");
            System.out.println(BasicBlock.print(dataout.get(i)) + "out");
            System.out.println("");
        }*/
    }
}
