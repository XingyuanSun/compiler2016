package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 孙星远 on 2016/5/2.
 */
public class RelationalBranch extends Quadruple {
    public Address src1;
    public Address src2;
    public RelationalOp op;
    public Label label; //jump if the condition if false
    public boolean reverse;

    public RelationalBranch(Address src1, RelationalOp op, Address src2, Label label) {
        this.src1 = src1;
        this.op = op;
        this.src2 = src2;
        this.label = label;
        this.reverse = false;
    }

    public RelationalBranch(Address src1, RelationalOp op, Address src2, Label label, boolean reverse) {
        this.src1 = src1;
        this.op = op;
        this.src2 = src2;
        this.label = label;
        this.reverse = reverse;
    }

    public void used(List<RenamedTemp> list) {
        if (src1 instanceof RenamedTemp)
            list.add((RenamedTemp) src1);
        if (src2 instanceof RenamedTemp)
            list.add((RenamedTemp) src2);
    }

    public void read(ArrayList<Temp> list) {
        if (src1 instanceof Temp)
            list.add((Temp) src1);
        if (src2 instanceof Temp)
            list.add((Temp) src2);
    }

    public void write(ArrayList<Temp> list) {
    }

    public void defined(List<RenamedTemp> list) {
    }

    public String print() {
        String opr = "";
        if (reverse)
            switch (op) {
                case EQ: opr = "seq"; break;
                case NE: opr = "sne"; break;
                case GT: opr = "sgt"; break;
                case GE: opr = "sge"; break;
                case LT: opr = "slt"; break;
                case LE: opr = "sle"; break;
            }
        else
            switch (op) {
                case EQ: opr = "sne"; break;
                case NE: opr = "seq"; break;
                case GT: opr = "sle"; break;
                case GE: opr = "slt"; break;
                case LT: opr = "sge"; break;
                case LE: opr = "sgt"; break;
            }
        return opr + " " + src1.print() + " " + src2.print() + " " + label.print();
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Src1, Src2;
        if (src1 instanceof Temp)
            Src1 = Register.Allocate(list, (Temp) src1, true);
        else
            Src1 = src1;

        if (src2 instanceof Temp)
            Src2 = Register.Allocate(list, (Temp) src2, true, Src1);
        else
            Src2 = src2;
        for (int i = 0; i < Register.numOfReg; ++i)
            //Register.release(list, i);
            if (Register.reference[i] != null) //optimize
                if (Register.reference[i].useless)
                    Register.dispose(Register.reference[i]);
                else
                    Register.release(list, i);

        list.add(new RelationalBranch(Src1, op, Src2, label, reverse));
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Src1, Src2;
        if (src1 instanceof Temp)
            Src1 = Temp.get((Temp) src1, color);
        else
            Src1 = src1;

        if (src2 instanceof Temp)
            Src2 = Temp.get((Temp) src2, color);
        else
            Src2 = src2;

        list.add(new RelationalBranch(Src1, op, Src2, label, reverse));
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Src1, Src2;
        if (src1 instanceof Temp && from.equal((Temp) src1))
            Src1 = to;
        else
            Src1 = src1;
        if (src2 instanceof Temp && from.equal((Temp) src2))
            Src2 = to;
        else
            Src2 = src2;

        return new RelationalBranch(Src1, op, Src2, label, reverse);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (src1 instanceof RenamedTemp)
            src1 = mapping.get(src1);
        if (src2 instanceof RenamedTemp)
            src2 = mapping.get(src2);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Src1, Src2;
        Src1 = src1;
        Src2 = src2;

        return new RelationalBranch(Src1, op, Src2, label, reverse);
    }

    public Quadruple SSADestruction() {
        Address Src1, Src2;
        if (src1 instanceof RenamedTemp)
            Src1 = ((RenamedTemp) src1).temp;
        else
            Src1 = src1;
        if (src2 instanceof RenamedTemp)
            Src2 = ((RenamedTemp) src2).temp;
        else
            Src2 = src2;

        return new RelationalBranch(Src1, op, Src2, label, reverse);
    }

    public String generate() {
        if (reverse)
            switch (op) {
                case EQ: return "beq\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case NE: return "bne\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case LT: return "blt\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case LE: return "ble\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case GT: return "bgt\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case GE: return "bge\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
            }
        else
            switch (op) {
                case EQ: return "bne\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case NE: return "beq\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case LT: return "bge\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case LE: return "bgt\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case GT: return "ble\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
                case GE: return "blt\t" + src1.generate() + ", " + src2.generate() + ", " + label.generate() + "\n";
            }
        return "";
    }
}
