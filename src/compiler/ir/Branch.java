package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Branch extends Quadruple {
    public Address src;//must be a register
    public Label label1;
    public Label label2;
    public boolean reverse;

    public Branch(Address src, Label label1, Label label2) {
        if (src instanceof IntegerConst)
            System.out.print("Branch ERROR!\n");
        this.src = src;
        this.label1 = label1;
        this.label2 = label2;
        this.reverse = false;
    }

    public Branch(Address src, Label label1, Label label2, boolean reverse) {
        if (src instanceof IntegerConst)
            System.out.print("Branch ERROR!\n");
        this.src = src;
        this.label1 = label1;
        this.label2 = label2;
        this.reverse = reverse;
    }

    public String print() {
        if (reverse)
            return "br " + src.print() + " == true " + label2.print();
        else
            return "br " + src.print() + " == false " + label2.print();
    }

    public void read(ArrayList<Temp> list) {
        if (src instanceof Temp)
            list.add((Temp) src);
    }

    public void used(List<RenamedTemp> list) {
        if (src instanceof RenamedTemp)
            list.add((RenamedTemp) src);
    }

    public void write(ArrayList<Temp> list) {
    }

    public void defined(List<RenamedTemp> list) {
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Src;
        if (src instanceof Temp)
            Src = Register.Allocate(list, (Temp) src, true);
        else
            Src = src;
        for (int i = 0; i < Register.numOfReg; ++i)
            //Register.release(list, i);
            if (Register.reference[i] != null) //optimize
                if (Register.reference[i].useless)
                    Register.dispose(Register.reference[i]);
                else
                    Register.release(list, i);
        list.add(new Branch(Src, label1, label2, reverse));
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Src;
        if (src instanceof Temp)
            Src = Temp.get((Temp) src, color);
        else
            Src = src;
        list.add(new Branch(Src, label1, label2, reverse));
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Src;
        if (src instanceof Temp && from.equal((Temp) src))
            Src = to;
        else
            Src = src;
        return new Branch(Src, label1, label2, reverse);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (src instanceof RenamedTemp)
            src = mapping.get(src);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Src;
        Src = src;
        return new Branch(Src, label1, label2, reverse);
    }

    public Quadruple SSADestruction() {
        Address Src;
        if (src instanceof RenamedTemp)
            Src = ((RenamedTemp) src).temp;
        else
            Src = src;

        return new Branch(Src, label1, label2, reverse);
    }

    public String generate() {
        if (reverse)
            return "bnez\t" + src.generate() + ", " + label2.generate() + "\n";
        else
            return "beqz\t" + src.generate() + ", " + label2.generate() + "\n";
    }
}
