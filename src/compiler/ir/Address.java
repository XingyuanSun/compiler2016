package compiler.ir;

import compiler.cfg.RenamedTemp;

public abstract class Address {
    abstract public String print();
    abstract public String generate();
    //public RenamedTemp reachingDef;
}
