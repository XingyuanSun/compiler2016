package compiler.ir;

import java.util.ArrayList;
import java.util.List;

public class IntegerConst extends Const {
    public int value;

    public IntegerConst(int value) {
        this.value = value;
    }

    public String print() {
        return String.valueOf(value);
    }

    public String generate() {
        return String.valueOf(value);
    }
}
