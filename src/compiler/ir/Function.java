package compiler.ir;

import compiler.cfg.CFG;
import compiler.cfg.PhiFunction;
import compiler.cfg.RenamedTemp;

import java.util.*;

public class Function {
    public String name;
    public List<Temp> args;
    public List<Quadruple> body;
    public List<Quadruple> dynamic;
    public List<Quadruple> static_;
    public List<Register> static_args;
    public int size;
    public List<BasicBlock> basicBlocks;
    public HashMap<Temp, Register> color;
    public boolean isLeaf;

    public Function(String name, List<Temp> args, List<Quadruple> body, int size) {
        this.name = name;
        this.args = args;
        this.body = body;
        this.size = size;
    }

    public void isLeaf() {
        this.isLeaf = true;
        for (Quadruple quadruple : body)
            if (quadruple instanceof Call || quadruple instanceof BuiltInCall) {
                this.isLeaf = false;
                break;
            }
    }

    public String print() {
        String s = "func " + name;
        for (int i = 0; i < args.size(); ++i)
            s = s + " " + args.get(i).print();
        s = s + " {\n";
        for (int i = 0; i < body.size(); ++i) {
            if (!(body.get(i) instanceof Label))
                s = s + "\t";
            s = s + body.get(i).print();
            if (body.get(i) instanceof Label)
                s = s + ":";
            s = s + "\n";
        }
        return s + "}";
    }

    public void adjust() {
        ArrayList<Quadruple> newBody = new ArrayList<Quadruple>();
        for (int i = 0; i < body.size(); ++i) {
            if (body.get(i) instanceof Label) {
                if (newBody.size() != 0 && !(newBody.get(newBody.size() - 1) instanceof Branch) && !(newBody.get(newBody.size() - 1) instanceof Goto) && !(newBody.get(newBody.size() - 1) instanceof RelationalBranch))
                    newBody.add(new Goto((Label) body.get(i)));
                newBody.add(body.get(i));
            }
            else if (body.get(i) instanceof Goto) {
                newBody.add(body.get(i));
                if (i == body.size() - 1 || !(body.get(i + 1) instanceof Label))
                    newBody.add(new Label());
            }
            else {
                 newBody.add(body.get(i));
            }
        }
        body = newBody;
    }

    public void dynamicAllocation() {
        Register.initialize();
        dynamic = new ArrayList<Quadruple>();
        for (int i = 0; i < args.size() && i < Register.numOfParam; ++i) {
            Register.reference[Register.numOfReg + i] = args.get(i);
            Register.time[Register.numOfReg + i] = 1;
            Register.modified[Register.numOfReg + i] = false;
        }
        for (int i = 0; i < body.size(); ++i)
            body.get(i).dynamicTranslate(dynamic);
        for (int i = 0; i < Register.numOfReg; ++i)
            Register.releaseGlobal(dynamic, i);
    }

    public String dynamicToString() {
        String s = name + "_enter:\n";
        if (!isLeaf)
            s = s + "sw\t" + "$ra, " + (size + Register.getNum("$ra")) * 4 + "($sp)" + "\n";

        for (int i = 0; i < dynamic.size(); ++i) {
            if (dynamic.get(i) instanceof Goto && i + 1 < dynamic.size() && dynamic.get(i + 1) instanceof Label)
                if (((Goto) dynamic.get(i)).label.num == ((Label) dynamic.get(i + 1)).num)
                    continue;
            s = s + dynamic.get(i).generate();
            if (dynamic.get(i) instanceof Label)
                s = s + ":\n";
        }

        s = s + name + "_exit:\n";
        if (!isLeaf)
            s = s + "lw\t" + "$ra, " + (size + Register.getNum("$ra")) * 4 + "($sp)" + "\n";
        s = s + "jr\t" + "$ra" + "\n";
        return s;
    }

    public String staticToString() {
        String s = name + "_enter:\n";
        s = s + "sw\t" + "$ra, " + (Register.getNum("$ra")) * 4 + "($sp)" + "\n";
        for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            Temp key = (Temp) entry.getKey();
            Register register = (Register) entry.getValue();
            if (key.name != null)
                s = s + "lw\t" + register.print() + ", " + "var" + key.name + "\n";
        }

        for (int i = 0; i < static_.size(); ++i) {
            if (static_.get(i) instanceof Goto && i + 1 < static_.size() && static_.get(i + 1) instanceof Label)
                if (((Goto) static_.get(i)).label.num == ((Label) static_.get(i + 1)).num)
                    continue;
            s = s + static_.get(i).generate();
            if (static_.get(i) instanceof Label)
                s = s + ":\n";
        }

        s = s + name + "_exit:\n";
        for (Iterator iter = color.entrySet().iterator(); iter.hasNext(); ) {
            HashMap.Entry entry = (HashMap.Entry) iter.next();
            Temp key = (Temp) entry.getKey();
            Register register = (Register) entry.getValue();
            if (key.name != null)
                s = s + "sw\t" + register.print() + ", " + "var" + key.name + "\n";
        }
        s = s + "lw\t" + "$ra, " + (Register.getNum("$ra")) * 4 + "($sp)" + "\n";
        s = s + "jr\t" + "$ra" + "\n";
        return s;
    }

    public void staticAllocation() {
        static_args = new ArrayList<Register>();
        for (int i = 0; i < args.size(); ++i)
            static_args.add(Temp.get(args.get(i), color));
        static_ = new ArrayList<Quadruple>();
        for (int i = 0; i < body.size(); ++i)
            body.get(i).staticTranslate(static_, color);
    }

    public boolean coloring() {
        basicBlocks = new ArrayList<BasicBlock>();
        HashSet<Temp> globals = new HashSet<>();
        for (int i = 0; i < body.size(); ++i) {
            ArrayList<Temp> read = new ArrayList<Temp>();
            body.get(i).read(read);
            for (int j = 0; j < read.size(); ++j)
                if (read.get(j).name != null)
                    globals.add(read.get(j));

            ArrayList<Temp> write = new ArrayList<Temp>();
            body.get(i).write(write);
            for (int j = 0; j < write.size(); ++j)
                if (write.get(j).name != null)
                    globals.add(write.get(j));
        }
        /*List<Temp> list = new ArrayList<Temp>();
        for (Iterator iter = globals.iterator(); iter.hasNext(); )
            list.add((Temp) iter.next());
        for (Temp temp : args)
            list.add(temp);*/
        /*for (Iterator<Temp> iter = globals.iterator(); iter.hasNext(); ) {
            basicBlocks.get(0).data.add(new Assign(iter.next(), new IntegerConst(0))); //Virtual Instructions
        }
        if (body.size() != 0)
            basicBlocks.get(0).data.add((Label) body.get(0));*/
        Label begin = new Label();
        Label end = new Label();
        basicBlocks.add(new BasicBlock(begin));
        for (Iterator iter = globals.iterator(); iter.hasNext(); )
            basicBlocks.get(0).def.add((Temp) iter.next());
        for (Temp temp : args)
            basicBlocks.get(0).def.add(temp);

        for (int i = 0; i < body.size(); ++i) {
            if (body.get(i) instanceof Label) {
                basicBlocks.add(new BasicBlock((Label) body.get(i)));
            }
            basicBlocks.get(basicBlocks.size() - 1).data.add(body.get(i));
        }

        basicBlocks.add(new BasicBlock(end));
        for (Iterator iter = globals.iterator(); iter.hasNext(); )
            basicBlocks.get(basicBlocks.size() - 1).use.add((Temp) iter.next());
        basicBlocks.get(0).succ1 = basicBlocks.get(1);

        basicBlocks.get(basicBlocks.size() - 2).succ1 = basicBlocks.get(basicBlocks.size() - 1);

        for (int i = 0; i < basicBlocks.size(); ++i) {
            if (basicBlocks.get(i).data.size() == 0)
                continue;
            if (basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1) instanceof Branch) {
                Branch branch = (Branch) basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1);
                for (int j = 0; j < basicBlocks.size(); ++j) {
                    if (branch.label1.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ1 = basicBlocks.get(j);
                    if (branch.label2.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ2 = basicBlocks.get(j);
                }
            }
            else if (basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1) instanceof Goto) {
                Goto got = (Goto) basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1);
                for (int j = 0; j < basicBlocks.size(); ++j) {
                    if (got.label.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ1 = basicBlocks.get(j);
                }
            }
            else if (basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1) instanceof RelationalBranch) {
                RelationalBranch relationalBranch = (RelationalBranch) basicBlocks.get(i).data.get(basicBlocks.get(i).data.size() - 1);
                for (int j = 0; j < basicBlocks.size(); ++j) {
                    if (relationalBranch.label.num == basicBlocks.get(j).label.num)
                        basicBlocks.get(i).succ1 = basicBlocks.get(j);
                }
                basicBlocks.get(i).succ2 = basicBlocks.get(i + 1);
            }

            /*if (basicBlocks.size() > 1)
                basicBlocks.get(0).succ1 = basicBlocks.get(1);*/
            /*System.out.println(basicBlocks.get(i).label.print() + ":");
            if (basicBlocks.get(i).succ1 != null)
                System.out.println(basicBlocks.get(i).succ1.label.print());
            if (basicBlocks.get(i).succ2 != null)
                System.out.println(basicBlocks.get(i).succ2.label.print());
            System.out.println("");*/
        }

        for (int i = 0; i < basicBlocks.size(); ++i) {
            basicBlocks.get(i).initialize();
            basicBlocks.get(i).addIn(basicBlocks.get(i).use, null);
        }

        for (; ; ) {
            boolean modified = false;
            for (int i = 0; i < basicBlocks.size(); ++i) {
                BasicBlock basicBlock = basicBlocks.get(i);
                if (basicBlock.succ1 != null)
                    modified = modified || basicBlock.addOut(basicBlock.succ1.in);
                if (basicBlock.succ2 != null)
                    modified = modified || basicBlock.addOut(basicBlock.succ2.in);
                modified = modified || basicBlock.addIn(basicBlock.out, basicBlock.def);
            }
            if (!modified)
                break;
        }

        /*for (int i = 0; i < basicBlocks.size(); ++i) {
            System.out.println(basicBlocks.get(i).label.print());
            System.out.println(BasicBlock.print(basicBlocks.get(i).use) + "use");
            System.out.println(BasicBlock.print(basicBlocks.get(i).def) + "def");
            System.out.println(BasicBlock.print(basicBlocks.get(i).in) + "in");
            System.out.println(BasicBlock.print(basicBlocks.get(i).out) + "out");
        }*/

        for (int i = 0; i < basicBlocks.size(); ++i)
            basicBlocks.get(i).fixPoint();

        HashMap<Temp, HashSet<Temp>> edges = buildGraph();
        List<Temp> list = new ArrayList<Temp>();
        for (; edges.size() != 0; ) {
            boolean modified = false;
            for (Iterator iter = edges.entrySet().iterator(); iter.hasNext(); ) {
                HashMap.Entry entry = (HashMap.Entry) iter.next();
                Temp key = (Temp) entry.getKey();
                HashSet<Temp> val = (HashSet<Temp>) entry.getValue();
                if (val.size() < Register.numOfReg + Register.numOfParam - 1) { //the last for isolated vertexes
                    list.add(key);
                    modified = true;
                    for (Iterator iter2 = val.iterator(); iter2.hasNext(); ) {
                        Temp temp = (Temp) iter2.next();
                        edges.get(temp).remove(key);
                    }
                    iter.remove();
                    break;
                }
            }
            if (edges.size() != 0 && !modified)
                return false;
        }
        edges = buildGraph();
        color = new HashMap<Temp, Register>();
        for (int i = list.size() - 1; i >= 0; --i) {
            //System.out.println(list.get(i).print());
            boolean[] reg = new boolean[Register.numOfReg + Register.numOfParam];
            HashSet<Temp> neighbor = edges.get(list.get(i));
            for (Iterator iter = neighbor.iterator(); iter.hasNext(); ) {
                Register register = color.get(iter.next());
                if (register != null)
                    reg[register.num] = true;
            }
            for (int j = 0; j < Register.numOfReg + Register.numOfParam; ++j)
                if (!reg[j]) {
                    color.put(list.get(i), new Register(j));
                    break;
                }
        }
        return true;
    }

    public HashMap<Temp, HashSet<Temp>> buildGraph() {
        HashMap<Temp, HashSet<Temp>> edges = new HashMap<Temp, HashSet<Temp>>();

        for (BasicBlock basicBlock: basicBlocks) {
            if (basicBlock.data.size() == 0) {
                for (Iterator<Temp> iter1 = basicBlock.def.iterator(); iter1.hasNext(); ) {
                    Temp temp1 = iter1.next();
                    for (Iterator<Temp> iter2 = basicBlock.out.iterator(); iter2.hasNext(); ) {
                        Temp temp2 = iter2.next();
                        if (temp1.equals(temp2))
                            continue;
                        Function.addEdge(temp1, temp2, edges);
                        Function.addEdge(temp2, temp1, edges);
                    }
                }
            }
            else
                for (int i = 0; i < basicBlock.data.size(); ++i) {
                    for (Iterator<Temp> iter1 = basicBlock.datadef.get(i).iterator(); iter1.hasNext(); ) {
                        Temp temp1 = iter1.next();
                        for (Iterator<Temp> iter2 = basicBlock.dataout.get(i).iterator(); iter2.hasNext(); ) {
                            Temp temp2 = iter2.next();
                            if (basicBlock.data.get(i) instanceof Assign && basicBlock.datause.get(i).contains(temp2))
                                continue;
                            if (temp1.equals(temp2))
                                continue;
                            //System.out.println("Edge " + temp1.print() + " " + temp2.print());
                            Function.addEdge(temp1, temp2, edges);
                            Function.addEdge(temp2, temp1, edges);
                        }
                    }
                }
        }
        return edges;
    }

    public static void addEdge(Temp temp1, Temp temp2, HashMap<Temp, HashSet<Temp>> edges) {
        if (edges.containsKey(temp1))
            edges.get(temp1).add(temp2);
        else {
            HashSet<Temp> hashSet = new HashSet<>();
            hashSet.add(temp2);
            edges.put(temp1, hashSet);
        }
    }

    public void set(CFG cfg) {
        List<Quadruple> newbody = new ArrayList<Quadruple>();
        for (int i = 0; i < cfg.basicBlocks.size(); ++i) {
            boolean succ = false;
            for (compiler.cfg.BasicBlock basicBlock : cfg.basicBlocks)
                if (basicBlock.originalRank == i) {
                    for (Quadruple quadruple : basicBlock.data)
                        if (!quadruple.useless)
                            newbody.add(quadruple.SSADestruction());
                    succ = true;
                    break;
                }
        }
        body = newbody;
    }

    public String printSSA(CFG cfg) {
        String s = "func " + name;
        for (int i = 0; i < args.size(); ++i)
            s = s + " " + args.get(i).print();
        s = s + " {\n";
        for (int i = 0; i < cfg.basicBlocks.size(); ++i) {
            for (compiler.cfg.BasicBlock basicBlock : cfg.basicBlocks)
                if (basicBlock.originalRank == i) {
                    s = s + basicBlock.data.get(0).print() + ":\n";

                    for (PhiFunction phiFunction : basicBlock.phiFunctions) {
                        if (phiFunction.useless)
                            continue;
                        s = s + "\t" + phiFunction.def.print() + " = phi";
                        for (RenamedTemp renamedTemp : phiFunction.used)
                            s = s + " " + renamedTemp.print();
                        s = s + "\n";
                    }

                    for (Quadruple quadruple : basicBlock.data) {
                        if (quadruple instanceof Label)
                            continue;
                        if (!quadruple.useless)
                            s = s + "\t" + quadruple.print() + "\n";
			        }
                    break;
                }
        }
        return s + "}\n";
    }
}
