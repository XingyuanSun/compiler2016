package compiler.ir;

public abstract class Const extends Address {
    abstract public String print();
}
