package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Label extends Quadruple {
    private static int labelCount = 0;

    public int num;

    public Label() {
        num = labelCount++;
    }

    public String print() {
        return "%" + num;
    }

    public int hashCode() {
        return num;
    }

    public boolean equals(Label rhs) {
        if (rhs == null)
            return false;
        return num == rhs.num;
    }

    public void used(List<RenamedTemp> list) {
    }

    public void read(ArrayList<Temp> list) {
    }

    public void write(ArrayList<Temp> list) {
    }

    public void defined(List<RenamedTemp> list) {
    }

    public void dynamicTranslate(List<Quadruple> list) {
        list.add(this);
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        list.add(this);
    }

    public Quadruple replaceUse(Temp from, Address to) {
        return this;
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        return this;
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
    }

    public Quadruple SSADestruction() {
        return this;
    }

    public String generate() {
        return "l" + num;
    }
}
