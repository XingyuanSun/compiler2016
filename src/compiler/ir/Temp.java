package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.awt.image.renderable.RenderableImage;
import java.util.HashMap;

public class Temp extends Address {
    //private static int tempCount = 0;
    public static int tempCount = 0;
    public static int globalCount = 0;
    public boolean useless;
    public static HashMap<Temp, RenamedTemp> last;

    public int num;
    public String name;

    public Temp() {
        num = tempCount++;
        this.useless = false;
    }

    public int hashCode() {
        if (name != null)
            return -num;
        else
            return num;
    }

    public boolean equals(Temp rhs) {
        if (rhs == null)
            return false;
        if (num != rhs.num)
            return false;
        if (name != null && !name.equals(rhs.name))
            return false;
        if (name == null && rhs.name != null)
            return false;
        return true;
    }

    public Temp(boolean useless) {
        this.useless = useless;
        num = tempCount++;
    }

    public Temp(String name) {
        num = globalCount++;
        this.name = name;
    }

    public boolean equal(Temp rhs) {
        if (rhs == null)
            return false;
        if (num != rhs.num)
            return false;
        if (name != null && !name.equals(rhs.name))
            return false;
        if (name == null && rhs.name != null)
            return false;
        return true;
    }

    public String print() {
        /*if (useless)
            return "!$" + num;
        else
            return "$" + num;*/
        if (name == null)
            return "$" + num;
        else
            return "$" + name;
    }

    public static Register get(Temp temp, HashMap<Temp, Register> color) {
        if (color.containsKey(temp))
            return color.get(temp);
        else {
            color.put(temp, new Register(21));
            return color.get(temp);
        }
    }

    public String generate() {
        return "";
    }
}
