package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Goto extends Quadruple {
    public Label label;

    public Goto(Label label) {
        this.label = label;
    }

    public String print() {
        return "jump " + label.print();
    }

    public void used(List<RenamedTemp> list) {
    }

    public void read(ArrayList<Temp> list) {
    }

    public void write(ArrayList<Temp> list) {
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
    }

    public void defined(List<RenamedTemp> list) {
    }

    public void dynamicTranslate(List<Quadruple> list) {
        for (int i = 0; i < Register.numOfReg; ++i)
            if (Register.reference[i] != null && Register.reference[i].useless) { //optimize
                Register.dispose(Register.reference[i]);
            }
            else {
                Register.release(list, i);
            }
        list.add(this);
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        list.add(this);
    }

    public Quadruple replaceUse(Temp from, Address to) {
        return this;
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        return this;
    }

    public Quadruple SSADestruction() {
        return this;
    }

    public String generate() {
        return "j\t" + label.generate() + "\n";
    }
}
