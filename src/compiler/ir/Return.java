package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Return extends Quadruple {
    public Address value;
    public String name;

    public Return(Address value, String name) {
        this.value = value;
        this.name = name;
    }

    public String print() {
        if (value != null)
            return "ret " + value.print();
        else
            return "ret";
    }

    public void used(List<RenamedTemp> list) {
        if (value instanceof RenamedTemp)
            list.add((RenamedTemp) value);
    }

    public void read(ArrayList<Temp> list) {
        if (value instanceof Temp)
            list.add((Temp) value);
    }

    public void write(ArrayList<Temp> list) {
    }

    public void defined(List<RenamedTemp> list) {
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Value;
        if (value instanceof Temp)
            Value = Register.Allocate(list, (Temp) value, true);
        else
            Value = value;

        for (int i = 0; i < Register.numOfReg; ++i)
            Register.releaseGlobal(list, i);
        list.add(new Return(Value, name));
        if (Value instanceof Register)
            Register.modify((Register) Value);
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Value;
        if (value instanceof Temp)
            Value = Temp.get((Temp) value, color);
        else
            Value = value;
        list.add(new Return(Value, name));
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Value;
        if (value instanceof Temp && from.equal((Temp) value))
            Value = to;
        else
            Value = value;
        return new Return(Value, name);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (value instanceof RenamedTemp)
            value = mapping.get(value);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Value;
        Value = value;
        return new Return(Value, name);
    }

    public Quadruple SSADestruction() {
        Address Value;
        if (value instanceof RenamedTemp)
            Value = ((RenamedTemp) value).temp;
        else
            Value = value;
        return new Return(Value, name);
    }

    public String generate() {
        String s = "";
        if (value != null) {
            if (value instanceof IntegerConst)
                s = s + "li\t$v0, " + value.generate() + "\n";
            else
                s = s + "move\t$v0, " + value.generate() + "\n";
        }
        return s + "j " + name + "_exit\n";
    }
}
