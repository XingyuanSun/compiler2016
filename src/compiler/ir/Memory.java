package compiler.ir;

import java.util.ArrayList;

/**
 * Created by 孙星远 on 2016/4/25.
 */
public class Memory extends Address {
    public Address temp;
    public IntegerConst offset;

    public Memory(Address temp, IntegerConst offset) {
        this.temp = temp;
        this.offset = offset;
    }

    public String print() {
        return "";
    }
    public String generate() {
        System.out.print("Memory ERROR!\n");
        return "";
    }
}

