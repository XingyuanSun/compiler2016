package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MemoryRead extends Quadruple {
    public Address dest;
    public Address src;
    public IntegerConst offset;
    public int size;

    public MemoryRead(Address dest, Address src, IntegerConst offset) {
        this.dest = dest;
        this.offset = offset;
        this.src = src;
        this.size = 4;
    }

    public MemoryRead(Address dest, Address src, IntegerConst offset, int size) {
        this.dest = dest;
        this.offset = offset;
        this.src = src;
        this.size = size;
    }

    public String print() {
        return dest.print() + " = load " + size + " " + src.print() + " " + offset.print();
    }

    public void used(List<RenamedTemp> list) {
        if (src instanceof RenamedTemp)
            list.add((RenamedTemp) src);
    }

    public void read(ArrayList<Temp> list) {
        if (src instanceof Temp)
            list.add((Temp) src);
    }

    public void write(ArrayList<Temp> list) {
        if (dest instanceof Temp)
            list.add((Temp) dest);
    }

    public void defined(List<RenamedTemp> list) {
        if (dest instanceof RenamedTemp)
            list.add((RenamedTemp) dest);
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Dest, Src;
        if (src instanceof Temp)
            Src = Register.Allocate(list, (Temp) src, true);
        else
            Src = src;

        if (dest instanceof Temp)
            Dest = Register.Allocate(list, (Temp) dest, false, Src);
        else
            Dest = dest;
        if (Dest instanceof Register)
            Register.modify((Register) Dest);

        list.add(new MemoryRead(Dest, Src, offset, size));
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Dest, Src;
        if (src instanceof Temp)
            Src = Temp.get((Temp) src, color);
        else
            Src = src;

        if (dest instanceof Temp)
            Dest = Temp.get((Temp) dest, color);
        else
            Dest = dest;
        list.add(new MemoryRead(Dest, Src, offset, size));
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Dest, Src;
        if (src instanceof Temp && from.equal((Temp) src))
            Src = to;
        else
            Src = src;

        Dest = dest;
        return new MemoryRead(Dest, Src, offset, size);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (src instanceof RenamedTemp)
            src = mapping.get(src);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Dest, Src;
        Src = src;

        if (dest instanceof Temp && from.equal((Temp) dest))
            Dest = to;
        else
            Dest = dest;
        return new MemoryRead(Dest, Src, offset, size);
    }

    public Quadruple SSADestruction() {
        Address Dest, Src;
        if (src instanceof RenamedTemp)
            Src = ((RenamedTemp) src).temp;
        else
            Src = src;
        if (dest instanceof RenamedTemp)
            Dest = ((RenamedTemp) dest).temp;
        else
            Dest = dest;
        return new MemoryRead(Dest, Src, offset, size);
    }

    public String generate() {
        String s;
        if (size == 4)
            s =  "lw\t" + dest.generate() + ", ";
        else if (size == 2)
            s = "lh\t" + dest.generate() + ", ";
        else
            s = "lb\t" + dest.generate() + ", ";
        if (src instanceof Temp) {
            if (offset.value != 0)
                System.out.print("MemoryRead ERROR!\n");
            if (((Temp) src).name != null)
                s = s + "var" + ((Temp) src).name + "\n";
                //s = s + ((Temp) src).name + "($gp)" + "\n";
            else
                s = s + ((Temp) src).num * 4 + "($sp)" + "\n";
        }
        else {
            s = s + offset.value + "(" + src.generate() + ")" + "\n";
        }
        return s;
    }
}
