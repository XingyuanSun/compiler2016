package compiler.ir;

import compiler.cfg.RenamedTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RelationalExpr extends Quadruple {
    public RelationalOp op;
    public Address dest;
    public Address src1;
    public Address src2;

    public RelationalExpr(Address dest, Address src1, RelationalOp op, Address src2) {
        if (src1 instanceof IntegerConst)
            System.out.print("RelationalExpr ERROR\n");
        this.dest = dest;
        this.src1 = src1;
        this.op = op;
        this.src2 = src2;
    }

    public String print() {
        String opr = "";
        switch (op) {
            case EQ: opr = "seq"; break;
            case NE: opr = "sne"; break;
            case GT: opr = "sgt"; break;
            case GE: opr = "sge"; break;
            case LT: opr = "slt"; break;
            case LE: opr = "sle"; break;
        }
        return dest.print() + " = " + opr + " " + src1.print() + " " + src2.print();
    }

    public void used(List<RenamedTemp> list) {
        if (src1 instanceof RenamedTemp)
            list.add((RenamedTemp) src1);
        if (src2 instanceof RenamedTemp)
            list.add((RenamedTemp) src2);
    }

    public void read(ArrayList<Temp> list) {
        if (src1 instanceof Temp)
            list.add((Temp) src1);
        if (src2 instanceof Temp)
            list.add((Temp) src2);
    }

    public void write(ArrayList<Temp> list) {
        if (dest instanceof Temp)
            list.add((Temp) dest);
    }

    public void defined(List<RenamedTemp> list) {
        if (dest instanceof RenamedTemp)
            list.add((RenamedTemp) dest);
    }

    public void dynamicTranslate(List<Quadruple> list) {
        Address Dest, Src1, Src2;
        if (src1 instanceof Temp)
            Src1 = Register.Allocate(list, (Temp) src1, true);
        else
            Src1 = src1;
        if (src2 instanceof Temp)
            Src2 = Register.Allocate(list, (Temp) src2, true, Src1);
        else
            Src2 = src2;

        if (dest instanceof Temp)
            Dest = Register.Allocate(list, (Temp) dest, false, Src1, Src2);
        else
            Dest = dest;

        list.add(new RelationalExpr(Dest, Src1, op, Src2));
        if (Dest instanceof Register)
            Register.modify((Register) Dest);
    }

    public void staticTranslate(List<Quadruple> list, HashMap<Temp, Register> color) {
        Address Dest, Src1, Src2;
        if (src1 instanceof Temp)
            Src1 = Temp.get((Temp) src1, color);
        else
            Src1 = src1;
        if (src2 instanceof Temp)
            Src2 = Temp.get((Temp) src2, color);
        else
            Src2 = src2;

        if (dest instanceof Temp)
            Dest = Temp.get((Temp) dest, color);
        else
            Dest = dest;

        list.add(new RelationalExpr(Dest, Src1, op, Src2));
        if (Dest instanceof Register)
            Register.modify((Register) Dest);
    }

    public Quadruple replaceUse(Temp from, Address to) {
        Address Dest, Src1, Src2;
        if (src1 instanceof Temp && from.equal((Temp) src1))
            Src1 = to;
        else
            Src1 = src1;
        if (src2 instanceof Temp && from.equal((Temp) src2))
            Src2 = to;
        else
            Src2 = src2;
        Dest = dest;

        return new RelationalExpr(Dest, Src1, op, Src2);
    }

    public void replaceUse(HashMap<RenamedTemp, RenamedTemp> mapping) {
        if (src1 instanceof RenamedTemp)
            src1 = mapping.get(src1);
        if (src2 instanceof RenamedTemp)
            src2 = mapping.get(src2);
    }

    public Quadruple replaceDefine(Temp from, Address to) {
        Address Dest, Src1, Src2;
        Src1 = src1;
        Src2 = src2;
        if (dest instanceof Temp && from.equal((Temp) dest))
            Dest = to;
        else
            Dest = dest;

        return new RelationalExpr(Dest, Src1, op, Src2);
    }

    public Quadruple SSADestruction() {
        Address Dest, Src1, Src2;
        if (src1 instanceof RenamedTemp)
            Src1 = ((RenamedTemp) src1).temp;
        else
            Src1 = src1;
        if (src2 instanceof RenamedTemp)
            Src2 = ((RenamedTemp) src2).temp;
        else
            Src2 = src2;
        if (dest instanceof RenamedTemp)
            Dest = ((RenamedTemp) dest).temp;
        else
            Dest = dest;

        return new RelationalExpr(Dest, Src1, op, Src2);
    }

    public String generate() {
        String opr = "";
        switch (op) {
            case EQ: opr = "seq"; break;
            case NE: opr = "sne"; break;
            case GT: opr = "sgt"; break;
            case GE: opr = "sge"; break;
            case LT: opr = "slt"; break;
            case LE: opr = "sle"; break;
        }
        return opr + " " + dest.generate() + ", " + src1.generate() + ", " + src2.generate() + "\n";
    }
}
