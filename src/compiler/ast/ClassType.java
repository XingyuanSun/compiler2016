package compiler.ast;

import compiler.ir.Address;
import compiler.ir.Quadruple;
import compiler.ir.Temp;

import java.util.ArrayList;

public class ClassType extends BasicType {
    public Symbol tag;

    public ClassType(Symbol tag) {
        this.tag = tag;
    }
	
	public String toString(int d) {
		return indent(d) + "ClassType\n" + tag.toString(d + 1);
	}

	public String prettyPrinter(int d) {
		return tag.toString();
	}

	public boolean equal(Type rhs) {
		if (rhs instanceof ClassType) {
			if (((ClassType)rhs).tag == tag)
				return true;
			else
				return false;
		}
		else
			return false;
	}

	public boolean round_2(SymbolTable table) {
		if (table.get(tag) == null) {
			System.out.println("Type undefined!\n" + this.info.toString() + this.toString(0));
			return false;
		}
		else if (table.get(tag) instanceof FunctionDecl) {
			System.out.println("Type name undefined!\n" + this.info.toString() + this.toString(0));
			return false;
		}
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new ClassType(tag);
	}

	public Address getSize(SymbolTable table, ArrayList<Quadruple> list) {
		return ((ClassDecl) table.get(tag)).getSize();
	}
}
