package compiler.ast;

public class Info {
    private int row;
    private int column;
	public Info(int arow, int acolumn) {
		row = arow;
		column = acolumn;
	}
	public String toString() {
		return "At line " + row + ", column " + column + ".\n"; 
	}
}
