package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class ReturnStmt extends Stmt {
    public Expr expr;

    public ReturnStmt() {
        expr = null;
    }

    public ReturnStmt(Expr expr) {
        this.expr = expr;
    }
	
	public String toString(int d) {
		String string = indent(d) + "ReturnStmt\n";
		if (expr != null)
			string += expr.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		String string = indent(d) + "return";
		if (expr != null)
			string += " " + expr.prettyPrinter(0);
		string += ";\n";
		return string;
	}

	public boolean round_2(SymbolTable table) {
		if (expr != null)
			return expr.round_2(table);
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (func == null) {
			System.out.println("No function here!\n" + this.info.toString() + this.toString(0));
			return null;
		}
		if (expr != null) {
			AST returnType = expr.round_3(table, loop, func);
			if (returnType == null)
				return null;
			if (!(returnType instanceof Type)) {
				System.out.println("Type needed here!\n" + info.toString() + toString(0));
				return null;
			}
			if (!(((Type) returnType).equal(func.returnType))) {
				if (((Type) returnType).equal(new NullType()) && !func.returnType.equal(new IntType()) && !func.returnType.equal(new StringType()) && !func.returnType.equal(new BoolType()))
					return this;
				System.out.println("Wrong return type!\n" + this.info.toString() + this.toString(0));
				return null;
			}
		}
		else
			if (func.returnType.equal(new VoidType()))
				return this;
			else {
				System.out.println("Wrong return type!\n" + this.info.toString() + this.toString(0));
				return null;
			}
		return this;
	}

	@Override
	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		if (expr == null)
			list.add(new Return(null, function));
		else
			list.add(new Return(expr.getValue(table, list), function));
	}
}
