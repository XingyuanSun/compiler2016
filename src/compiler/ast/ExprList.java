package compiler.ast;

import compiler.ir.Address;
import compiler.ir.ArithmeticExpr;
import compiler.ir.Quadruple;
import compiler.ir.Temp;

import java.util.ArrayList;

public class ExprList extends AST {
    public Expr expr;
    public ExprList exprList;

    public ExprList(Expr expr) {
        this.expr = expr;
        this.exprList = null;
    }

    public ExprList(Expr expr, ExprList exprList) {
        this.expr = expr;
        this.exprList = exprList;
    }
	
	public String toString(int d) {
		String string = indent(d) + "ExprList" + "\n" + expr.toString(d + 1);
		if (exprList != null)
			string += exprList.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		if (exprList != null)
			return expr.prettyPrinter(0) + ", " + exprList.prettyPrinter(0);
		else
			return expr.prettyPrinter(0);
	}

	public boolean round_2(SymbolTable table) {
		if (expr != null && !expr.round_2(table))
			return false;
		if (exprList != null && !exprList.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		AST exprType = expr.round_3(table, loop, func);
		if (exprType == null)
			return null;
		if (exprList == null)
			return new VarDeclList(new VarDecl((Type)exprType));
		else {
			AST varDeclList = exprList.round_3(table, loop, func);
			if (varDeclList == null)
				return null;
			return new VarDeclList(new VarDecl((Type)exprType), (VarDeclList)varDeclList);
		}
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, ArrayList<Address> ans) {
		ans.add(expr.getValue(table, list));
		if (exprList != null)
			exprList.translate(table, list, ans);
	}
}
