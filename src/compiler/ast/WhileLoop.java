package compiler.ast;

import compiler.ir.*;
import jdk.nashorn.internal.ir.annotations.Ignore;

import java.util.ArrayList;

public class WhileLoop extends Stmt {
    public Expr condition;
    public Stmt body;

    public WhileLoop(Expr condition, Stmt body) {
        this.condition = condition;
        this.body = body;
	}
	
	public String toString(int d) {
		return indent(d) + "WhileLoop\n" + condition.toString(d + 1) + body.toString(d + 1);
	}

	public String prettyPrinter(int d) {
		if (body instanceof CompoundStmt) {
			if (((CompoundStmt) body).stats.stmtList == null && ((CompoundStmt) body).stats.stmt != null)
				return indent(d) + "while (" + condition.prettyPrinter(0) + ")\n" + ((CompoundStmt) body).stats.stmt.prettyPrinter(d + 1);
			else
				return indent(d) + "while (" + condition.prettyPrinter(0) + ") " + body.prettyPrinter(-(d + 1));
		}
		else
			return indent(d) + "while (" + condition.prettyPrinter(0) + ")\n" + body.prettyPrinter(d + 1);
	}

	public boolean round_2(SymbolTable table) {
		if (condition != null && !condition.round_2(table))
			return false;
		if (body != null && !body.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		AST condType = condition.round_3(table, loop, func);
		if (condType == null)
			return null;
		if (!(condType instanceof Type)) {
			System.out.println("Type needed here!\n" + condition.info.toString() + condition.toString(0));
			return null;
		}
		if (!((Type)condType).equal(new BoolType())) {
			System.out.println("BoolType expected here!\n" + this.condition.info.toString() + this.condition.toString(0));
			return null;
		}
		table.beginScope();
		if (body.round_3(table, this, func) == null) {
			table.endScope();
			return null;
		}
		table.endScope();
		return this;
	}

	@Override
	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		Label label1 = new Label();
		Label label2 = new Label();
		Label label3 = new Label();
		list.add(label1);
		Address cond = condition.getValue(table, list);
		if (cond instanceof IntegerConst) {
			if (((IntegerConst) cond).value == 1)
				list.add(new Goto(label2));
			else
				list.add(new Goto(label3));
		}
		else {
			if (list.get(list.size() - 1) instanceof RelationalExpr) {
				RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
				list.remove(list.size() - 1);
				list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, label3));
			}
			else
				list.add(new Branch(cond, label2, label3));
		}
		list.add(label2);
		table.beginScope();
		body.translate(table, list, label1, label3, function);
		table.endScope();
		list.add(new Goto(label1));
		list.add(label3);
	}
}
