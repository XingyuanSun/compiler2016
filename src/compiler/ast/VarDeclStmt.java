package compiler.ast;

import compiler.ir.Label;
import compiler.ir.Quadruple;

import java.util.ArrayList;

public class VarDeclStmt extends Stmt {
    VarDecl varDecl;

    public VarDeclStmt(VarDecl varDecl) {
        this.varDecl = varDecl;
    }
	
    public VarDeclStmt(Type type, Symbol name) {
        varDecl = new VarDecl(type, name);
    }

    public VarDeclStmt(Type type, Symbol name, Expr init) {
        varDecl = new VarDecl(type, name, init);
    }
	
	public String toString(int d) {
		return indent(d) + "VarDeclStmt\n" + varDecl.toString(d + 1);
	}

    public String prettyPrinter(int d) {
        return indent(d) + varDecl.prettyPrinter(0) + ";\n";
    }

	public boolean round_2(SymbolTable table) {
		if (varDecl != null && !varDecl.round_2(table))
			return false;
		return true;
	}

    public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
        if (varDecl.round_3(table, loop, func) == null)
            return null;
        else
            return this;
    }

    @Override
    public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
        varDecl.translate(table, list);
    }
}
