package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;
import java.util.logging.Level;

public class BinaryExpr extends Expr {
	public Expr left;
	public BinaryOp op;
	public Expr right;
	public AST leftType;
	public AST rightType;

	public BinaryExpr(Expr left, BinaryOp op, Expr right) {
		this.left = left;
		this.op = op;
		this.right = right;
	}

	public String toString(int d) {
		return indent(d) + "BinaryExpr\n" + left.toString(d + 1) + indent(d + 1) + op.toString() + "\n" + right.toString(d + 1);
	}

	public String prettyPrinter(int d) {
		String s;
		switch (op) {
			case COMMA: s = ","; break;
			case ASSIGN: s = "="; break;
			case LOGICAL_OR: s = "||"; break;
			case LOGICAL_AND: s = "&&"; break;
			case OR: s = "|"; break;
			case XOR: s = "^"; break;
			case AND: s = "&"; break;
			case EQ: s = "=="; break;
			case NE: s = "!="; break;
			case LT: s = "<"; break;
			case GT: s = ">"; break;
			case LE: s = "<="; break;
			case GE: s = ">="; break;
			case SHL: s = "<<"; break;
			case SHR: s = ">>"; break;
			case ADD: s = "+"; break;
			case SUB: s = "-"; break;
			case MUL: s = "*"; break;
			case DIV: s = "/"; break;
			case MOD: s = "%"; break;
			default: s = ""; break;
		}
		int leftPriority = 20;
		if (left instanceof BinaryExpr)
			leftPriority = Expr.getPriority(((BinaryExpr) left).op);
		else if (left instanceof UnaryExpr)
			leftPriority = Expr.getPriority(((UnaryExpr) left).op);

		int rightPriority = 20;
		if (right instanceof BinaryExpr)
			rightPriority = Expr.getPriority(((BinaryExpr) right).op);
		else if (right instanceof UnaryExpr)
			rightPriority = Expr.getPriority(((UnaryExpr) right).op);

		int priority = Expr.getPriority(op);
		String Left = left.prettyPrinter(0);
		String Right = right.prettyPrinter(0);

		if (leftPriority < priority)
			Left = "(" + Left + ")";
		if (rightPriority <= priority)
			Right = "(" + Right + ")";
		s = Left + " " + s + " " + Right;
		if (d != 0)
			s = indent(d) + s + ";\n";
		return s;
	}

	public boolean round_2(SymbolTable table) {
		if (left != null && !left.round_2(table))
			return false;
		if (right != null && !right.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		leftType = left.round_3(table, loop, func);
		rightType = right.round_3(table, loop, func);
		if (leftType == null || rightType == null)
			return null;
		if (!(leftType instanceof Type) || !(leftType instanceof Type)) {
			System.out.println("Type expected here!\n" + this.info.toString() + this.toString(0));
			return null;
		}
		if (op == BinaryOp.OR || op == BinaryOp.XOR || op == BinaryOp.AND || op == BinaryOp.SHL || op == BinaryOp.SHR
				|| op == BinaryOp.ADD || op == BinaryOp.SUB || op == BinaryOp.MUL || op == BinaryOp.DIV || op == BinaryOp.MOD) {
			if (!((Type)leftType).equal(new IntType()) || !((Type)rightType).equal(new IntType())) {
				if (op == BinaryOp.ADD && ((Type)leftType).equal(new StringType()) && ((Type)rightType).equal(new StringType())) {
					return new StringType();
				}
				System.out.println("IntType expected here!\n" + this.info.toString() + this.toString(0));
				return null;
			}
			else
				return new IntType();
		}
		else if (op == BinaryOp.LT || op == BinaryOp.GT || op == BinaryOp.LE || op == BinaryOp.GE) {
			if (!((Type)leftType).equal(new IntType()) || !((Type)rightType).equal(new IntType())) {
				if (((Type)leftType).equal(new StringType()) && ((Type)rightType).equal(new StringType())) {
					return new BoolType();
				}
				System.out.println("IntType expected here!\n" + this.info.toString() + this.toString(0));
				return null;
			}
			else
				return new BoolType();
		}
		else if (op == BinaryOp.LOGICAL_OR || op == BinaryOp.LOGICAL_AND) {
			if (!((Type)leftType).equal(new BoolType()) || !((Type)rightType).equal(new BoolType())) {
				System.out.println("BoolType expected here!\n" + this.info.toString() + this.toString(0));
				return null;
			}
			else
				return new BoolType();
		}
		else if (op == BinaryOp.EQ || op == BinaryOp.NE) {
			if (((Type)leftType).equal(new NullType()))
				if ((rightType instanceof ClassType) || (rightType instanceof ArrayType))
					return new BoolType();
			if (((Type)rightType).equal(new NullType()))
				if ((leftType instanceof ClassType) || (leftType instanceof ArrayType))
					return new BoolType();
			if (!((Type)leftType).equal((Type)rightType)) {
				System.out.println("Type does not match here!\n" + this.info.toString() + this.toString(0));
				return null;
			}
			else
				return new BoolType();
		}
		else {//assign
			if (!((Type)leftType).equal((Type)rightType)) {
				if (((Type)rightType).equal(new NullType()))
					if (!(((Type)leftType).equal(new IntType()) || ((Type)leftType).equal(new BoolType()) || ((Type)leftType).equal(new StringType())))
						if (((Type)leftType).lvalue)
							return leftType;
				System.out.println("Type does not match here!\n" + this.info.toString() + this.toString(0));
				return null;
			}
			else if (!((Type)leftType).lvalue) {
				System.out.println("Lvalue needed here!\n" + this.info.toString() + this.toString(0));
				return null;
			}
			else
				return leftType;
		}
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		if (op == BinaryOp.LOGICAL_AND) {
			Address src1;
			Label fail = null;
			if (left instanceof BinaryExpr) {
				fail = new Label();
				src1 = ((BinaryExpr) left).shortCircuit(table, list, op, fail);
			}
			else
				src1 = left.getValue(table, list);
			if (src1 instanceof IntegerConst) {
				if (((IntegerConst) src1).value == 0)
					return new IntegerConst(0);
				else
					return right.getValue(table, list);
			}
			else {
				Label label = new Label();
				Label success = new Label();
				if (fail == null)
					fail = new Label();
				Label end = new Label();
				Temp dest = new Temp();

				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else
					list.add(new Branch(src1, label, fail));
				//list.add(new Branch(src1, label, fail));
				list.add(label);
				Address src2 = right.getValue(table, list);
				if (src2 instanceof IntegerConst) {
					if (((IntegerConst) src2).value == 1)
						list.add(new Goto(success));
					else
						list.add(new Goto(fail));
				}
				else if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else list.add(new Branch(src2, success, fail));

				list.add(success);
				list.add(new Assign(dest, new IntegerConst(1)));
				list.add(new Goto(end));
				list.add(fail);
				list.add(new Assign(dest, new IntegerConst(0)));
				list.add(end);
				return dest;
			}
		}
		if (op == BinaryOp.LOGICAL_OR) {
			Label success = null;
			Address src1;
			if (left instanceof BinaryExpr) {
				success = new Label();
				src1 = ((BinaryExpr) left).shortCircuit(table, list, op, success);
			}
			else
				src1 = left.getValue(table, list);
			if (src1 instanceof IntegerConst) {
				if (((IntegerConst) src1).value == 1)
					return new IntegerConst(1);
				else
					return right.getValue(table, list);
			}
			else {
				Label label = new Label();
				if (success == null)
					success = new Label();
				Label fail = new Label();
				Label end = new Label();
				Temp dest = new Temp();
				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, success, true));
				}
				else
					list.add(new Branch(src1, label, success, true));
				list.add(label);
				Address src2 = right.getValue(table, list);
				if (src2 instanceof IntegerConst) {
					if (((IntegerConst) src2).value == 1)
						list.add(new Goto(success));
					else
						list.add(new Goto(fail));
				}
				else if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else
					list.add(new Branch(src2, success, fail));
				list.add(success);
				list.add(new Assign(dest, new IntegerConst(1)));
				list.add(new Goto(end));
				list.add(fail);
				list.add(new Assign(dest, new IntegerConst(0)));
				list.add(end);
				return dest;
			}
		}
		if (op == BinaryOp.ASSIGN) {
			Address address = left.getAddress(table, list);
			//System.out.print(address.print());
			if (address instanceof Temp) {
				right.getValue(table, list, (Temp) address);
				return address;
			}
			else {
				Temp temp = new Temp(true);
				right.getValue(table, list, temp);
				list.add(new MemoryWrite(((Memory)address).temp, ((Memory)address).offset, temp));
				return temp;
			}
		}
		Address src2 = right.getValue(table, list);
		Address src1 = left.getValue(table, list);
		if (op == BinaryOp.OR || op == BinaryOp.LOGICAL_OR) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value | ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.OR, src1));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.OR, src2));
				return dest;
			}
		}
		if (op == BinaryOp.XOR) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value ^ ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.XOR, src1));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.XOR, src2));
				return dest;
			}
		}
		if (op == BinaryOp.AND || op == BinaryOp.LOGICAL_AND) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value & ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.AND, src1));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.AND, src2));
				return dest;
			}
		}
		if (op == BinaryOp.SHL) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value << ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.SHL, src2));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.SHL, src2));
				return dest;
			}
		}
		if (op == BinaryOp.SHR) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value >> ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.SHR, src2));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.SHR, src2));
				return dest;
			}
		}
		if (op == BinaryOp.ADD) {
			if (((Type)leftType).equal(new IntType())) {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
					return new IntegerConst(((IntegerConst) src1).value + ((IntegerConst) src2).value);
				else if (src1 instanceof IntegerConst) {
					Temp dest = new Temp(true);
					list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.ADD, src1));
					return dest;
				}
				else {
					Temp dest = new Temp(true);
					list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.ADD, src2));
					return dest;
				}
			}
			else {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				Temp temp = new Temp();
				list.add(new BuiltInCall(temp, 7, arg));
				return temp;
			}
		}
		if (op == BinaryOp.SUB) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value - ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.SUB, src2));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.SUB, src2));
				return dest;
			}
		}
		if (op == BinaryOp.MUL) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value * ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.MUL, src1));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.MUL, src2));
				return dest;
			}
		}
		if (op == BinaryOp.DIV) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value / ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.DIV, src2));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.DIV, src2));
				return dest;
			}
		}
		if (op == BinaryOp.MOD) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				return new IntegerConst(((IntegerConst) src1).value % ((IntegerConst) src2).value);
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.MOD, src2));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.MOD, src2));
				return dest;
			}
		}

		if (op == BinaryOp.LT) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				Temp temp = new Temp();
				list.add(new BuiltInCall(temp, 9, arg));
				return temp;
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value < ((IntegerConst) src2).value)
						return new IntegerConst(1);
					else
						return new IntegerConst(0);
				}
				else if (src1 instanceof IntegerConst) {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src2, RelationalOp.GT, src1));
					return dest;
				}
				else {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src1, RelationalOp.LT, src2));
					return dest;
				}
			}
		}
		if (op == BinaryOp.GT) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				Temp temp = new Temp();
				list.add(new BuiltInCall(temp, 10, arg));
				list.add(new ArithmeticExpr(temp, ArithmeticOp.BOOLNOT, temp));
				return temp;
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value > ((IntegerConst) src2).value)
						return new IntegerConst(1);
					else
						return new IntegerConst(0);
				}
				else if (src1 instanceof IntegerConst) {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src2, RelationalOp.LT, src1));
					return dest;
				}
				else {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src1, RelationalOp.GT, src2));
					return dest;
				}
			}
		}
		if (op == BinaryOp.LE) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				Temp temp = new Temp();
				list.add(new BuiltInCall(temp, 10, arg));
				return temp;
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value <= ((IntegerConst) src2).value)
						return new IntegerConst(1);
					else
						return new IntegerConst(0);
				}
				else if (src1 instanceof IntegerConst) {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src2, RelationalOp.GE, src1));
					return dest;
				}
				else {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src1, RelationalOp.LE, src2));
					return dest;
				}
			}
		}
		if (op == BinaryOp.GE) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				Temp temp = new Temp();
				list.add(new BuiltInCall(temp, 9, arg));
				list.add(new ArithmeticExpr(temp, ArithmeticOp.BOOLNOT, temp));
				return temp;
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value >= ((IntegerConst) src2).value)
						return new IntegerConst(1);
					else
						return new IntegerConst(0);
				}
				else if (src1 instanceof IntegerConst) {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src2, RelationalOp.LE, src1));
					return dest;
				}
				else {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src1, RelationalOp.GE, src2));
					return dest;
				}
			}
		}

		if (op == BinaryOp.EQ) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				Temp temp = new Temp();
				list.add(new BuiltInCall(temp, 8, arg));
				return temp;
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value == ((IntegerConst) src2).value)
						return new IntegerConst(1);
					else
						return new IntegerConst(0);
				}
				else if (src1 instanceof IntegerConst) {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src2, RelationalOp.EQ, src1));
					return dest;
				}
				else {
					Temp dest = new Temp(true);
					list.add(new RelationalExpr(dest, src1, RelationalOp.EQ, src2));
					return dest;
				}
			}
		}
		if (op == BinaryOp.NE) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				Temp temp = new Temp();
				list.add(new BuiltInCall(temp, 8, arg));
				list.add(new ArithmeticExpr(temp, ArithmeticOp.BOOLNOT, temp));
				return temp;
			}
			else if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
				if (((IntegerConst) src1).value != ((IntegerConst) src2).value)
					return new IntegerConst(1);
				else
					return new IntegerConst(0);
			}
			else if (src1 instanceof IntegerConst) {
				Temp dest = new Temp(true);
				list.add(new RelationalExpr(dest, src2, RelationalOp.NE, src1));
				return dest;
			}
			else {
				Temp dest = new Temp(true);
				list.add(new RelationalExpr(dest, src1, RelationalOp.NE, src2));
				return dest;
			}
		}
		return null;
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		if (op == BinaryOp.LOGICAL_AND) {
			Label fail = null;
			Address src1;
			if (left instanceof BinaryExpr) {
				fail = new Label();
				src1 = ((BinaryExpr) left).shortCircuit(table, list, op, fail);
			}
			else
				src1 = left.getValue(table, list);
			if (src1 instanceof IntegerConst) {
				if (((IntegerConst) src1).value == 0)
					list.add(new Assign(dest, new IntegerConst(0)));
				else
					list.add(new Assign(dest, right.getValue(table, list)));
			}
			else {
				Label label = new Label();
				Label success = new Label();
				if (fail == null)
					fail = new Label();
				Label end = new Label();
				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else
					list.add(new Branch(src1, label, fail));
				//list.add(new Branch(src1, label, fail));
				list.add(label);
				Address src2 = right.getValue(table, list);
				if (src2 instanceof IntegerConst) {
					if (((IntegerConst) src2).value == 1)
						list.add(new Goto(success));
					else
						list.add(new Goto(fail));
				}
				else if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else list.add(new Branch(src2, success, fail));

				list.add(success);
				list.add(new Assign(dest, new IntegerConst(1)));
				list.add(new Goto(end));
				list.add(fail);
				list.add(new Assign(dest, new IntegerConst(0)));
				list.add(end);
			}
		}
		if (op == BinaryOp.LOGICAL_OR) {
			Label success = null;
			Address src1;
			if (left instanceof BinaryExpr) {
				success = new Label();
				src1 = ((BinaryExpr) left).shortCircuit(table, list, op, success);
			}
			else
				src1 = left.getValue(table, list);
			if (src1 instanceof IntegerConst) {
				if (((IntegerConst) src1).value == 1)
					list.add(new Assign(dest, new IntegerConst(1)));
				else
					list.add(new Assign(dest, right.getValue(table, list)));
			}
			else {
				Label label = new Label();
				if (success == null)
					success = new Label();
				Label fail = new Label();
				Label end = new Label();
				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, success, true));
				}
				else
					list.add(new Branch(src1, label, success, true));
				list.add(label);
				Address src2 = right.getValue(table, list);
				if (src2 instanceof IntegerConst) {
					if (((IntegerConst) src2).value == 1)
						list.add(new Goto(success));
					else
						list.add(new Goto(fail));
				}
				else if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else
					list.add(new Branch(src2, success, fail));
				list.add(success);
				list.add(new Assign(dest, new IntegerConst(1)));
				list.add(new Goto(end));
				list.add(fail);
				list.add(new Assign(dest, new IntegerConst(0)));
				list.add(end);
			}
		}
		if (op == BinaryOp.ASSIGN) {
			Address address = left.getAddress(table, list);
			//System.out.print(address.print());
			if (address instanceof Temp) {
				right.getValue(table, list, (Temp) address);
				list.add(new Assign(dest, (Temp) address));
			}
			else {
				right.getValue(table, list, dest);
				list.add(new MemoryWrite(((Memory)address).temp, ((Memory)address).offset, dest));
			}
		}
		Address src2 = right.getValue(table, list);
		Address src1 = left.getValue(table, list);
		if (op == BinaryOp.OR || op == BinaryOp.LOGICAL_OR) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value | ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst)
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.OR, src1));
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.OR, src2));
		}
		if (op == BinaryOp.XOR) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value ^ ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst)
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.XOR, src1));
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.XOR, src2));
		}
		if (op == BinaryOp.AND || op == BinaryOp.LOGICAL_AND) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value & ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst)
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.AND, src1));
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.AND, src2));
		}
		if (op == BinaryOp.SHL) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value << ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.SHL, src2));
			}
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.SHL, src2));
		}
		if (op == BinaryOp.SHR) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value >> ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.SHR, src2));
			}
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.SHR, src2));
		}
		if (op == BinaryOp.ADD) {
			if (((Type)leftType).equal(new IntType())) {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
					list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value + ((IntegerConst) src2).value)));
				else if (src1 instanceof IntegerConst)
					list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.ADD, src1));
				else
					list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.ADD, src2));
			}
			else {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				list.add(new BuiltInCall(dest, 7, arg));
			}
		}
		if (op == BinaryOp.SUB) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value - ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.SUB, src2));
			}
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.SUB, src2));
		}
		if (op == BinaryOp.MUL) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value * ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst)
				list.add(new ArithmeticExpr(dest, src2, ArithmeticOp.MUL, src1));
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.MUL, src2));
		}
		if (op == BinaryOp.DIV) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value / ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.DIV, src2));
			}
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.DIV, src2));
		}
		if (op == BinaryOp.MOD) {
			if (src1 instanceof IntegerConst && src2 instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(((IntegerConst) src1).value % ((IntegerConst) src2).value)));
			else if (src1 instanceof IntegerConst) {
				Temp temp = new Temp(true);
				list.add(new Assign(temp, src1));
				list.add(new ArithmeticExpr(dest, temp, ArithmeticOp.MOD, src2));
			}
			else
				list.add(new ArithmeticExpr(dest, src1, ArithmeticOp.MOD, src2));
		}

		if (op == BinaryOp.LT) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				list.add(new BuiltInCall(dest, 9, arg));
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value < ((IntegerConst) src2).value)
						list.add(new Assign(dest, new IntegerConst(1)));
					else
						list.add(new Assign(dest, new IntegerConst(0)));
				}
				else if (src1 instanceof IntegerConst) {
					list.add(new RelationalExpr(dest, src2, RelationalOp.GT, src1));
				}
				else {
					list.add(new RelationalExpr(dest, src1, RelationalOp.LT, src2));
				}
			}
		}
		if (op == BinaryOp.GT) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				list.add(new BuiltInCall(dest, 10, arg));
				list.add(new ArithmeticExpr(dest, ArithmeticOp.BOOLNOT, dest));
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value > ((IntegerConst) src2).value)
						list.add(new Assign(dest, new IntegerConst(1)));
					else
						list.add(new Assign(dest, new IntegerConst(0)));
				}
				else if (src1 instanceof IntegerConst)
					list.add(new RelationalExpr(dest, src2, RelationalOp.LT, src1));
				else
					list.add(new RelationalExpr(dest, src1, RelationalOp.GT, src2));
			}
		}
		if (op == BinaryOp.LE) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				list.add(new BuiltInCall(dest, 10, arg));
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value <= ((IntegerConst) src2).value)
						list.add(new Assign(dest, new IntegerConst(1)));
					else
						list.add(new Assign(dest, new IntegerConst(0)));
				}
				else if (src1 instanceof IntegerConst)
					list.add(new RelationalExpr(dest, src2, RelationalOp.GE, src1));
				else
					list.add(new RelationalExpr(dest, src1, RelationalOp.LE, src2));
			}
		}
		if (op == BinaryOp.GE) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				list.add(new BuiltInCall(dest, 9, arg));
				list.add(new ArithmeticExpr(dest, ArithmeticOp.BOOLNOT, dest));
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value >= ((IntegerConst) src2).value)
						list.add(new Assign(dest, new IntegerConst(1)));
					else
						list.add(new Assign(dest, new IntegerConst(0)));
				}
				else if (src1 instanceof IntegerConst)
					list.add(new RelationalExpr(dest, src2, RelationalOp.LE, src1));
				else
					list.add(new RelationalExpr(dest, src1, RelationalOp.GE, src2));
			}
		}

		if (op == BinaryOp.EQ) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				list.add(new BuiltInCall(dest, 8, arg));
			}
			else {
				if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
					if (((IntegerConst) src1).value == ((IntegerConst) src2).value)
						list.add(new Assign(dest, new IntegerConst(1)));
					else
						list.add(new Assign(dest, new IntegerConst(0)));
				}
				else if (src1 instanceof IntegerConst)
					list.add(new RelationalExpr(dest, src2, RelationalOp.EQ, src1));
				else
					list.add(new RelationalExpr(dest, src1, RelationalOp.EQ, src2));
			}
		}
		if (op == BinaryOp.NE) {
			if (((Type)leftType).equal(new StringType())) {
				ArrayList<Address> arg = new ArrayList<Address>();
				arg.add(src1);
				arg.add(src2);
				list.add(new BuiltInCall(dest, 8, arg));
				list.add(new ArithmeticExpr(dest, ArithmeticOp.BOOLNOT, dest));
			}
			else if (src1 instanceof IntegerConst && src2 instanceof IntegerConst) {
				if (((IntegerConst) src1).value != ((IntegerConst) src2).value)
					list.add(new Assign(dest, new IntegerConst(1)));
				else
					list.add(new Assign(dest, new IntegerConst(0)));
			}
			else if (src1 instanceof IntegerConst)
				list.add(new RelationalExpr(dest, src2, RelationalOp.NE, src1));
			else
				list.add(new RelationalExpr(dest, src1, RelationalOp.NE, src2));
		}
	}

	public Address shortCircuit(SymbolTable table, ArrayList<Quadruple> list, BinaryOp operation, Label jump) {
		//return getValue(table, list);
		if (operation != op)
			return getValue(table, list);
		if (op == BinaryOp.LOGICAL_AND) {
			Address src1;
			if (left instanceof BinaryExpr)
				src1 = ((BinaryExpr) left).shortCircuit(table, list, op, jump);
			else
				src1 = left.getValue(table, list);
			if (src1 instanceof IntegerConst) {
				if (((IntegerConst) src1).value == 0)
					return new IntegerConst(0);
				else
					return right.getValue(table, list);
			}
			else {
				Label label = new Label();
				Label success = new Label();
				Label fail = jump;
				Label end = new Label();
				Temp dest = new Temp();

				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else
					list.add(new Branch(src1, label, fail));
				//list.add(new Branch(src1, label, fail));
				list.add(label);
				Address src2 = right.getValue(table, list);
				if (src2 instanceof IntegerConst) {
					if (((IntegerConst) src2).value == 1)
						list.add(new Goto(success));
					else
						list.add(new Goto(fail));
				}
				else if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, fail));
				}
				else list.add(new Branch(src2, success, fail));

				list.add(success);
				list.add(new Assign(dest, new IntegerConst(1)));
				list.add(new Goto(end));
				//list.add(fail);
				//list.add(new Assign(dest, new IntegerConst(0)));
				list.add(end);
				return dest;
			}
		}
		if (op == BinaryOp.LOGICAL_OR) {
			Address src1;
			if (left instanceof BinaryExpr)
				src1 = ((BinaryExpr) left).shortCircuit(table, list, op, jump);
			else
				src1 = left.getValue(table, list);
			if (src1 instanceof IntegerConst) {
				if (((IntegerConst) src1).value == 1)
					return new IntegerConst(1);
				else
					return right.getValue(table, list);
			}
			else {
				Label label = new Label();
				Label success = jump;
				Label fail = new Label();
				Label end = new Label();
				Temp dest = new Temp();
				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, success, true));
				}
				else
					list.add(new Branch(src1, label, success, true));
				list.add(label);
				Address src2 = right.getValue(table, list);
				if (src2 instanceof IntegerConst) {
					if (((IntegerConst) src2).value == 1)
						list.add(new Goto(success));
					else
						list.add(new Goto(fail));
				}
				else if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, success, true));
				}
				else
					list.add(new Branch(src2, fail, success, true));
				//list.add(success);
				//list.add(new Assign(dest, new IntegerConst(1)));
				//list.add(new Goto(end));
				list.add(fail);
				list.add(new Assign(dest, new IntegerConst(0)));
				list.add(end);
				return dest;
			}
		}
		return null;
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("ERROR!\n");
		return null;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
