package compiler.ast;

public enum UnaryOp {
    INC, DEC, PLUS, MINUS, TILDE, NOT
}
