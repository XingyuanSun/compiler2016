package compiler.ast;

public class NullType extends Type {
    public String toString(int d) {
        return indent(d) + "NullType\n";
    }

    public boolean equal(Type rhs) {
        if (rhs instanceof NullType)
            return true;
        else
            return false;
    }

    public String prettyPrinter(int d) {
        return "";
    }

    public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
        return new NullType();
    }
}