package compiler.ast;

public class VoidType extends BasicType {
	
	public String toString(int d) {
		return indent(d) + "VoidType\n";
	}

	public String prettyPrinter(int d) {
		return "void";
	}
	public boolean equal(Type rhs) {
		if (rhs instanceof VoidType)
			return true;
		else
			return false;
	}
	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new VoidType();
	}
}
