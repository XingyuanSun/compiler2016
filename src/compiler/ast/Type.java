package compiler.ast;

import compiler.ir.Address;
import compiler.ir.Quadruple;
import compiler.ir.Temp;

import java.util.ArrayList;

public abstract class Type extends AST {
	public boolean lvalue = false;
	public abstract boolean equal(Type rhs);
	public Address getSize(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("ERROR!");
		return new Temp();
	}
}
