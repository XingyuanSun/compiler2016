package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class NullConst extends Expr {
	public String toString(int d) {
		return indent(d) + "NullConst" + "\n";
	}

	public String prettyPrinter(int d) {
		if (d == 0)
			return "null";
		else
			return indent(d) + "null;\n";
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new NullType();
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		return new IntegerConst(0);
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		list.add(new Assign(dest, new IntegerConst(0)));
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("ERROR!\n");
		return null;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
