package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class CreationExpr extends Expr {
    public Type type;

    public CreationExpr(Type type) {
        this.type = type;
    }

    public String toString(int d) {
        return indent(d) + "CreationExpr\n" + type.toString(d + 1);
    }

    public String prettyPrinter(int d) {
		String string;
		if (type instanceof ArrayType)
			string = ((ArrayType)type).getInnermostType().prettyPrinter(0) + type.prettyPrinter(0);
		else
			string = type.prettyPrinter(0);
		if (d == 0)
			return "new " + string;
		else
			return indent(d) + "new " + string + ";\n";
    }

    public boolean round_2(SymbolTable table) {
		return type.round_2(table);
	}

    public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
        if (type instanceof ArrayType) {
            if (((ArrayType) type).arraySize == null) {
                System.out.println("You must indicate at least the outermost dimension's size!\n" + this.info.toString() + this.toString(0));
                return null;
            }
        }
		if (type instanceof IntType) {
			System.out.println("You can not new a IntType!\n" + this.info.toString() + this.toString(0));
            return null;
		}
		if (type instanceof BoolType) {
			System.out.println("You can not new a BoolType!\n" + this.info.toString() + this.toString(0));
            return null;
		}
		if (type instanceof StringType) {
			System.out.println("You can not new a StringType!\n" + this.info.toString() + this.toString(0));
            return null;
		}
        return type.round_3(table, loop, func);
    }

    public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
        if (type instanceof ClassType) {
            Address src = ((ClassType) type).getSize(table, list);
            Address dest = new Temp();
            list.add(new BuiltInCall(dest, 0, new IntegerConst(((IntegerConst) src).value * 4)));
            return dest;
        }
        else {
            Address src = ((ArrayType) type).getSize(table, list);
            if (src instanceof IntegerConst) {
                Address dest2 = new Temp();
                list.add(new BuiltInCall(dest2, 0, new IntegerConst((((IntegerConst) src).value + 1) * 4)));
                Temp tempSrc = new Temp(true);
                list.add(new Assign(tempSrc, src));
                list.add(new MemoryWrite(dest2, new IntegerConst(0), tempSrc));
                return dest2;
            }
            else {
                Address dest1 = new Temp(true);
                list.add(new ArithmeticExpr(dest1, src, ArithmeticOp.ADD, new IntegerConst(1)));
                Address temp = new Temp(true);
                list.add(new ArithmeticExpr(temp, dest1, ArithmeticOp.MUL, new IntegerConst(4)));
                Address dest2 = new Temp();
                list.add(new BuiltInCall(dest2, 0, temp));
                list.add(new MemoryWrite(dest2, new IntegerConst(0), src));
                return dest2;
            }
        }
    }

    public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
        if (type instanceof ClassType) {
            Address src = ((ClassType) type).getSize(table, list);
            list.add(new BuiltInCall(dest, 0, new IntegerConst(((IntegerConst) src).value * 4)));
        }
        else {
            Address src = ((ArrayType) type).getSize(table, list);
            if (src instanceof IntegerConst) {
                list.add(new BuiltInCall(dest, 0, new IntegerConst((((IntegerConst) src).value + 1) * 4)));
                Temp tempSrc = new Temp(true);
                list.add(new Assign(tempSrc, src));
                list.add(new MemoryWrite(dest, new IntegerConst(0), tempSrc));
            }
            else {
                Address dest1 = new Temp(true);
                list.add(new ArithmeticExpr(dest1, src, ArithmeticOp.ADD, new IntegerConst(1)));
                Address temp = new Temp(true);
                list.add(new ArithmeticExpr(temp, dest1, ArithmeticOp.MUL, new IntegerConst(4)));
                list.add(new BuiltInCall(dest, 0, temp));
                list.add(new MemoryWrite(dest, new IntegerConst(0), src));
            }
        }
    }

    public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
        System.out.println("ERROR!\n");
        return null;
    }

    public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
        getValue(table, list);
    }
}
