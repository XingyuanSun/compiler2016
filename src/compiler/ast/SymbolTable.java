package compiler.ast;
import compiler.ir.Function;
import compiler.ir.Temp;

import java.util.*;

public class SymbolTable {
	int layer;
	Hashtable<Symbol, Stack<ASTNode> > table;
	Stack<Stack<Symbol> > remove;
	
	public SymbolTable() {
		layer = 0;
		remove = new Stack<Stack<Symbol> >();
		remove.push(new Stack<Symbol>());
		table = new Hashtable<Symbol, Stack<ASTNode> >();
	}
	public boolean put(Symbol key, AST value) {
		return put(key, value, null);
	}
	public boolean put(Symbol key, AST value, Temp tmp) {
		if (table.containsKey(key) && !table.get(key).empty()) {
			if (table.get(key).peek().layer == layer || table.get(key).peek().ast instanceof FunctionDecl || table.get(key).peek().ast instanceof ClassDecl)
				return false;
			else {
				table.get(key).push(new ASTNode(value, layer, tmp));
				remove.peek().push(key);
				return true;
			}
		}
		else {
			if (table.containsKey(key))
				table.get(key).push(new ASTNode(value, layer, tmp));
			else {
				Stack<ASTNode> stack = new Stack<ASTNode>();
				stack.push(new ASTNode(value, layer, tmp));
				table.put(key, stack);
			}
			remove.peek().push(key);
			return true;
		}
	}
	public boolean putFunc(Symbol key, AST value, Function function) {
		if (table.containsKey(key) && !table.get(key).empty()) {
			if (table.get(key).peek().layer == layer || table.get(key).peek().ast instanceof FunctionDecl || table.get(key).peek().ast instanceof ClassDecl)
				return false;
			else {
				table.get(key).push(new ASTNode(value, layer, null, function));
				remove.peek().push(key);
				return true;
			}
		}
		else {
			if (table.containsKey(key))
				table.get(key).push(new ASTNode(value, layer, null, function));
			else {
				Stack<ASTNode> stack = new Stack<ASTNode>();
				stack.push(new ASTNode(value, layer, null, function));
				table.put(key, stack);
			}
			remove.peek().push(key);
			return true;
		}
	}
	public void set(Symbol key, Temp tmp) {
		if (table.containsKey(key) && !table.get(key).empty())
			table.get(key).peek().temp = tmp;
		else
			System.out.println("ERROR!\n");
	}
	public AST get(Symbol key) {
		if (table.containsKey(key) && !table.get(key).empty())
			return table.get(key).peek().ast;
		else
			return null;
	}
	public Temp getAddr(Symbol key) {
		if (table.containsKey(key) && !table.get(key).empty())
			return table.get(key).peek().temp;
		else
			return null;
	}
	public Function getFunc(Symbol key) {
		if (table.containsKey(key) && !table.get(key).empty())
			return table.get(key).peek().function;
		else
			return null;
	}
	public void beginScope() {
		++layer;
		remove.push(new Stack<Symbol>());
	}
	public void endScope() {
		for (; !remove.peek().empty(); )
			table.get(remove.peek().pop()).pop();
		--layer;
		remove.pop();
	}
}

class ASTNode {
	public AST ast;
	public int layer;
	public Temp temp;
	public Function function;
	public ASTNode(AST ast, int layer, Temp temp) {
		this.ast = ast;
		this.layer = layer;
		this.temp = temp;
	}
	public ASTNode(AST ast, int layer, Temp temp, Function function) {
		this.ast = ast;
		this.layer = layer;
		this.temp = temp;
		this.function = function;
	}
}
