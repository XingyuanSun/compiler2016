package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class StringConst extends Expr {
    public String value;
    public String original;

    public StringConst(String value) {
        this.original = value;
        this.value = "";
        for (int i = 0; i < value.length(); ++i)
            if (value.charAt(i) == '\\' && i < value.length() - 1) {
                if (value.charAt(i + 1) == 'n') {
                    this.value = this.value + '\n';
                    ++i;
                }
                else if (value.charAt(i + 1) == '\\') {
                    this.value = this.value + '\\';
                    ++i;
                }
                else if (value.charAt(i + 1) == '\"') {
                    this.value = this.value + '\"';
                    ++i;
                }
                else
                    this.value = this.value + value.charAt(i);
            }
            else
                this.value = this.value + value.charAt(i);
    }
	
	public String toString(int d) {
		return indent(d) + "StringConst : \"" + value + "\"\n";
	}

    public String prettyPrinter(int d) {
		if (d == 0)
			return "\"" + original + "\"";
		else
			return indent(d) + "\"" + original + "\";\n";
    }

    public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
        return new StringType();
    }

    public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
        StringAddressConst string = new StringAddressConst(value, original);
        Temp temp = new Temp();
        list.add(new Assign(temp, string));
        return temp;
    }

    public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
        StringAddressConst string = new StringAddressConst(value, original);
        list.add(new Assign(dest, string));
    }

    public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
        System.out.println("ERROR!\n");
        return null;
    }

    public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
        getValue(table, list);
    }
}
