package compiler.ast;

import compiler.ir.Goto;
import compiler.ir.Label;
import compiler.ir.Quadruple;

import java.util.ArrayList;

public class ContinueStmt extends Stmt {
	public String toString(int d) {
		return indent(d) + "ContinueStmt\n";
	}

	public String prettyPrinter(int d) {
		return indent(d) + "continue;\n";
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (loop == null) {
			System.out.println("No loop here!\n" + this.info.toString() + this.toString(0));
			return null;
		}
		return this;
	}

	@Override
	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		list.add(new Goto(begin));
	}
}
