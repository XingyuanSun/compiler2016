package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class ArrayAccess extends Expr {
    public Expr body;
    public Expr subscript;

    public ArrayAccess(Expr body, Expr subscript) {
        this.body = body;
        this.subscript = subscript;
    }
	
	public String toString(int d) {
		return indent(d) + "ArrayAccess\n" + body.toString(d + 1) + subscript.toString(d + 1);
	}
	
	public String prettyPrinter(int d) {
		if (d == 0)
			return body.prettyPrinter(0) + "[" + subscript.prettyPrinter(0) + "]";
		else
			return indent(d) + body.prettyPrinter(0) + "[" + subscript.prettyPrinter(0) + "];\n";
	}
	
	public boolean round_2(SymbolTable table) {
		if (body != null && !body.round_2(table))
			return false;
		if (subscript != null && !subscript.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		AST bodyType = body.round_3(table, loop, func);
		if (bodyType == null)
			return null;
		if (!(bodyType instanceof ArrayType)) {
			System.out.println("ArrayType needed here!\n" + info.toString() + toString(0));
			return null;
		}
		AST subscriptType = subscript.round_3(table, loop, func);
		if (!(subscriptType instanceof IntType)) {
			System.out.println("IntType needed here!\n" + subscript.info.toString() + subscript.toString(0));
			return null;
		}
		((ArrayType) bodyType).baseType.lvalue = true;
		return ((ArrayType) bodyType).baseType;
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		Address temp1 = body.getValue(table, list);
		Address temp2 = subscript.getValue(table, list);
		Address dest1;
		Address dest2;
		Temp dest = new Temp(true);
		if (temp2 instanceof IntegerConst) {
			dest1 = new IntegerConst(((IntegerConst) temp2).value * 4);
		}
		else {
			dest1 = new Temp(true);
			list.add(new ArithmeticExpr(dest1, temp2, ArithmeticOp.MUL, new IntegerConst(4)));
		}
		if (temp1 instanceof IntegerConst && dest1 instanceof IntegerConst) {
			dest2 = new IntegerConst(((IntegerConst) temp1).value + ((IntegerConst) dest1).value);
		}
		else if (temp1 instanceof IntegerConst) {
			dest2 = new Temp(true);
			list.add(new ArithmeticExpr(dest2, dest1, ArithmeticOp.ADD, temp1));
		}
		else {
			dest2 = new Temp(true);
			list.add(new ArithmeticExpr(dest2, temp1, ArithmeticOp.ADD, dest1));
		}
		list.add(new MemoryRead(dest, dest2, new IntegerConst(4)));
		return dest;
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		Address temp1 = body.getValue(table, list);
		Address temp2 = subscript.getValue(table, list);
		Address dest1;
		Address dest2;
		if (temp2 instanceof IntegerConst) {
			dest1 = new IntegerConst(((IntegerConst) temp2).value * 4);
		}
		else {
			dest1 = new Temp(true);
			list.add(new ArithmeticExpr(dest1, temp2, ArithmeticOp.MUL, new IntegerConst(4)));
		}
		if (temp1 instanceof IntegerConst && dest1 instanceof IntegerConst) {
			dest2 = new IntegerConst(((IntegerConst) temp1).value + ((IntegerConst) dest1).value);
		}
		else if (temp1 instanceof IntegerConst) {
			dest2 = new Temp(true);
			list.add(new ArithmeticExpr(dest2, dest1, ArithmeticOp.ADD, temp1));
		}
		else {
			dest2 = new Temp(true);
			list.add(new ArithmeticExpr(dest2, temp1, ArithmeticOp.ADD, dest1));
		}
		list.add(new MemoryRead(dest, dest2, new IntegerConst(4)));
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		Address temp1 = body.getValue(table, list);
		Address temp2 = subscript.getValue(table, list);
		Address dest1;
		Address dest2;
		if (temp2 instanceof IntegerConst) {
			dest1 = new IntegerConst(((IntegerConst) temp2).value * 4);
		}
		else {
			dest1 = new Temp(true);
			list.add(new ArithmeticExpr(dest1, temp2, ArithmeticOp.MUL, new IntegerConst(4)));
		}
		if (temp1 instanceof IntegerConst && dest1 instanceof IntegerConst) {
			dest2 = new IntegerConst(((IntegerConst) temp1).value + ((IntegerConst) dest1).value);
		}
		else if (temp1 instanceof IntegerConst) {
			dest2 = new Temp(true);
			list.add(new ArithmeticExpr(dest2, dest1, ArithmeticOp.ADD, temp1));
		}
		else {
			dest2 = new Temp(true);
			list.add(new ArithmeticExpr(dest2, temp1, ArithmeticOp.ADD, dest1));
		}
		return new Memory(dest2, new IntegerConst(4));
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
