package compiler.ast;

import compiler.ir.Address;
import compiler.ir.IntegerConst;
import compiler.ir.Quadruple;
import compiler.ir.Temp;

import java.io.*;
import java.util.ArrayList;

public class ClassDecl extends Decl {
    public Symbol tag;
    public VarDeclList fields;
	public Symbol extend;

    public ClassDecl(Symbol tag) {
        this.tag = tag;
        this.fields = null;
		this.extend = null;
    }
	
    public ClassDecl(Symbol tag, VarDeclList fields) {
        this.tag = tag;
        this.fields = fields;
		this.extend = null;
    }
	
    public ClassDecl(Symbol tag, VarDeclList fields, Symbol extend) {
        this.tag = tag;
        this.fields = fields;
		this.extend = extend;
    }
	
	public String toString(int d) {
		String string = indent(d) + "ClassDecl" + "\n" + tag.toString(d + 1);
		if (fields != null)
			string += fields.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		if (fields != null)
			return "class " + tag.toString() + " {\n" + fields.prettyPrinter(d + 1) + "}\n";
		else
			return "class " + tag.toString() + " {\n" + "}\n";
	}

	public boolean round_1(SymbolTable table) {
		if (!table.put(tag, this)) {
			System.out.println("Class name already used!\n" + this.info.toString() + this.toString(0));
			return false;
		}
		return true;
	}

	public void copy(SymbolTable table, Symbol ext) {
		if (ext != null) {
			VarDeclList tmp = ((ClassDecl)table.get(ext)).fields;
			for (; tmp != null; ) {
				fields = new VarDeclList(tmp.varDecl, fields);
				tmp = tmp.varDeclList;
			}
			copy(table, ((ClassDecl)table.get(ext)).extend);
		}
	}
	
	public boolean round_2(SymbolTable table) {
		copy(table, extend);
		extend = null;
		if (fields != null)
			return fields.round_2(table);
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (fields != null) {
			table.beginScope();
			if (fields.round_3(table, loop, func) == null) {
				table.endScope();
				return null;
			}
			table.endScope();
		}
		return this;
	}

	public Address getSize() {
		return new IntegerConst(fields.getSize());
	}

}
