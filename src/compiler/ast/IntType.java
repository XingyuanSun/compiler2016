package compiler.ast;

public class IntType extends BasicType {
	public String toString(int d) {
		return indent(d) + "IntType\n";
	}

	public boolean equal(Type rhs) {
		if (rhs instanceof IntType)
			return true;
		else
			return false;
	}

	public String prettyPrinter(int d) {
		return "int";
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new IntType();
	}
}
