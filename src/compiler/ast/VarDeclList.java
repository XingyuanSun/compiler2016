package compiler.ast;

import compiler.ir.Temp;

import java.util.ArrayList;

public class VarDeclList extends AST {
    public VarDecl varDecl;
    public VarDeclList varDeclList;

    public VarDeclList(VarDecl varDecl) {
        this.varDecl = varDecl;
        this.varDeclList = null;
    }

    public VarDeclList(VarDecl varDecl, VarDeclList varDeclList) {
        this.varDecl = varDecl;
        this.varDeclList = varDeclList;
    }
	
	public String toString(int d) {
		String string = indent(d) + "VarDeclList" + "\n" + varDecl.toString(d + 1);
		if (varDeclList != null)
			string += varDeclList.toString(d + 1);
		return string;
	}

	public boolean equal(VarDeclList rhs) {
		if (!rhs.varDecl.type.equal(varDecl.type))
			return false;
		if (varDeclList == null) {
			if (rhs.varDeclList == null)
				return true;
			else
				return false;
		}
		else {
			if (rhs.varDeclList == null)
				return false;
			else
				return varDeclList.equal(rhs.varDeclList);
		}
	}

	public String prettyPrinter(int d) {
		String string = "";
		if (d == 0) {
			string += varDecl.prettyPrinter(d);
			if (varDeclList == null)
				return string;
			else {
				string += ", " + varDeclList.prettyPrinter(d);
				return string;
			}
		}
		else {
			string += indent(d) + varDecl.prettyPrinter(d) + ";\n";
			if (varDeclList == null)
				return string;
			else {
				string += varDeclList.prettyPrinter(d);
				return string;
			}
		}
	}

	public Type find(Symbol attribute) {
		if (varDecl.name == attribute)
			return varDecl.type;
		else
		if (varDeclList == null)
			return null;
		else
			return varDeclList.find(attribute);
	}
	
	public boolean round_2(SymbolTable table) {
		if (!varDecl.round_2(table))
			return false;
		if (varDeclList != null)
			return varDeclList.round_2(table);
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (varDecl.round_3(table, loop, func) == null)
			return null;
		if (varDeclList != null) {
			if (varDeclList.round_3(table, loop, func) == null)
				return null;
		}
		return this;
	}

	public void addTo(SymbolTable table, ArrayList<Temp> args) {
		varDecl.addTo(table, args);
		if (varDeclList != null)
			varDeclList.addTo(table, args);
	}

	public int getSize() {
		if (varDeclList == null)
			return 1;
		else
			return varDeclList.getSize() + 1;
	}

	public int kth(Symbol attribute) {
		if (varDecl.name == attribute)
			return 0;
		else
			return 1 + varDeclList.kth(attribute);
	}
}
