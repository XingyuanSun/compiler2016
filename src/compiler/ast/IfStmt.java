package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class IfStmt extends Stmt {
    public Expr condition;
    public Stmt consequent;
    public Stmt alternative;

    public IfStmt(Expr condition, Stmt consequent) {
        this.condition = condition;
        this.consequent = consequent;
        this.alternative = null;
    }

    public IfStmt(Expr condition, Stmt consequent, Stmt alternative) {
        this.condition = condition;
        this.consequent = consequent;
        this.alternative = alternative;
    }
	
	public String toString(int d) {
		String string = indent(d) + "IfStmt\n" + condition.toString(d + 1) + consequent.toString(d + 1);
		if (alternative != null)
			string += alternative.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		String string;
		if (consequent instanceof CompoundStmt) {
			if (((CompoundStmt) consequent).stats.stmtList == null && ((CompoundStmt) consequent).stats.stmt != null)
				string = indent(d) + "if (" + condition.prettyPrinter(0) + ")\n" + ((CompoundStmt) consequent).stats.stmt.prettyPrinter(d + 1);
			else
				string = indent(d) + "if (" + condition.prettyPrinter(0) + ") " + consequent.prettyPrinter(-(d + 1));
		}
		else
			string = indent(d) + "if (" + condition.prettyPrinter(0) + ")\n" + consequent.prettyPrinter(d + 1);
		if (alternative != null) {
			string += indent(d) + "else";
			if (alternative instanceof CompoundStmt) {
				if (((CompoundStmt) alternative).stats.stmtList == null && ((CompoundStmt) alternative).stats.stmt != null)
					string += "\n" + ((CompoundStmt) alternative).stats.stmt.prettyPrinter(d + 1);
				else
					string += " " + alternative.prettyPrinter(-(d + 1));
			}
			else
				string += "\n" + alternative.prettyPrinter(d + 1);
			//string += indent(d) + "else\n" + alternative.prettyPrinter(d + 1);
		}
		return string;
	}

	public boolean round_2(SymbolTable table) {
		if (!condition.round_2(table))
			return false;
		if (!consequent.round_2(table))
			return false;
		if (alternative != null)
			return alternative.round_2(table);
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		AST condType = condition.round_3(table, loop, func);
		if (!(condType instanceof Type))
			return null;
		if (!((Type)condType).equal(new BoolType())) {
			System.out.println("BoolType expected here!\n" + this.condition.info.toString() + this.condition.toString(0));
			return null;
		}
		table.beginScope();
		if (consequent.round_3(table, loop, func) == null) {
			table.endScope();
			return null;
		}
		table.endScope();
		if (alternative != null){
			table.beginScope();
			if (alternative.round_3(table, loop, func) == null) {
				table.endScope();
				return null;
			}
			table.endScope();
		}
		return this;
	}

	@Override
	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		if (alternative != null) {
			Label label1 = new Label();
			Label label2 = new Label();
			Label label3 = new Label();
			Address cond = condition.getValue(table, list);
			if (cond instanceof IntegerConst) {
				if (((IntegerConst) cond).value == 1)
					list.add(new Goto(label1));
				else
					list.add(new Goto(label2));
			}
			else {
				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, label2));
				}
				else
					list.add(new Branch(cond, label1, label2));
			}
			list.add(label1);
			table.beginScope();
			consequent.translate(table, list, begin, end, function);
			table.endScope();
			list.add(new Goto(label3));
			list.add(label2);
			table.beginScope();
			alternative.translate(table, list, begin, end, function);
			table.endScope();
			list.add(new Goto(label3));
			list.add(label3);
		}
		else {
			Label label1 = new Label();
			Label label2 = new Label();
			Address cond = condition.getValue(table, list);
			if (cond instanceof IntegerConst) {
				if (((IntegerConst) cond).value == 1)
					list.add(new Goto(label1));
				else
					list.add(new Goto(label2));
			}
			else {
				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, label2));
				}
				else
					list.add(new Branch(cond, label1, label2));
			}
			list.add(label1);
			table.beginScope();
			consequent.translate(table, list, begin, end, function);
			table.endScope();
			list.add(new Goto(label2));
			list.add(label2);
		}
	}
}
