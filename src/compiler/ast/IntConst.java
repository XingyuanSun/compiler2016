package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class IntConst extends Expr {
    public long value;

    public IntConst(long value) {
        this.value = value;
    }
	
	public String toString(int d) {
		return indent(d) + "IntConst : " + value + "\n";
	}

    public String prettyPrinter(int d) {
		if (d == 0)
			return String.valueOf(value);
		else
			return indent(d) + String.valueOf(value) + ";\n";
    }

    public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
        return new IntType();
    }

    public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
        return new IntegerConst((int)value);
    }

    public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
         list.add(new Assign(dest, new IntegerConst((int)value)));
    }

    public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
        System.out.println("ERROR!\n");
        return null;
    }

    public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
        getValue(table, list);
    }
}
