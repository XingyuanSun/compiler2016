package compiler.ast;

import compiler.ir.Address;
import compiler.ir.Label;
import compiler.ir.Quadruple;
import compiler.ir.Temp;

import java.util.ArrayList;

public class EmptyExpr extends Expr {
	public String toString(int d) {
		return indent(d) + "EmptyExpr\n";
	}

	public String prettyPrinter(int d) {
		if (d == 0)
			return "";
		else
			return indent(d) + ";\n";
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new NullType();
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		return null;
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("ERROR!\n");
		return null;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
