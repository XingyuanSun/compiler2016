package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class PostSelfIncrement extends Expr {
    public Expr body;

    public PostSelfIncrement(Expr body) {
        this.body = body;
    }
	
	public String toString(int d) {
		return indent(d) + "PostSelfIncrement\n" + body.toString(d + 1);
	}

	public String prettyPrinter(int d) {
		if (d == 0)
			return body.prettyPrinter(0) + "++";
		else
			return indent(d) + body.prettyPrinter(0) + "++;\n";
	}

	public boolean round_2(SymbolTable table) {
		if (body != null)
			return body.round_2(table);
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		AST bodyType = body.round_3(table, loop, func);
		if (bodyType == null)
			return null;
		if (!(bodyType instanceof IntType)) {
			System.out.println("IntType needed here!\n" + info.toString() + toString(0));
			return null;
		}
		if (!((IntType)bodyType).lvalue){
			System.out.println("Lvalue needed here!\n" + info.toString() + toString(0));
			return null;
		}
		((IntType)bodyType).lvalue = false;
		return bodyType;
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		Address addr = body.getAddress(table, list);
		Address value = new Temp(true);
		if (addr instanceof Temp)
			list.add(new Assign(value, addr));
		else
			list.add(new MemoryRead(value, ((Memory)addr).temp, ((Memory)addr).offset));

		if (addr instanceof Temp)
			list.add(new ArithmeticExpr(addr, value, ArithmeticOp.ADD, new IntegerConst(1)));
		else {
			Temp temp = new Temp(true);
			list.add(new ArithmeticExpr(temp, value, ArithmeticOp.ADD, new IntegerConst(1)));
			list.add(new MemoryWrite(((Memory)addr).temp, ((Memory)addr).offset, temp));
		}
		return value;
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		Address addr = body.getAddress(table, list);
		if (addr instanceof Temp)
			list.add(new Assign(dest, addr));
		else
			list.add(new MemoryRead(dest, ((Memory)addr).temp, ((Memory)addr).offset));

		if (addr instanceof Temp)
			list.add(new ArithmeticExpr(addr, dest, ArithmeticOp.ADD, new IntegerConst(1)));
		else {
			Temp temp = new Temp(true);
			list.add(new ArithmeticExpr(temp, dest, ArithmeticOp.ADD, new IntegerConst(1)));
			list.add(new MemoryWrite(((Memory)addr).temp, ((Memory)addr).offset, temp));
		}
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("ERROR");
		return null;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
