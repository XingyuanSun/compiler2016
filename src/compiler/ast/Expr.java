package compiler.ast;

import compiler.ir.Address;
import compiler.ir.Quadruple;
import compiler.ir.Temp;

import java.util.ArrayList;

public abstract class Expr extends Stmt {
    abstract public Address getValue(SymbolTable table, ArrayList<Quadruple> list);
    abstract public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest);
    abstract public Address getAddress(SymbolTable table, ArrayList<Quadruple> list);

    public static int getPriority(BinaryOp op) {
        switch (op) {
            case ASSIGN: return 0;
            case LOGICAL_OR: return 1;
            case LOGICAL_AND: return 2;
            case OR: return 3;
            case XOR: return 4;
            case AND: return 5;
            case EQ: return 6;
            case NE: return 6;
            case LT: return 7;
            case GT: return 7;
            case LE: return 7;
            case GE: return 7;
            case SHL: return 8;
            case SHR: return 8;
            case ADD: return 9;
            case SUB: return 9;
            case MUL: return 10;
            case DIV: return 10;
            case MOD: return 10;
        }
        return -1;
    }

    public static int getPriority(UnaryOp op) {
        return 11;
    }
}
