package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class ClassAccess extends Expr {
	public Expr body;
	public Symbol attribute;
	public AST bodyType;

	public ClassAccess(Expr body, Symbol attribute) {
		this.body = body;
		this.attribute = attribute;
	}

	public String toString(int d) {
		return indent(d) + "ClassAccess\n" + body.toString(d + 1) + attribute.toString(d + 1);
	}

	public String prettyPrinter(int d) {
		if (d == 0)
			return body.prettyPrinter(0) + "." + attribute.toString();
		else
			return indent(d) + body.prettyPrinter(0) + "." + attribute.toString() + ";\n";
	}

	public boolean round_2(SymbolTable table) {
		if (body != null && !body.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		bodyType = body.round_3(table, loop, func);
		if (body == null || !(bodyType instanceof Type))
			return null;
		if (bodyType instanceof ClassType) {
			ClassDecl classDecl = (ClassDecl) table.get(((ClassType)bodyType).tag);
			/*if (classDecl.methods != null) {
				FunctionDecl ans = classDecl.methods.find(attribute);
				if (ans != null)
					return ans;
			}*/
			if (classDecl.fields != null){
				Type ans = classDecl.fields.find(attribute);
				if (ans != null) {
					if (((ClassType)bodyType).lvalue)
						ans.lvalue = true;
					else
						ans.lvalue = false;
					return ans;
				}
			}
			System.out.println("Attribute not found!\n" + info.toString() + toString(0));
			return null;
		}
		if (bodyType instanceof ArrayType) {
			/*if (((ArrayType)bodyType).methods != null) {
				FunctionDecl ans = ((ArrayType)bodyType).methods.find(attribute);
				if (ans != null)
					return ans;
			}*/
			System.out.println("Attribute not found!\n" + info.toString() + toString(0));
			return null;
		}
		if (bodyType instanceof StringType) {
			/*if (((StringType)bodyType).methods != null) {
				FunctionDecl ans = ((StringType)bodyType).methods.find(attribute);
				if (ans != null)
					return ans;
			}*/
			System.out.println("Attribute not found!\n" + info.toString() + toString(0));
			return null;
		}
		return null;
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		Address temp = body.getValue(table, list);
		Temp value = new Temp(true);
		ClassDecl classDecl = (ClassDecl)table.get(((ClassType)bodyType).tag);
		int count = classDecl.fields.kth(attribute);
		list.add(new MemoryRead(value, temp, new IntegerConst(4 * count)));
		return value;
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		Address temp = body.getValue(table, list);
		ClassDecl classDecl = (ClassDecl)table.get(((ClassType)bodyType).tag);
		int count = classDecl.fields.kth(attribute);
		list.add(new MemoryRead(dest, temp, new IntegerConst(4 * count)));
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		Address temp = body.getValue(table, list);
		ClassDecl classDecl = (ClassDecl)table.get(((ClassType)bodyType).tag);
		int count = classDecl.fields.kth(attribute);
		return new Memory(temp, new IntegerConst(4 * count));
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}

