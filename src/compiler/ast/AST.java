package compiler.ast;
import compiler.ir.Quadruple;

import java.util.*;

public abstract class AST {
	public Info info;
	public String indent(int number) {
		String string = "";
		for (; --number >= 0; )
			string = string + "\t";
		return string;
	}
	
	public abstract String toString(int d);
	public abstract String prettyPrinter(int d);
	public boolean round_1(SymbolTable table) {
		return true;
	}
	public boolean round_2(SymbolTable table) {
		return true;
	}
	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return this;
	}
	//public void dynamicTranslate(SymbolTable table, ArrayList<Quadruple> list) { }
}
