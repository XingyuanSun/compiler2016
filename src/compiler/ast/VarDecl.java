package compiler.ast;

import compiler.ir.Quadruple;
import compiler.ir.Temp;

import java.util.ArrayList;

public class VarDecl extends Decl {
    public Type type;
    public Symbol name;
    public Expr init;

	public VarDecl(Type type) {
		this.type = type;
		this.name = null;
		this.init = null;
	}

    public VarDecl(Type type, Symbol name) {
        this.type = type;
        this.name = name;
        this.init = null;
    }

    public VarDecl(Type type, Symbol name, Expr init) {
        this.type = type;
        this.name = name;
        this.init = init;
    }
	
	public String toString(int d) {
		String string = indent(d) + "Var" + "\n" + type.toString(d + 1) + name.toString(d + 1);
		if (init != null)
			string += init.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		String string;
		if (type instanceof ArrayType)
			string = ((ArrayType)type).getInnermostType().prettyPrinter(0) + type.prettyPrinter(0);
		else
			string = type.prettyPrinter(0);
		string = string + " " + name.toString();
		if (init != null)
			string += " = " + init.prettyPrinter(0);
		return string;
	}
	
	public boolean round_2(SymbolTable table) {
		if (!type.round_2(table))
			return false;
		if (init != null)
			return init.round_2(table);
		return true;
	}
	
	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (!table.put(name, type)) {
			System.out.println("Varible name already used!\n" + this.info.toString() + this.toString(0));
			return null;
		}
		if (type.round_3(table, loop, func) == null)
			return null;
		if (init != null) {
			AST initType = init.round_3(table, loop, func);
			if (!(initType instanceof Type))
				return null;
			if (!type.equal((Type)initType)) {
				if (((Type)initType).equal(new NullType())) {
					if (!(type.equal(new IntType()) || type.equal(new BoolType()) || type.equal(new StringType())))
						return this;
				}
				System.out.println("Initializer type unexpected!\n" + this.info.toString() + this.toString(0));
				return null;
			}
		}
		return this;
	}

	public void addTo(SymbolTable table, ArrayList<Temp> args) {
		Temp temp = new Temp();
		args.add(temp);
		table.put(name, type, temp);
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list) {
		Temp temp = new Temp();
		table.put(name, type, temp);
		if (init != null)
			init.getValue(table, list, temp);
	}
}
