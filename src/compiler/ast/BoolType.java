package compiler.ast;

public class BoolType extends BasicType {
	public String toString(int d) {
		return indent(d) + "BoolType\n";
	}

	public boolean equal(Type rhs) {
		if (rhs instanceof BoolType)
			return true;
		else
			return false;
	}

	public String prettyPrinter(int d) {
		return "bool";
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new BoolType();
	}
}
