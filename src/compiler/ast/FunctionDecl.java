package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;
import java.util.List;

public class FunctionDecl extends Decl {
    public Type returnType;
    public Symbol name;
    public VarDeclList params;
    public CompoundStmt body;

    public FunctionDecl(Type returnType, Symbol name, VarDeclList params, CompoundStmt body) {
        this.returnType = returnType;
        this.name = name;
        this.params = params;
        this.body = body;
    }
	
	public String toString(int d) {
		String string = indent(d) + "FunctionDecl" + "\n" + returnType.toString(d + 1) + name.toString(d + 1);
		if (params != null)
			string += params.toString(d + 1);
		string += body.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		String string;
		if (returnType instanceof ArrayType)
			string = indent(d) + ((ArrayType)returnType).getInnermostType().prettyPrinter(0) + returnType.prettyPrinter(0) + " " + name.toString() + "(";
		else
			string = indent(d) + returnType.prettyPrinter(0) + " " + name.toString() + "(";
		if (params != null)
			string += params.prettyPrinter(0);
		if (body instanceof CompoundStmt)
			string += ") " + body.prettyPrinter(-(d + 1));
		else
			string += ")\n" + body.prettyPrinter(d + 1);
		return string;
	}

	@Override
	public boolean round_1(SymbolTable table) {
		return true;
	}

	public boolean round_2(SymbolTable table) {
		if (name == Symbol.get("main")){
			if (!(returnType instanceof IntType)) {
				System.out.println("Main must return a IntType!\n" + this.info.toString() + this.toString(0));
				return false;
			}
			if (params != null) {
				System.out.println("Main must not have any parameter!\n" + this.info.toString() + this.toString(0));
				return false;
			}
		}
		if (!table.putFunc(name, this, new Function(name.toString(), new ArrayList<Temp>(), new ArrayList<Quadruple>(), 0))) {
			System.out.println("CFG name already used!\n" + this.info.toString() + this.toString(0));
			return false;
		}
		if (returnType != null && !returnType.round_2(table))
			return false;
		if (params != null && !params.round_2(table))
			return false;
		if (body != null && !body.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (returnType.round_3(table, loop, func) == null)
			return null;
		table.beginScope();
		if (params != null) {
			if (params.round_3(table, loop, func) == null) {
				table.endScope();
				return null;
			}
		}
		if (body.round_3_noNewScope(table, loop, this) == null) {
			table.endScope();
			return null;
		}
		table.endScope();
		return this;
	}

	public Function translate(SymbolTable table) {
		Temp.tempCount = 0;
		table.beginScope();
		ArrayList<Quadruple> list = new ArrayList<Quadruple>();
		Label label = new Label();
		list.add(label);
		ArrayList<Temp> args = new ArrayList<Temp>();
		if (params != null)
			params.addTo(table, args);
		body.translate(table, list, null, null, name.toString());
		table.endScope();
		Function function = table.getFunc(name);
		function.name = name.toString();
		function.args = args;
		function.body = list;
		function.size = Temp.tempCount;
		return function;
	}
}
