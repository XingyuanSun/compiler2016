package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class ForLoop extends Stmt {
    public Expr init;
    public Expr condition;
    public Expr step;
    public Stmt body;

    public ForLoop(Expr init, Expr condition, Expr step, Stmt body) {
        this.init = init;
        this.condition = condition;
        this.step = step;
        this.body = body;
    }
	
	public String toString(int d) {
		String string = indent(d) + "ForLoop\n";
		if (init != null)
			string += init.toString(d + 1);
		if (condition != null)
			string += condition.toString(d + 1);
		if (step != null)
			string += step.toString(d + 1);
		string += body.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		String string = indent(d) + "for (";
		if (init != null)
			string += init.prettyPrinter(0);
		string += "; ";
		if (condition != null)
			string += condition.prettyPrinter(0);
		string += "; ";
		if (step != null)
			string += step.prettyPrinter(0);
		if (body instanceof CompoundStmt) {
			if (((CompoundStmt) body).stats.stmtList == null && ((CompoundStmt) body).stats.stmt != null)
				string += ")\n" + ((CompoundStmt) body).stats.stmt.prettyPrinter(d + 1);
			else
				string += ") " + body.prettyPrinter(-(d + 1));
		}
		else
			string += ")\n" + body.prettyPrinter(d + 1);
		return string;
	}
	
	public boolean round_2(SymbolTable table) {
		if (init != null && !init.round_2(table))
			return false;
		if (condition != null && !condition.round_2(table))
			return false;
		if (step != null && !step.round_2(table))
			return false;
		if (body != null && !body.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (init != null)
			if (init.round_3(table, loop, func) == null)
				return null;
		if (condition != null) {
			AST conditionType = condition.round_3(table, loop, func);
			if (conditionType == null)
				return null;
			if (!(conditionType instanceof Type)) {
				System.out.println("Type needed here!\n" + condition.info.toString() + condition.toString(0));
				return null;
			}
			if (!(((Type)conditionType).equal(new BoolType()))) {
				System.out.println("BoolType expected here!\n" + this.condition.info.toString() + this.condition.toString(0));
				return null;
			}
		}
		if (step != null)
			if (step.round_3(table, loop, func) == null)
				return null;
		table.beginScope();
		if (body.round_3(table, this, func) == null) {
			table.endScope();
			return null;
		}
		table.endScope();
		return this;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		Label label1 = new Label();
		Label label2 = new Label();
		Label label3 = new Label();
		Label label4 = new Label();
		if (init != null)
			init.getValue(table, list);
		list.add(label1);
		if (condition != null) {
			Address cond = condition.getValue(table, list);
			if (cond instanceof IntegerConst) {
				if (((IntegerConst) cond).value == 1)
					list.add(new Goto(label2));
				else
					list.add(new Goto(label3));
			}
			else {
				if (list.get(list.size() - 1) instanceof RelationalExpr) {
					RelationalExpr relationalExpr = (RelationalExpr) list.get(list.size() - 1);
					list.remove(list.size() - 1);
					list.add(new RelationalBranch(relationalExpr.src1, relationalExpr.op, relationalExpr.src2, label3));
				}
				else
					list.add(new Branch(cond, label2, label3));
			}
		}
		else
			list.add(new Goto(label2));
		list.add(label2);
		table.beginScope();
		body.translate(table, list, label4, label3, function);
		table.endScope();
		list.add(label4);
		if (step != null)
			step.getValue(table, list);
		list.add(new Goto(label1));
		list.add(label3);
	}
}
