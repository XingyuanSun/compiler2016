package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class Identifier extends Expr {
    public Symbol symbol;
    public boolean isThis;

    public Identifier(Symbol symbol) {
        this.symbol = symbol;
    }
	
	public String toString(int d) {
		return indent(d) + "Identifier\n" + symbol.toString(d + 1);
	}

    public String prettyPrinter(int d) {
		if (d == 0)
			return symbol.toString();
		else
			return indent(d) + symbol.toString() + ";\n";
    }

    public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
        if (table.get(Symbol.get("this")) instanceof ClassType) {
            VarDeclList list = ((ClassDecl) table.get(((ClassType) table.get(Symbol.get("this"))).tag)).fields;
            if (list != null) {
                if (list.find(symbol) != null) {
                    isThis = true;
                    Type tmp = list.find(symbol);
                    tmp.lvalue = true;
                    return tmp;
                }
            }
        }
        if (table.get(symbol) == null) {
            System.out.println("Undefined identifier here!\n" + this.toString(0));// + this.info.toString() + this.toString(0));
            return null;
        }
		AST rtn = table.get(symbol);
		if (rtn instanceof Type) {
			rtn = rtn.round_3(table, loop, func);
            ((Type) rtn).lvalue = true;
		}
        return rtn;
    }

    public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
        if (isThis) {
            Address temp = table.getAddr(Symbol.get("this"));
            Temp value = new Temp(true);
            ClassDecl classDecl = (ClassDecl)table.get(((ClassType) table.get(Symbol.get("this"))).tag);
            int count = classDecl.fields.kth(symbol);
            list.add(new MemoryRead(value, temp, new IntegerConst(4 * count)));
            return value;
        }
        return table.getAddr(symbol);
    }

    public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
        if (isThis) {
            Address temp = table.getAddr(Symbol.get("this"));
            ClassDecl classDecl = (ClassDecl)table.get(((ClassType) table.get(Symbol.get("this"))).tag);
            int count = classDecl.fields.kth(symbol);
            list.add(new MemoryRead(dest, temp, new IntegerConst(4 * count)));
        }
        list.add(new Assign(dest, table.getAddr(symbol)));
    }

    public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
        if (isThis) {
            Address temp = table.getAddr(Symbol.get("this"));
            ClassDecl classDecl = (ClassDecl)table.get(((ClassType) table.get(Symbol.get("this"))).tag);
            int count = classDecl.fields.kth(symbol);
            return new Memory(temp, new IntegerConst(4 * count));
        }
        return table.getAddr(symbol);
    }

    public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
        getValue(table, list);
    }
}
