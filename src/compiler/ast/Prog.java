package compiler.ast;

import compiler.ir.*;

import java.util.List;
import java.util.LinkedList;
import java.util.*;

public class Prog extends AST {
    /** Declarations are stored in this value by their scanning order. */
    public List<Decl> decls;

    public Prog() {
        decls = new LinkedList<Decl>();
    }

    public Prog(List<Decl> decls) {
        this.decls = decls;
    }
	
	public String toString(int d) {
		String string = indent(d) + "Prog" + "\n";
		for (int i = 0; i < decls.size(); ++i) {
			string += decls.get(i).toString(d + 1);
		}
		return string;
	}
	
	public String prettyPrinter(int d) {
		String string = "";
		for (int i = 0; i < decls.size(); ++i) {
			string += decls.get(i).prettyPrinter(d);
			if (decls.get(i) instanceof VarDecl) {
				string += ";\n";
			}
			else
				string += "\n";
		}
		return string;
	}
	
	public boolean round_1(SymbolTable table) {
		for (int i = 0; i < decls.size(); ++i)
			if (!decls.get(i).round_1(table))
				return false;
		return true;
	}
	
	public boolean round_2(SymbolTable table) {
		for (int i = 0; i < decls.size(); ++i)
			if (!decls.get(i).round_2(table))
				return false;
		return true;
	}
	
	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		for (int i = 0; i < decls.size(); ++i)
			if (decls.get(i).round_3(table, loop, func) == null)
				return null;
		return this;
	}

	public IR translate(SymbolTable table) {
		StringAddressConst.initialize();
		IR ir = new IR(new ArrayList<Function>(), new ArrayList<Temp>(), new ArrayList<Quadruple>(), null, 0);
		Temp.globalCount = 0;
		Temp.tempCount = 0;//For global variables
		for (int i = 0; i < decls.size(); ++i)
			if (decls.get(i) instanceof VarDecl) {
				Temp temp = new Temp(((VarDecl) decls.get(i)).name.toString());
				ir.variables.add(temp);
				//table.put(((VarDecl) decls.get(i)).name, ((VarDecl) decls.get(i)).type, temp);
				table.set(((VarDecl) decls.get(i)).name, temp);
				//System.out.print(((VarDecl) decls.get(i)).name);
				if (((VarDecl) decls.get(i)).init != null)
					((VarDecl) decls.get(i)).init.getValue(table, ir.initializer, table.getAddr(((VarDecl) decls.get(i)).name));
					//ir.initializer.add(new Assign(table.getAddr(((VarDecl) decls.get(i)).name), ((VarDecl) decls.get(i)).init.getValue(table, ir.initializer)));
			}
		ir.size = Temp.tempCount;
		for (int i = 0; i < decls.size(); ++i)
			if (decls.get(i) instanceof FunctionDecl) {
				ir.fragments.add(((FunctionDecl) decls.get(i)).translate(table));
				if (((FunctionDecl) decls.get(i)).name.toString() == "main")
					ir.mainFunction = ir.fragments.get(ir.fragments.size() - 1);
			}
		if (ir.mainFunction == null)
			System.out.print("main function needed!\n");
		return ir;
	}
}
