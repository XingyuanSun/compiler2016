package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;

public class BoolConst extends Expr {
	public boolean value;
	
	public BoolConst(boolean value) {
        this.value = value;
    }
	
	public String toString(int d) {
		return indent(d) + "BoolConst : " + value + "\n";
	}

	public String prettyPrinter(int d) {
		if (d == 0){
			if (value)
				return "true";
			else
				return "false";
		}
		else {
			if (value)
				return indent(d) + "true;\n";
			else
				return indent(d) + "false;\n";
		}
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new BoolType();
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		if (value)
			return new IntegerConst(1);
		else
			return new IntegerConst(0);
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		if (value)
			list.add(new Assign(dest, new IntegerConst(1)));
		else
			list.add(new Assign(dest, new IntegerConst(0)));
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("ERROR!\n");
		return null;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
