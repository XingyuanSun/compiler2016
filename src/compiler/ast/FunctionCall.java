package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;
import java.util.List;

public class FunctionCall extends Expr {
    public Expr body;
    public ExprList args;
	public AST function;
	public Type rtnType;

    public FunctionCall(Expr body) {
        this.body = body;
        this.args = null;
    }

    public FunctionCall(Expr body, ExprList args) {
        this.body = body;
        this.args = args;
    }
	
	public String toString(int d) {
		String string = indent(d) + "FunctionCall\n";
		string += body.toString(d + 1);
		if (args != null)
			string += args.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		String string;
		if (((FunctionDecl) function).name.toString().equals("size") || ((FunctionDecl) function).name.toString().equals("length") || ((FunctionDecl) function).name.toString().equals("parseInt")) {
			string = args.expr.prettyPrinter(0) + "." + ((FunctionDecl) function).name.toString() + "()";
		}
		else if (((FunctionDecl) function).name.toString().equals("ord") || ((FunctionDecl) function).name.toString().equals("substring")) {
			string = args.expr.prettyPrinter(0) + "." + ((FunctionDecl) function).name.toString() + "(" + args.exprList.prettyPrinter(0) + ")";
		}
		else {
			if (args != null)
				string = body.prettyPrinter(0) + "(" + args.prettyPrinter(0) + ")";
			else
				string = body.prettyPrinter(0) + "()";
		}
		if (d != 0)
			string = indent(d) + string + ";\n";
		return string;
	}

	public boolean round_2(SymbolTable table) {
		if (args != null && !args.round_2(table))
			return false;
		if (!body.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		function = body.round_3(table, loop, func);
		if (!(function instanceof FunctionDecl)) {
			System.out.println("CFG needed here!\n" + info.toString() + toString(0));
			return null;
		}
		rtnType = (Type)((FunctionDecl)function).returnType.round_3(table, loop, func);
		rtnType.lvalue = false;
		if (args != null) {
			AST argsType = args.round_3(table, loop, func);
			if (argsType == null || !(argsType instanceof VarDeclList))
				return null;
			if (((FunctionDecl)function).params == null) {
				System.out.println("CFG arguments do not match!\n" + info.toString() + toString(0));
				return null;
			}
			else {
				if (((FunctionDecl)function).name == Symbol.get("size")
						&& ((VarDeclList)argsType).varDecl.type instanceof ArrayType
						&& ((VarDeclList)argsType).varDeclList == null
						|| ((FunctionDecl)function).params.equal((VarDeclList) argsType))
					return rtnType;
				else {
					System.out.println("CFG arguments do not match!\n" + info.toString() + toString(0));
					return null;
				}
			}
		}
		else {
			if (((FunctionDecl)function).params == null)
				return rtnType;
			else {
				System.out.println("CFG arguments do not match!\n" + info.toString() + toString(0));
				return null;
			}
		}
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		ArrayList<Address> arg = new ArrayList<Address>();
		if (args != null)
			args.translate(table, list, arg);
		if (((FunctionDecl) function).name.toString().equals("size") || ((FunctionDecl) function).name.toString().equals("length")) {
			Temp temp = new Temp();
			list.add(new MemoryRead(temp, arg.get(0), new IntegerConst(0)));
			return temp;
		}
		else if (((FunctionDecl) function).name.toString().equals("ord")) {
			Temp temp = new Temp();
			Temp dest = new Temp();
			list.add(new ArithmeticExpr(temp, arg.get(0), ArithmeticOp.ADD, arg.get(1)));
			list.add(new MemoryRead(dest, temp, new IntegerConst(4), 1));
			return dest;
		}
		else if (((FunctionDecl) function).name.toString().equals("print")) {
			list.add(new BuiltInCall(null, 1, arg));
			return null;
		}
		else if (((FunctionDecl) function).name.toString().equals("println")) {
			list.add(new BuiltInCall(null, 1, arg));
			Temp temp = new Temp();
			list.add(new Assign(temp, StringAddressConst.newLine));
			list.add(new BuiltInCall(null, 1, temp));
			return null;
		}
		else if (((FunctionDecl) function).name.toString().equals("getString")) {
			Temp temp = new Temp();
			list.add(new BuiltInCall(temp, 2));
			return temp;
		}
		else if (((FunctionDecl) function).name.toString().equals("getInt")) {
			Temp temp = new Temp();
			list.add(new BuiltInCall(temp, 3));
			return temp;
		}
		else if (((FunctionDecl) function).name.toString().equals("toString")) {
			Temp temp = new Temp();
			list.add(new BuiltInCall(temp, 4, arg));
			return temp;
		}
		else if (((FunctionDecl) function).name.toString().equals("substring")) {
			Temp temp = new Temp();
			list.add(new BuiltInCall(temp, 5, arg));
			return temp;
		}
		else if (((FunctionDecl) function).name.toString().equals("parseInt")) {
			Temp temp = new Temp();
			list.add(new BuiltInCall(temp, 6, arg));
			return temp;
		}
		else {
			if (rtnType instanceof VoidType) {
				list.add(new Call(null, table.getFunc(((FunctionDecl) function).name), arg, true));
				return null;
			}
			else {
				Temp rt = new Temp();
				list.add(new Call(rt, table.getFunc(((FunctionDecl) function).name), arg, false));
				return rt;
			}
		}
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		ArrayList<Address> arg = new ArrayList<Address>();
		if (args != null)
			args.translate(table, list, arg);
		if (((FunctionDecl) function).name.toString().equals("size") || ((FunctionDecl) function).name.toString().equals("length"))
			list.add(new MemoryRead(dest, arg.get(0), new IntegerConst(0)));
		else if (((FunctionDecl) function).name.toString().equals("ord")) {
			Temp temp = new Temp();
			list.add(new ArithmeticExpr(temp, arg.get(0), ArithmeticOp.ADD, arg.get(1)));
			list.add(new MemoryRead(dest, temp, new IntegerConst(4), 1));
		}
		else if (((FunctionDecl) function).name.toString().equals("print"))
			list.add(new BuiltInCall(null, 1, arg));
		else if (((FunctionDecl) function).name.toString().equals("println")) {
			list.add(new BuiltInCall(null, 1, arg));
			Temp temp = new Temp();
			list.add(new Assign(temp, StringAddressConst.newLine));
			list.add(new BuiltInCall(null, 1, temp));
		}
		else if (((FunctionDecl) function).name.toString().equals("getString"))
			list.add(new BuiltInCall(dest, 2));
		else if (((FunctionDecl) function).name.toString().equals("getInt"))
			list.add(new BuiltInCall(dest, 3));
		else if (((FunctionDecl) function).name.toString().equals("toString"))
			list.add(new BuiltInCall(dest, 4, arg));
		else if (((FunctionDecl) function).name.toString().equals("substring"))
			list.add(new BuiltInCall(dest, 5, arg));
		else if (((FunctionDecl) function).name.toString().equals("parseInt"))
			list.add(new BuiltInCall(dest, 6, arg));
		else {
			if (rtnType instanceof VoidType)
				list.add(new Call(null, table.getFunc(((FunctionDecl) function).name), arg, true));
			else
				list.add(new Call(dest, table.getFunc(((FunctionDecl) function).name), arg, false));
		}
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("Error!");
		return null;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
