package compiler.ast;

import compiler.ir.Label;
import compiler.ir.Quadruple;

import java.util.ArrayList;

public class StmtList extends AST {
    public Stmt stmt;
    public StmtList stmtList;

    public StmtList(Stmt stmt) {
        this.stmt = stmt;
        this.stmtList = null;
    }

    public StmtList(Stmt stmt, StmtList stmtList) {
        this.stmt = stmt;
        this.stmtList = stmtList;
    }
	
	public String toString(int d) {
		String string = indent(d) + "StmtList" + "\n" + stmt.toString(d + 1);
		if (stmtList != null)
			string += stmtList.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		String string = "";
		if (stmt instanceof CompoundStmt)
			string += stmt.prettyPrinter(d + 1);
		else
			string += stmt.prettyPrinter(d);
		if (stmtList != null)
			string += stmtList.prettyPrinter(d);
		return string;
	}

	public boolean round_2(SymbolTable table) {
		if (stmt != null && !stmt.round_2(table))
			return false;
		if (stmtList != null && !stmtList.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (stmt.round_3(table, loop, func) == null)
			return null;
		if (stmtList != null)
			if (stmtList.round_3(table, loop, func) == null)
				return null;
		return this;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		//if (stmt instanceof Expr)
			//((Expr) stmt).getValue(table, list);
		//else
			stmt.translate(table, list, begin, end, function);
		if (stmtList != null)
			stmtList.translate(table, list, begin, end, function);
	}
}
