package compiler.ast;

import compiler.ir.Label;
import compiler.ir.Quadruple;

import java.util.ArrayList;
import java.util.List;

public class CompoundStmt extends Stmt {
    public StmtList stats;

    public CompoundStmt() {
        this.stats = null;
    }

    public CompoundStmt(StmtList stats) {
        this.stats = stats;
    }
	
	public String toString(int d) {
		String string = indent(d) + "CompoundStmt\n";
		if (stats != null)
			string += stats.toString(d + 1);
		return string;
	}

	public String prettyPrinter(int d) {
		if (d < 0) {
			d = -d;
			if (stats != null)
				return "{\n" + stats.prettyPrinter(d) + indent(d - 1) + "}\n";
			else
				return "{\n" + indent(d - 1) + "}\n";
		}
		else {
			if (stats != null)
				return indent(d - 1) + "{\n" + stats.prettyPrinter(d) + indent(d - 1) + "}\n";
			else
				return indent(d - 1) + "{\n" + indent(d - 1) + "}\n";
		}
	}

	public boolean round_2(SymbolTable table) {
		if (stats != null && !stats.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		table.beginScope();
		if (stats.round_3(table, loop, func) == null) {
			table.endScope();
			return null;
		}
		table.endScope();
		return this;
	}

	public AST round_3_noNewScope(SymbolTable table, Stmt loop, FunctionDecl func) {
		if (stats.round_3(table, loop, func) == null)
			return null;
		return this;
	}

	@Override
	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		table.beginScope();
		stats.translate(table, list, begin, end, function);
		table.endScope();
	}
}
