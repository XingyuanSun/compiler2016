package compiler.ast;

import compiler.ir.Label;
import compiler.ir.Quadruple;

import java.util.ArrayList;

public abstract class Stmt extends AST {
     abstract public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function);
}
