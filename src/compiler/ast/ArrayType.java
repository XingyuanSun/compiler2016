package compiler.ast;

import compiler.ir.Address;
import compiler.ir.Quadruple;

import java.util.ArrayList;

public class ArrayType extends Type {
    public Type baseType;
    public Expr arraySize;
	//public FunctionDeclList methods;

    public ArrayType(Type baseType) {
        //methods = new FunctionDeclList(new FunctionDecl(new IntType(), Symbol.get("size"), null, new CompoundStmt(new StmtList(new EmptyExpr()))));
		this.baseType = baseType;
		this.arraySize = null;
    }
	
    public ArrayType(Type baseType, Expr arraySize) {
        //methods = new FunctionDeclList(new FunctionDecl(new IntType(), Symbol.get("size"), null, new CompoundStmt(new StmtList(new EmptyExpr()))));
		this.baseType = baseType;
		this.arraySize = arraySize;
    }

	public String toString(int d) {
		String string = indent(d) + "ArrayType\n" + baseType.toString(d + 1);
		if (arraySize != null)
			string += arraySize.toString(d + 1);
		return string;
	}
	
	public Type getInnermostType() {
		if (baseType instanceof ArrayType)
			return ((ArrayType)baseType).getInnermostType();
		else
			return baseType;
	}

	public String prettyPrinter(int d) {
		if (baseType instanceof ArrayType) {
			if (arraySize != null)
				return "[" + arraySize.prettyPrinter(0) + "]" + baseType.prettyPrinter(0);
			else
				return "[]" + baseType.prettyPrinter(0);
		}
		else {
			if (arraySize != null)
				return "[" + arraySize.prettyPrinter(0) + "]";
			else
				return "[]";
		}
	}


	public boolean equal(Type rhs) {
		if (!(rhs instanceof ArrayType))
			return false;
		if (!((ArrayType)rhs).baseType.equal(this.baseType))
			return false;
		return true;
	}
	
	public boolean round_2(SymbolTable table) {
		if (baseType != null && !baseType.round_2(table))
			return false;
		if (arraySize != null && !arraySize.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		AST base = baseType.round_3(table, loop, func);
		if (base == null)
			return null;
		if (arraySize != null) {
			AST type = arraySize.round_3(table, loop, func);
			if (type == null)
				return null;
			if (!(type instanceof Type)) {
				System.out.println("Type needed here!\n" + info.toString() + toString(0));
				return null;
			}
			if (!(((Type)type).equal(new IntType()))) {
				System.out.println("IntType expected here!\n" + this.info.toString() + this.toString(0));
				return null;
			}
		}
		return new ArrayType((Type)base);
	}

	public Address getSize(SymbolTable table, ArrayList<Quadruple> list) {
		if (arraySize != null)
			return arraySize.getValue(table, list);
		return baseType.getSize(table, list);
	}
}
