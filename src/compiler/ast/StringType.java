package compiler.ast;

public class StringType extends BasicType {
	//public FunctionDeclList methods;
	
	public StringType() {
		//methods = new FunctionDeclList(new FunctionDecl(new IntType(), Symbol.get("length"), null, new CompoundStmt(new StmtList(new EmptyExpr()))));
		//methods = new FunctionDeclList(new FunctionDecl(this, Symbol.get("substring"), new VarDeclList(new VarDecl(new IntType(), Symbol.get("left")), new VarDeclList(new VarDecl(new IntType(), Symbol.get("right")))), new CompoundStmt(new StmtList(new EmptyExpr()))), methods);
		//methods = new FunctionDeclList(new FunctionDecl(new IntType(), Symbol.get("parseInt"), null, new CompoundStmt(new StmtList(new EmptyExpr()))), methods);
		//methods = new FunctionDeclList(new FunctionDecl(new IntType(), Symbol.get("ord"), new VarDeclList(new VarDecl(new IntType(), Symbol.get("pos"))), new CompoundStmt(new StmtList(new EmptyExpr()))), methods);
	}
	public String toString(int d) {
		return indent(d) + "StringType\n";
	}

	public String prettyPrinter(int d) {
		return "string";
	}

	public boolean equal(Type rhs) {
		if (rhs instanceof StringType)
			return true;
		else
			return false;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		return new StringType();
	}
}
