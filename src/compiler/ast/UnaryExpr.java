package compiler.ast;

import compiler.ir.*;

import java.util.ArrayList;
import java.util.concurrent.Exchanger;

public class UnaryExpr extends Expr {
    public UnaryOp op;
    public Expr expr;

    public UnaryExpr(UnaryOp op, Expr expr) {
        this.op = op;
        this.expr = expr;
    }
	
	public String toString(int d) {
		return indent(d) + "UnaryExpr\n" + indent(d + 1) + op.toString() + "\n" + expr.toString(d + 1);
	}

	public String prettyPrinter(int d) {
		String string = "";
		switch (op) {
			case INC: string = "++"; break;
			case DEC: string = "--"; break;
			case PLUS: string = "+"; break;
			case MINUS: string = "-"; break;
			case TILDE: string = "~"; break;
			case NOT: string = "!"; break;
		}
		int exprPriority = 20;
		if (expr instanceof BinaryExpr)
			exprPriority = Expr.getPriority(((BinaryExpr) expr).op);
		else if (expr instanceof UnaryExpr)
			exprPriority = Expr.getPriority(((UnaryExpr) expr).op);
		int priority = Expr.getPriority(op);

		String Expr = expr.prettyPrinter(0);
		if (exprPriority < priority)
			Expr = "(" + Expr + ")";

		string = string + Expr;
		if (d != 0)
			string = indent(d) + string + ";\n";
		return string;
	}
	
	public boolean round_2(SymbolTable table) {
		if (expr != null && !expr.round_2(table))
			return false;
		return true;
	}

	public AST round_3(SymbolTable table, Stmt loop, FunctionDecl func) {
		AST bodyType = expr.round_3(table, loop, func);
		if (bodyType == null)
			return null;
		if (op == UnaryOp.NOT) {
			if (!(bodyType instanceof BoolType)) {
				System.out.println("BoolType needed here!\n" + info.toString() + toString(0));
				return null;
			}
			else
				return bodyType;
		}
		if (!(bodyType instanceof IntType)) {
			System.out.println("IntType needed here!\n" + info.toString() + toString(0));
			return null;
		}
		if (op == UnaryOp.INC || op == UnaryOp.DEC){
			if (!((IntType)bodyType).lvalue) {
				System.out.println("Lvalue needed here!\n" + info.toString() + toString(0));
				return null;
			}
			else
				return bodyType;
		}
		else {
			((IntType) bodyType).lvalue = false;
			return bodyType;
		}
	}

	public Address getValue(SymbolTable table, ArrayList<Quadruple> list) {
		if (op == UnaryOp.INC) {
			Address address = expr.getAddress(table, list);
			if (address instanceof Temp) {
				list.add(new ArithmeticExpr(address, address, ArithmeticOp.ADD, new IntegerConst(1)));
				return address;
			}
			else {
				Temp temp = new Temp(true);
				list.add(new MemoryRead(temp, ((Memory)address).temp, ((Memory)address).offset));
				list.add(new ArithmeticExpr(temp, temp, ArithmeticOp.ADD, new IntegerConst(1)));
				list.add(new MemoryWrite(((Memory)address).temp, ((Memory)address).offset, temp));
				return temp;
			}
		}
		if (op == UnaryOp.DEC) {
			Address address = expr.getAddress(table, list);
			if (address instanceof Temp) {
				list.add(new ArithmeticExpr(address, address, ArithmeticOp.SUB, new IntegerConst(1)));
				return address;
			}
			else {
				Temp temp = new Temp(true);
				list.add(new MemoryRead(temp, ((Memory)address).temp, ((Memory)address).offset));
				list.add(new ArithmeticExpr(temp, temp, ArithmeticOp.SUB, new IntegerConst(1)));
				list.add(new MemoryWrite(((Memory)address).temp, ((Memory)address).offset, temp));
				return temp;
			}
		}
		Address src = expr.getValue(table, list);
		if (op == UnaryOp.PLUS) {
			return src;
		}
		Address dest;
		if (op == UnaryOp.MINUS) {
			if (src instanceof IntegerConst)
				dest = new IntegerConst(-(((IntegerConst) src).value));
			else {
				dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, ArithmeticOp.MINUS, src));
			}
			return dest;
		}
		if (op == UnaryOp.TILDE) {
			if (src instanceof IntegerConst)
				dest = new IntegerConst(~((IntegerConst) src).value);
			else {
				dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, ArithmeticOp.TILDE, src));
			}
			return dest;
		}
		if (op == UnaryOp.NOT) {
			if (src instanceof IntegerConst)
				dest = new IntegerConst(1 - ((IntegerConst) src).value);
			else {
				dest = new Temp(true);
				list.add(new ArithmeticExpr(dest, ArithmeticOp.BOOLNOT, src));
			}
			return dest;
		}
		return null;
	}

	public void getValue(SymbolTable table, ArrayList<Quadruple> list, Temp dest) {
		if (op == UnaryOp.INC) {
			Address address = expr.getAddress(table, list);
			if (address instanceof Temp) {
				list.add(new ArithmeticExpr(address, address, ArithmeticOp.ADD, new IntegerConst(1)));
				list.add(new Assign(dest, address));
			}
			else {
				list.add(new MemoryRead(dest, ((Memory)address).temp, ((Memory)address).offset));
				list.add(new ArithmeticExpr(dest, dest, ArithmeticOp.ADD, new IntegerConst(1)));
				list.add(new MemoryWrite(((Memory)address).temp, ((Memory)address).offset, dest));
			}
		}
		if (op == UnaryOp.DEC) {
			Address address = expr.getAddress(table, list);
			if (address instanceof Temp) {
				list.add(new ArithmeticExpr(address, address, ArithmeticOp.SUB, new IntegerConst(1)));
				list.add(new Assign(dest, address));
			}
			else {
				list.add(new MemoryRead(dest, ((Memory)address).temp, ((Memory)address).offset));
				list.add(new ArithmeticExpr(dest, dest, ArithmeticOp.SUB, new IntegerConst(1)));
				list.add(new MemoryWrite(((Memory)address).temp, ((Memory)address).offset, dest));
			}
		}
		Address src = expr.getValue(table, list);
		if (op == UnaryOp.PLUS) {
			list.add(new Assign(dest, src));
		}
		if (op == UnaryOp.MINUS) {
			if (src instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(-(((IntegerConst) src).value))));
			else
				list.add(new ArithmeticExpr(dest, ArithmeticOp.MINUS, src));
		}
		if (op == UnaryOp.TILDE) {
			if (src instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(~((IntegerConst) src).value)));
			else
				list.add(new ArithmeticExpr(dest, ArithmeticOp.TILDE, src));
		}
		if (op == UnaryOp.NOT) {
			if (src instanceof IntegerConst)
				list.add(new Assign(dest, new IntegerConst(1 - ((IntegerConst) src).value)));
			else
				list.add(new ArithmeticExpr(dest, ArithmeticOp.BOOLNOT, src));
		}
	}

	public Address getAddress(SymbolTable table, ArrayList<Quadruple> list) {
		System.out.println("ERROR!\n");
		return null;
	}

	public void translate(SymbolTable table, ArrayList<Quadruple> list, Label begin, Label end, String function) {
		getValue(table, list);
	}
}
