import java.io.*;
import java.util.*;

import org.antlr.v4.misc.Utils;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import java.io.IOException;

import compiler.parser.*;
import compiler.ast.*;

public class MoListenerAST extends MoBaseListener {
	MoParser parser;
	Stack<AST> stack = new Stack<AST>();
	boolean successful = true;
	
	public MoListenerAST(MoParser parser) {
		this.parser = parser;
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterProgram(MoParser.ProgramContext ctx) {
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitProgram(MoParser.ProgramContext ctx) {
		LinkedList<Decl> decls = new LinkedList<Decl>();
		//System.out.println(stack.size());
		for (; !stack.empty(); ) {
			decls.add((Decl)stack.pop());
		}
		Prog prog = new Prog();
		for (; decls.size() != 0; ) {
			prog.decls.add(decls.getLast());
			decls.removeLast();
		}
		stack.push(prog);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterClass_decl(MoParser.Class_declContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitClass_decl(MoParser.Class_declContext ctx) {
		Stack<FunctionDecl> tmp = new Stack<FunctionDecl>();
		for (int i = 0; ctx.func_decl(i) != null; ++i) {
			tmp.push((FunctionDecl)stack.peek());
			if (tmp.peek().params != null)
				tmp.peek().params = new VarDeclList(new VarDecl(new ClassType(Symbol.get(ctx.ID(0).getText())), Symbol.get("this")), tmp.peek().params);
			else
				tmp.peek().params = new VarDeclList(new VarDecl(new ClassType(Symbol.get(ctx.ID(0).getText())), Symbol.get("this")));
			stack.pop();
		}
		if (ctx.member_decl_list() != null) {
			if (ctx.ID(1) != null)
				stack.push(new ClassDecl(Symbol.get(ctx.ID(0).getText()), (VarDeclList)stack.pop(), Symbol.get(ctx.ID(1).getText())));
			else
				stack.push(new ClassDecl(Symbol.get(ctx.ID(0).getText()), (VarDeclList)stack.pop()));
		}
		else {
			if (ctx.ID(1) != null)
				stack.push(new ClassDecl(Symbol.get(ctx.ID(0).getText()), null, Symbol.get(ctx.ID(1).getText())));
			else
				stack.push(new ClassDecl(Symbol.get(ctx.ID(0).getText())));
		}
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		for (; !tmp.empty(); ) {
			stack.push(tmp.peek());
			tmp.pop();
		}
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMember_decl_list_(MoParser.Member_decl_list_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMember_decl_list_(MoParser.Member_decl_list_Context ctx) {
		stack.push(new VarDeclList(new VarDecl((Type)stack.pop(), Symbol.get(ctx.ID().getText()))));
		((VarDeclList)(stack.peek())).varDecl.info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMember_decl_list_c(MoParser.Member_decl_list_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMember_decl_list_c(MoParser.Member_decl_list_cContext ctx) {
		VarDeclList last = (VarDeclList)stack.pop();
		stack.push(new VarDeclList(new VarDecl((Type)stack.pop(), Symbol.get(ctx.ID().getText())), last));
		((VarDeclList)(stack.peek())).varDecl.info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterType(MoParser.TypeContext ctx) {	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitType(MoParser.TypeContext ctx) {
		if (ctx.getText().equals("int"))
			stack.push(new IntType());
		else if (ctx.getText().equals("string"))
			stack.push(new StringType());
		else if (ctx.getText().equals("bool"))
			stack.push(new BoolType());
		else
			stack.push(new ClassType(Symbol.get(ctx.getText())));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterType_arr_c(MoParser.Type_arr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitType_arr_c(MoParser.Type_arr_cContext ctx) {
		stack.push(new ArrayType((Type) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterType_arr_(MoParser.Type_arr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitType_arr_(MoParser.Type_arr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVar_decl_stmt(MoParser.Var_decl_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVar_decl_stmt(MoParser.Var_decl_stmtContext ctx) {
		stack.push(new VarDeclStmt((VarDecl) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVar_decl_(MoParser.Var_decl_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVar_decl_(MoParser.Var_decl_Context ctx) {
		Type type = (Type)stack.pop();
		VarDecl varDecl = new VarDecl(type, Symbol.get(ctx.ID().getText()), null);
		stack.push(varDecl);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVar_decl_expr(MoParser.Var_decl_exprContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVar_decl_expr(MoParser.Var_decl_exprContext ctx) {
		Expr expr = (Expr)stack.pop();
		Type type = (Type)stack.pop();
		VarDecl varDecl = new VarDecl(type, Symbol.get(ctx.ID().getText()), expr);
		stack.push(varDecl);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunc_decl_t(MoParser.Func_decl_tContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunc_decl_t(MoParser.Func_decl_tContext ctx) {
		CompoundStmt compoundStmt = (CompoundStmt) stack.pop();
		Symbol symbol = Symbol.get(ctx.ID().getText());
		if (ctx.parameter_list() == null) {
			Type type = (Type) stack.pop();
			stack.push(new FunctionDecl(type, symbol, null, compoundStmt));
		}
		else {
			VarDeclList varDeclList = (VarDeclList) stack.pop();
			Type type = (Type) stack.pop();
			stack.push(new FunctionDecl(type, symbol, varDeclList, compoundStmt));
		}
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterFunc_decl_v(MoParser.Func_decl_vContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitFunc_decl_v(MoParser.Func_decl_vContext ctx) {
		CompoundStmt compoundStmt = (CompoundStmt) stack.pop();
		Symbol symbol = Symbol.get(ctx.ID().getText());
		if (ctx.parameter_list() == null) {
			stack.push(new FunctionDecl(new VoidType(), symbol, null, compoundStmt));
		}
		else {
			VarDeclList varDeclList = (VarDeclList) stack.pop();
			stack.push(new FunctionDecl(new VoidType(), symbol, varDeclList, compoundStmt));
		}
		((FunctionDecl)(stack.peek())).returnType.info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBlock_stmt(MoParser.Block_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBlock_stmt(MoParser.Block_stmtContext ctx) {
		if (ctx.stmt_list() != null)
			stack.push(new CompoundStmt((StmtList) stack.pop()));
		else {
			stack.push(new CompoundStmt(new StmtList(new EmptyExpr())));
			((CompoundStmt)(stack.peek())).stats.info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
			((CompoundStmt)(stack.peek())).stats.stmt.info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		}
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_list_(MoParser.Stmt_list_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_list_(MoParser.Stmt_list_Context ctx) {
		stack.push(new StmtList((Stmt)stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_list_c(MoParser.Stmt_list_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_list_c(MoParser.Stmt_list_cContext ctx) {
		StmtList stmtList = (StmtList) stack.pop();
		stack.push(new StmtList((Stmt)stack.pop(), stmtList));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterParameter_list_(MoParser.Parameter_list_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitParameter_list_(MoParser.Parameter_list_Context ctx) {
		stack.push(new VarDeclList(new VarDecl((Type)stack.pop(), Symbol.get(ctx.ID().getText()))));
		((VarDeclList)(stack.peek())).varDecl.info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterParameter_list_c(MoParser.Parameter_list_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitParameter_list_c(MoParser.Parameter_list_cContext ctx) {
		VarDeclList last = (VarDeclList)stack.pop();
		stack.push(new VarDeclList(new VarDecl((Type)stack.pop(), Symbol.get(ctx.ID().getText())), last));
		((VarDeclList)(stack.peek())).varDecl.info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArgument_list_(MoParser.Argument_list_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArgument_list_(MoParser.Argument_list_Context ctx) {
		stack.push(new ExprList((Expr)stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArgument_list_c(MoParser.Argument_list_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArgument_list_c(MoParser.Argument_list_cContext ctx) {
		ExprList exprList = (ExprList)stack.pop();
		stack.push(new ExprList((Expr)stack.pop(), exprList));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_block_stmt(MoParser.Stmt_block_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_block_stmt(MoParser.Stmt_block_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_expr_stmt(MoParser.Stmt_expr_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_expr_stmt(MoParser.Stmt_expr_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_selection_stmt(MoParser.Stmt_selection_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_selection_stmt(MoParser.Stmt_selection_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_iteration_stmt(MoParser.Stmt_iteration_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_iteration_stmt(MoParser.Stmt_iteration_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_jump_stmt(MoParser.Stmt_jump_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_jump_stmt(MoParser.Stmt_jump_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStmt_var_decl_stmt(MoParser.Stmt_var_decl_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStmt_var_decl_stmt(MoParser.Stmt_var_decl_stmtContext ctx) {
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConstant_null(MoParser.Constant_nullContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConstant_null(MoParser.Constant_nullContext ctx) {
		stack.push(new NullConst());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConstant_int(MoParser.Constant_intContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConstant_int(MoParser.Constant_intContext ctx) {
		stack.push(new IntConst(Long.valueOf(ctx.getText()).longValue()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		/*if (ctx.getText().length() > 6) {
			System.out.println("IntConst too long!\n" + stack.peek().info.toString() + stack.peek().toString(0));
			successful = false;
		}*/
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConstant_string(MoParser.Constant_stringContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConstant_string(MoParser.Constant_stringContext ctx) {
		stack.push(new StringConst(ctx.getText().substring(1, ctx.getText().length() - 1)));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		if (ctx.getText().length() > 550) {
			System.out.println("StringConst too long!\n" + stack.peek().info.toString() + stack.peek().toString(0));
			successful = false;
		}
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConstant_true(MoParser.Constant_trueContext ctx) {	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConstant_true(MoParser.Constant_trueContext ctx) {
		stack.push(new BoolConst(true));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConstant_false(MoParser.Constant_falseContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConstant_false(MoParser.Constant_falseContext ctx) {
		stack.push(new BoolConst(false));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJump_stmt_return(MoParser.Jump_stmt_returnContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJump_stmt_return(MoParser.Jump_stmt_returnContext ctx) {
		if (ctx.expr() == null)
			stack.push(new ReturnStmt());
		else {
			Expr expr = (Expr) stack.pop();
			stack.push(new ReturnStmt(expr));
		}
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJump_stmt_break(MoParser.Jump_stmt_breakContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJump_stmt_break(MoParser.Jump_stmt_breakContext ctx) {
		stack.push(new BreakStmt());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJump_stmt_continue(MoParser.Jump_stmt_continueContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJump_stmt_continue(MoParser.Jump_stmt_continueContext ctx) {
		stack.push(new ContinueStmt());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSelection_stmt_if(MoParser.Selection_stmt_ifContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSelection_stmt_if(MoParser.Selection_stmt_ifContext ctx) {
		Stmt stmt = (Stmt) stack.pop();
		Expr expr = (Expr) stack.pop();
		stack.push(new IfStmt(expr, stmt));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSelection_stmt_if_else(MoParser.Selection_stmt_if_elseContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSelection_stmt_if_else(MoParser.Selection_stmt_if_elseContext ctx) {
		Stmt stmt2 = (Stmt) stack.pop();
		Stmt stmt1 = (Stmt) stack.pop();
		Expr expr = (Expr) stack.pop();
		stack.push(new IfStmt(expr, stmt1, stmt2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIteration_stmt_while(MoParser.Iteration_stmt_whileContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIteration_stmt_while(MoParser.Iteration_stmt_whileContext ctx) {
		Stmt stmt = (Stmt) stack.pop();
		Expr expr = (Expr) stack.pop();
		stack.push(new WhileLoop(expr, stmt));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIteration_stmt_for(MoParser.Iteration_stmt_forContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIteration_stmt_for(MoParser.Iteration_stmt_forContext ctx) {
		Stmt stmt = (Stmt) stack.pop();
		Expr expr1 = null, expr2 = null, expr3 = null;
		int count = 0;
		for (ParseTree parseTree : ctx.children) {
			if (parseTree instanceof MoParser.ExprContext) {
				if (count == 2)
					expr3 = (Expr) stack.pop();
			}
			if (parseTree.getText().equals(";")) {
				++count;
			}
		}
		count = 0;
		for (ParseTree parseTree : ctx.children) {
			if (parseTree instanceof MoParser.ExprContext) {
				if (count == 1)
					expr2 = (Expr) stack.pop();
			}
			if (parseTree.getText().equals(";")) {
				++count;
			}
		}
		count = 0;
		for (ParseTree parseTree : ctx.children) {
			if (parseTree instanceof MoParser.ExprContext) {
				if (count == 0)
					expr1 = (Expr) stack.pop();
			}
			if (parseTree.getText().equals(";")) {
				++count;
			}
		}
		stack.push(new ForLoop(expr1, expr2, expr3, stmt));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterExpr_stmt(MoParser.Expr_stmtContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitExpr_stmt(MoParser.Expr_stmtContext ctx) {
		if (ctx.expr() == null)
			stack.push(new EmptyExpr());
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterExpr(MoParser.ExprContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitExpr(MoParser.ExprContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAssignment_expr_(MoParser.Assignment_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAssignment_expr_(MoParser.Assignment_expr_Context ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.ASSIGN, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAssignment_expr_c(MoParser.Assignment_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAssignment_expr_c(MoParser.Assignment_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCondition_expr_(MoParser.Condition_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCondition_expr_(MoParser.Condition_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCondition_expr_c(MoParser.Condition_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCondition_expr_c(MoParser.Condition_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLogical_or_expr_c(MoParser.Logical_or_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLogical_or_expr_c(MoParser.Logical_or_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLogical_or_expr_(MoParser.Logical_or_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLogical_or_expr_(MoParser.Logical_or_expr_Context ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.LOGICAL_OR, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLogical_and_expr_c(MoParser.Logical_and_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLogical_and_expr_c(MoParser.Logical_and_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLogical_and_expr_(MoParser.Logical_and_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLogical_and_expr_(MoParser.Logical_and_expr_Context ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.LOGICAL_AND, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_ior_expr_c(MoParser.Bitwise_ior_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_ior_expr_c(MoParser.Bitwise_ior_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_ior_expr_(MoParser.Bitwise_ior_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_ior_expr_(MoParser.Bitwise_ior_expr_Context ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.OR, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_eor_expr_c(MoParser.Bitwise_eor_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_eor_expr_c(MoParser.Bitwise_eor_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_eor_expr_(MoParser.Bitwise_eor_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_eor_expr_(MoParser.Bitwise_eor_expr_Context ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.XOR, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_and_expr_c(MoParser.Bitwise_and_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_and_expr_c(MoParser.Bitwise_and_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_and_expr_(MoParser.Bitwise_and_expr_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_and_expr_(MoParser.Bitwise_and_expr_Context ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.AND, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_equal_expr_eq(MoParser.Relation_equal_expr_eqContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_equal_expr_eq(MoParser.Relation_equal_expr_eqContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.EQ, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_equal_expr_c(MoParser.Relation_equal_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_equal_expr_c(MoParser.Relation_equal_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_equal_expr_neq(MoParser.Relation_equal_expr_neqContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_equal_expr_neq(MoParser.Relation_equal_expr_neqContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.NE, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_gl_expr_c(MoParser.Relation_gl_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_gl_expr_c(MoParser.Relation_gl_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_gl_expr_le(MoParser.Relation_gl_expr_leContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_gl_expr_le(MoParser.Relation_gl_expr_leContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.LE, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_gl_expr_g(MoParser.Relation_gl_expr_gContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_gl_expr_g(MoParser.Relation_gl_expr_gContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.GT, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_gl_expr_ge(MoParser.Relation_gl_expr_geContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_gl_expr_ge(MoParser.Relation_gl_expr_geContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.GE, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterRelation_gl_expr_l(MoParser.Relation_gl_expr_lContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRelation_gl_expr_l(MoParser.Relation_gl_expr_lContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.LT, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_shift_expr_c(MoParser.Bitwise_shift_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_shift_expr_c(MoParser.Bitwise_shift_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_shift_expr_r(MoParser.Bitwise_shift_expr_rContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_shift_expr_r(MoParser.Bitwise_shift_expr_rContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.SHR, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBitwise_shift_expr_l(MoParser.Bitwise_shift_expr_lContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitBitwise_shift_expr_l(MoParser.Bitwise_shift_expr_lContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.SHL, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAdditive_subtractive_expr_c(MoParser.Additive_subtractive_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAdditive_subtractive_expr_c(MoParser.Additive_subtractive_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAdditive_subtractive_expr_s(MoParser.Additive_subtractive_expr_sContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAdditive_subtractive_expr_s(MoParser.Additive_subtractive_expr_sContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.SUB, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAdditive_subtractive_expr_a(MoParser.Additive_subtractive_expr_aContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAdditive_subtractive_expr_a(MoParser.Additive_subtractive_expr_aContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.ADD, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMultiplicative_divisive_expr_c(MoParser.Multiplicative_divisive_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMultiplicative_divisive_expr_c(MoParser.Multiplicative_divisive_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMultiplicative_divisive_expr_mul(MoParser.Multiplicative_divisive_expr_mulContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMultiplicative_divisive_expr_mul(MoParser.Multiplicative_divisive_expr_mulContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.MUL, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMultiplicative_divisive_expr_mod(MoParser.Multiplicative_divisive_expr_modContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMultiplicative_divisive_expr_mod(MoParser.Multiplicative_divisive_expr_modContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.MOD, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMultiplicative_divisive_expr_div(MoParser.Multiplicative_divisive_expr_divContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMultiplicative_divisive_expr_div(MoParser.Multiplicative_divisive_expr_divContext ctx) {
		Expr expr2 = (Expr) stack.pop();
		Expr expr1 = (Expr) stack.pop();
		stack.push(new BinaryExpr(expr1, BinaryOp.DIV, expr2));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCreation_expr_d(MoParser.Creation_expr_dContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCreation_expr_d(MoParser.Creation_expr_dContext ctx) {
		stack.push(new CreationExpr((Type) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCreation_expr_e(MoParser.Creation_expr_eContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCreation_expr_e(MoParser.Creation_expr_eContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCreation_expr_u(MoParser.Creation_expr_uContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCreation_expr_u(MoParser.Creation_expr_uContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterDim_expr_c(MoParser.Dim_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitDim_expr_c(MoParser.Dim_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterDim_expr_d(MoParser.Dim_expr_dContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitDim_expr_d(MoParser.Dim_expr_dContext ctx) {
		Type type = (Type) stack.pop();
		stack.push(new ArrayType(type, (Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterDim_expr_noe_(MoParser.Dim_expr_noe_Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitDim_expr_noe_(MoParser.Dim_expr_noe_Context ctx) {
		Stack<Expr> tmp = new Stack<Expr>();
		for (; stack.peek() instanceof Expr; )
			tmp.push((Expr) stack.pop());
		Type type = (Type) stack.pop();
		for (; !tmp.empty(); )
			stack.push(tmp.pop());
		stack.push(type);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterDim_expr_noe_c(MoParser.Dim_expr_noe_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitDim_expr_noe_c(MoParser.Dim_expr_noe_cContext ctx) {
		Type type = (Type) stack.pop();
		stack.push(new ArrayType(type));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnary_expr_c(MoParser.Unary_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnary_expr_c(MoParser.Unary_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnary_expr_in(MoParser.Unary_expr_inContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnary_expr_in(MoParser.Unary_expr_inContext ctx) {
		stack.push(new UnaryExpr(UnaryOp.INC, (Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnary_expr_de(MoParser.Unary_expr_deContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnary_expr_de(MoParser.Unary_expr_deContext ctx) {
		stack.push(new UnaryExpr(UnaryOp.DEC, (Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnary_expr_n(MoParser.Unary_expr_nContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnary_expr_n(MoParser.Unary_expr_nContext ctx) {
		stack.push(new UnaryExpr(UnaryOp.NOT, (Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnary_expr_d(MoParser.Unary_expr_dContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnary_expr_d(MoParser.Unary_expr_dContext ctx) {
		stack.push(new UnaryExpr(UnaryOp.TILDE, (Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnary_expr_add(MoParser.Unary_expr_addContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnary_expr_add(MoParser.Unary_expr_addContext ctx) {
		stack.push(new UnaryExpr(UnaryOp.PLUS, (Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterUnary_expr_sub(MoParser.Unary_expr_subContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitUnary_expr_sub(MoParser.Unary_expr_subContext ctx) {
		stack.push(new UnaryExpr(UnaryOp.MINUS, (Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPostfix_expr_f(MoParser.Postfix_expr_fContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPostfix_expr_f(MoParser.Postfix_expr_fContext ctx) {
		if (ctx.argument_list() != null) {
			ExprList exprList = (ExprList) stack.pop();
			stack.push(new FunctionCall(new Identifier(Symbol.get(ctx.ID().getText())), new ExprList((Expr) stack.pop(), exprList)));
		}
		else
			stack.push(new FunctionCall(new Identifier(Symbol.get(ctx.ID().getText())), new ExprList((Expr) stack.pop())));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPostfix_expr_a(MoParser.Postfix_expr_aContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPostfix_expr_a(MoParser.Postfix_expr_aContext ctx) {
		if (ctx.argument_list() != null) {
			ExprList exprList = (ExprList) stack.pop();
			stack.push(new FunctionCall((Expr) stack.pop(), exprList));
		}
		else
			stack.push(new FunctionCall((Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPostfix_expr_de(MoParser.Postfix_expr_deContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPostfix_expr_de(MoParser.Postfix_expr_deContext ctx) {
		stack.push(new PostSelfDecrement((Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPostfix_expr_c(MoParser.Postfix_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPostfix_expr_c(MoParser.Postfix_expr_cContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPostfix_expr_e(MoParser.Postfix_expr_eContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPostfix_expr_e(MoParser.Postfix_expr_eContext ctx) {
		Expr expr = (Expr) stack.pop();
		stack.push(new ArrayAccess((Expr) stack.pop(), expr));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPostfix_expr_in(MoParser.Postfix_expr_inContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPostfix_expr_in(MoParser.Postfix_expr_inContext ctx) {
		stack.push(new PostSelfIncrement((Expr) stack.pop()));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPostfix_expr_id(MoParser.Postfix_expr_idContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPostfix_expr_id(MoParser.Postfix_expr_idContext ctx) {
		stack.push(new ClassAccess((Expr)stack.pop(), Symbol.get(ctx.ID().getText())));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPrimary_expr_id(MoParser.Primary_expr_idContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPrimary_expr_id(MoParser.Primary_expr_idContext ctx) {
		stack.push(new Identifier(Symbol.get(ctx.ID().getText())));
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		if (ctx.ID().getText().length() > 150) {
			System.out.println("Identifier too long!\n" + stack.peek().info.toString() + stack.peek().toString(0));
			successful = false;
		}
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPrimary_expr_const(MoParser.Primary_expr_constContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPrimary_expr_const(MoParser.Primary_expr_constContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPrimary_expr_paren(MoParser.Primary_expr_parenContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPrimary_expr_paren(MoParser.Primary_expr_parenContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal(TerminalNode node) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitErrorNode(ErrorNode node) {
		//System.exit(1);
	}
}
